package chatcriptografia;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

public class Certificate {
	private static final String URL = "https://danforth.ancla.com/phpmyadmin";

	public static void main(String[] args) throws IOException {
		HttpsURLConnection conn = connect(URL);

		if (conn != null) {
			try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
				String input;

				while ((input = br.readLine()) != null) {
					System.out.println(input);
				}
			}
		}
	}

	public static HttpsURLConnection connect(String url) {
		HttpsURLConnection conn = null;

		try {
			conn = (HttpsURLConnection) new URL(url).openConnection();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return conn;
	}
}
