package chatcriptografia;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.Socket;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class SSLClient {
	public static void main(String args[]) throws Exception {
		// TODO System.setProperty >> assigna el fitxer amb la clau
		System.setProperty("javax.net.ssl.keyStore", "cacert_sarrato.jks");
		System.setProperty("javax.net.ssl.keyStorePassword", "123456");
		System.out.println("Client...");

		// TODO Obté SSLSocketFactory
		SSLSocketFactory ssf = (SSLSocketFactory) SSLSocketFactory.getDefault();
		// TODO Del factory crea el SSLSocket
		SSLSocket ss = (SSLSocket) ssf.createSocket();
		// TODO Obté session del socket
		SSLSession session = (SSLSession) ss.getSession();
		// TODO Certificate[] cchain = session.getPeerCertificates();
		System.out.println("The Certificates used by peer");

		// TODO Llista els certificats

	//	Certificate[] certChain = session.getPeerCertificates();
	      Certificate[] cchain2 = session.getPeerCertificates();
	      System.out.println("paso del array: " + cchain2.length);
	      for (int i = 0; i < cchain2.length; i++) {
	    	  System.out.println("entro al bucle");
	        System.out.println(((X509Certificate) cchain2[i]).getSubjectDN());
	      }

		System.out.println("Peer host is: " + session.getPeerHost());
		System.out.println("Cipher is: " + session.getCipherSuite());
		System.out.println("Protocol is: " + session.getProtocol());
		System.out.println("ID is: " + new BigInteger(session.getId()));
		System.out.println("Session created in: " + session.getCreationTime());
		System.out.println("Session accessed in: " + session.getLastAccessedTime());

		BufferedReader in = new BufferedReader(new InputStreamReader(ss.getInputStream()));
		String x = in.readLine();
		System.out.println(x);
		in.close();

	}
}