/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatcriptografia;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alex
 */
public class ByteArrayCommunicator {
    
      public static byte[] readBytes(InputStream in) {
        byte[] message = null;
        try {
            DataInputStream dIn = new DataInputStream(in);

            int length = dIn.readInt();                    // read length of incoming message
            if (length > 0) {
                message = new byte[length];
                dIn.readFully(message, 0, message.length); // read the message
            }
        } catch (IOException ex) {
            Logger.getLogger(CriptoClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Receiving (" + Integer.toString(message.length) + "): " + Arrays.toString(message));
        return message;
    }

    public static void sendBytes(byte[] data, OutputStream out) {
        DataOutputStream dOut = new DataOutputStream(out);
        try {
            dOut.writeInt(data.length); // write length of the message
            dOut.write(data);
            System.out.println("Sending (" + Integer.toString(data.length) + "): " + Arrays.toString(data));
        } catch (IOException ex) {
            Logger.getLogger(CriptoServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
