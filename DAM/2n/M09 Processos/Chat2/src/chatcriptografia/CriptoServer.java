/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatcriptografia;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author alex
 */
public class CriptoServer {

    private Socket socket;
    GenerateKeys serverKeys;
    PublicKey clientKey;
    AsymmetricCryptography asym;
    InputStream in;
    OutputStream out;
    PublicKey publica;
    PrivateKey privada;
	byte[] comunicador;
	PublicKey publicaClient;
	File fitxer;
	

    public static void main(String[] args) throws Exception {
        CriptoServer server;
        server = new CriptoServer();
        server.initServer();
        server.handShake();
        server.communication();
    }

    
   public void init(){
        try {
            this.initServer();
            this.handShake();
            this.communication();
        } catch (IOException ex) {
            Logger.getLogger(CriptoClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(CriptoServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(CriptoServer.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }    
    
    /**
     * Services this thread's client by first sending the client a welcome
     * message then repeatedly reading strings and sending back the capitalized
     * version of the string.
     */
    public void initServer() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        System.out.println("The capitalization server is running.");
        // ENGEGUEM EL SERVIDOR
        ServerSocket listener = new ServerSocket(9898);
        socket = listener.accept();
        this.socket = socket;
        in = socket.getInputStream();
        out = socket.getOutputStream();
        System.out.println("New connection at " + socket);
    }

    public void handShake() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");    
        System.out.println("START SERVER HANDSHAKE");
        //// GENERA KEYS DE 1024 (serverKeys = new GenerateKeys ... )
        try {
			serverKeys = new GenerateKeys(1024);
			publica = serverKeys.getPublicKey();
			privada = serverKeys.getPrivateKey();

			comunicador = publica.getEncoded();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println("Sending Server Public Key");
        //// ENVIA PUBLIC KEY CAP EL CLIENT
        //out.write(comunicador);
        ByteArrayCommunicator.sendBytes(comunicador, out);
        //// LLEGEIX LA RESPOSTA DEL CLIENT, EN byte[]
        //// HA DE SER LA SEVA CLAU PUBLICA, 
        byte[] clau = new ByteArrayCommunicator().readBytes(in);

        //// GENEREM CLAU: KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(key));
        //// LA GUARDEM A clientKey
        publicaClient = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(clau));
        System.out.println(Arrays.toString(publicaClient.getEncoded()));
        
        System.out.println("END SERVER HANDSHAKE");
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");
    }
    
    public void communication(){
        AsymmetricCryptography asym = null;
        byte[] textEncriptatBytes = null;
        byte[] bytesDesencriptats = null;
        String textClar = "no text";
        Boolean endCommunication=false;
        byte[] textBytes = textClar.getBytes();
        
        ////INSTANCIO AsymmetricCryptography
        asym = new AsymmetricCryptography();
        while(!endCommunication){
        	
        	
        	
        	
        //// LLEGIR DEL SOCKET
        textEncriptatBytes = ByteArrayCommunicator.readBytes(in);
        System.out.println("SERVER: REBO DADES XIFRADES>>> "+  Arrays.toString(textEncriptatBytes) );
        System.out.println(textEncriptatBytes.length);
        //// DESXIFRAR AMB LA MEVA CLAU PRIVADA
        //bytesDesencriptats = /* PROCESSA ELS BYTES LLEGITS PER DESXIFRAR-LOS AMB  AsymmetricCryptography */ textEncriptatBytes;
        try {
			bytesDesencriptats = asym.decryptPrivate(textEncriptatBytes, privada);
		
        textClar = new String(bytesDesencriptats);
        if(textClar.contains("@file")) {
        	continue;
        }
        if(textClar.contains("\0")) {
        	textClar = textClar.substring(0, textClar.indexOf("\0"));
        }
        if(textClar.contains("@signature")) {
        	fitxer = new File("signature.txt");
            RandomAccessFile ran = new RandomAccessFile(fitxer, "rw");
            ran.seek(ran.length());
            byte[] byteSignature = textClar.getBytes();
            Signature signature;
            ran.write(textClar.getBytes());
        }else {
        	fitxer = new File("fitxerSever.txt");
            RandomAccessFile ran = new RandomAccessFile(fitxer, "rw");
            ran.seek(ran.length());
            ran.write(textClar.getBytes());
        }

        
        //// MOSTRA PER PANTALLA EL TEXT CLAR
        System.out.println("SERVER: TEXT DESENCRIPTAT>>> "+  textClar );
        
        
        //// TO UPPERCASE
        textClar = textClar.toUpperCase();
        System.out.println("SERVER: CONVERTING TO UPPERCASE >>> "+  textClar );
        //// ENCRIPTA AMB CLAU PUBLICA DEL CLIENT
        //textEncriptatBytes = /* ENCRIPTA AMB LA CLAU PUBLICA DEL CLIENT */ textClar.getBytes();
        textBytes = textClar.getBytes();
        textEncriptatBytes = asym.encryptPublic(textBytes, publicaClient);
                
        //// ENVIA-LI
        ByteArrayCommunicator.sendBytes(textEncriptatBytes, out);
        if (textClar.equals("quit")) endCommunication= true;
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException
				| NoSuchAlgorithmException | NoSuchPaddingException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        }            
        
    }

}
