package chatcriptografia;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.util.Enumeration;

public class Magatzem {

	public static void main(String[] args) {
		System.out.println("Mostrem contingut de: " + args[0]);
		if (args.length == 1) {
			File fitxer = new File(args[0]);
			sida(fitxer);
		}
	}

	public static void sida(File fitxer) {
		try {
			
			FileInputStream in = new FileInputStream(fitxer);

			KeyStore magatzem = KeyStore.getInstance(KeyStore.getDefaultType());

			String password = "123456";

			magatzem.load(in, password.toCharArray());

			Enumeration<String> alias = magatzem.aliases();
			Certificate certificat = null;
			String element;
			while (alias.hasMoreElements()) {
				element = alias.nextElement();
				certificat = magatzem.getCertificate(element);
				System.out.println(certificat.toString());
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

	}
}