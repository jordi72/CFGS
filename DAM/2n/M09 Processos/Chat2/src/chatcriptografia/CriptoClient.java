/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatcriptografia;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author alex
 */
public class CriptoClient {

	GenerateKeys clientKeys;
	PublicKey serverKey;
	AsymmetricCryptography Asym;
	String serverAddress = "localhost";
	InputStream in;
	OutputStream out;
	PublicKey publica, serverPublica;
	PrivateKey privada;
	File fitxer;
	Signature sign;
	byte[] signatura;

	public static void main(String[] args) throws InterruptedException, SignatureException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		try {
			CriptoClient client = new CriptoClient();
			client.connectToServer();
			client.handShake();
			Thread.sleep(500);
			client.communication();
		} catch (IOException ex) {
			Logger.getLogger(CriptoClient.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Constructs the client by laying out the GUI and registering a listener with
	 * the textfield so that pressing Enter in the listener sends the textfield
	 * contents to the server.
	 */
	public CriptoClient() {
		CriptoServer cs = new CriptoServer();

		new Thread(() -> {
			cs.init();
		}).start();// LLENÇO EL SERVER EN UN THREAD

	}

	/**
	 * Implements the connection logic by prompting the end user for the server's IP
	 * address, connecting, setting up streams, and consuming the welcome messages
	 * from the server. The Capitalizer protocol says that the server sends three
	 * lines of text to the client immediately after establishing a connection.
	 */
	public void connectToServer() throws IOException {
		//// GENERA KEYS PUBLIC / PRIVATE. GUARDALES
		try {
			clientKeys = new GenerateKeys(1024);
			publica = clientKeys.getPublicKey();
			privada = clientKeys.getPrivateKey();
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// CONNEXIO
		System.out.println("Welcome to the Capitalization Program");
		System.out.println("Client Connecting to server...");

		Socket socket = new Socket(serverAddress, 9898);
		in = socket.getInputStream();
		out = socket.getOutputStream();

	}

	public void handShake() throws IOException {
		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" ");
		System.out.println("BEGIN CLIENT HANDSHAKE");
		byte[] clau = null;

		//// LLEGEIXO LA CLAU PUBLICA DEL SERVER.
		clau = new ByteArrayCommunicator().readBytes(in);
		//// AMB ELS BYTES LLEGITS GENEREM LA CLAU PUBLICA:
		//// KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(clau));
		//// LA GUARDEM A serverKey
		try {
			serverPublica = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(clau));
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Server Public Key Received: ");
		System.out.println(Arrays.toString(clau));
		System.out.println("Sending Client Public Key");
		//// TOFO envar clau publica al server
		ByteArrayCommunicator.sendBytes(publica.getEncoded(), out);
		System.out.println("END CLIENT HANDSHAKE");
		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" ");
	}

	public void communication() throws SignatureException, InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
		System.out.println("Comença la comunicació xifrada!!");
		AsymmetricCryptography asym = null;
		byte[] textEncriptatBytes = null;
		byte[] bytesDesencriptats = null;
		String textDesencriptat = null;
		String textClar = null;
		Boolean endCommunication = false;
		byte[] textBytes = null;

		while (!endCommunication) {
			String[] userShit = null;
			//// INPUT USER LOWERCASE TEXT
			System.out.println("Enter lowercase text: ");
			Scanner scanner = new Scanner(System.in);
			textClar = scanner.nextLine();

			if (textClar.contains("@file")) {
				userShit = textClar.split(":");

				//// TODO ENCRIPT WITH SERVER PUBLIC KEY
				// textBytes = textClar.getBytes();
				try {
					sign = Signature.getInstance("SHA1withRSA");
					sign.initSign(privada);
					// System.out.println(Arrays.toString(textBytes));
					System.out.println(Arrays.toString(serverPublica.getEncoded()));
					// System.out.println(textClar);
					asym = new AsymmetricCryptography();

					// @file:fitxer.txt

					// parse ":"
					// nomFitxer

					fitxer = new File(userShit[1]);

					// envies @file:nomFitxer:longitud

					String toServer = userShit[0] + fitxer.getName() + fitxer.length() + "\n";
					byte[] serverShit = toServer.getBytes();
					textEncriptatBytes = asym.encryptPublic(serverShit, serverPublica);
					ByteArrayCommunicator.sendBytes(textEncriptatBytes, out);
					try {
						RandomAccessFile ran = new RandomAccessFile(fitxer, "r");
						byte[] fitxerArray = new byte[(int) ran.length()];
						ran.readFully(fitxerArray);
						System.out.println(Arrays.toString(fitxerArray));
						System.out.println(fitxerArray.length);

						for (int i = 0; i < fitxerArray.length; i += 117) {
							// int j = 0;
							byte[] tempArray = Arrays.copyOfRange(fitxerArray, i, i + 117);
							sign.update(tempArray);
							// j = j+117;
							System.out.println(new String(tempArray));
							System.out.println(tempArray.length);
							System.out.println(i);

							textEncriptatBytes = asym.encryptPublic(tempArray, serverPublica);
							//// SEND TO SERVER
							ByteArrayCommunicator.sendBytes(textEncriptatBytes, out);
						}

						signatura = sign.sign();

						System.out.println(Arrays.toString(signatura));
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					// textEncriptatBytes = asym.encryptPublic(textBytes, serverPublica);

					//// LLEGEIX DEL SERVER EL LA RESPOSTA ENCRIPTADA
					textEncriptatBytes = ByteArrayCommunicator.readBytes(in);
					System.out.println("CLIENT REBEM RESPOSTA XIFRADA>>> " + Arrays.toString(textEncriptatBytes));
					//// DECRYPT WITH CLIENT PRIVATE KEY
					// bytesDesencriptats = /**TODO DESENCRIPTA AMB LA TEVA CLAU PRIVADA */
					//// textEncriptatBytes;
					bytesDesencriptats = asym.decryptPrivate(textEncriptatBytes, privada);
					textDesencriptat = new String(bytesDesencriptats);
					System.out.println("CLIENT: DESXIFREM LA RESPOSTA >>> " + textDesencriptat);

					if (textDesencriptat.equals("quit")) {
						endCommunication = true;
					}
				} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
						| UnsupportedEncodingException | IllegalBlockSizeException | BadPaddingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (textClar.contains("@signature")) {
				asym = new AsymmetricCryptography();
				String signatureName = "@signature" + String.valueOf(signatura.length);
				byte[] byteTempArray = signatureName.getBytes();
				
				for (int i = 0; i < byteTempArray.length; i += 117) {
					byte[] tempArray = Arrays.copyOfRange(byteTempArray, i, i + 117);

					textEncriptatBytes = asym.encryptPublic(tempArray, serverPublica);
					//// SEND TO SERVER
					ByteArrayCommunicator.sendBytes(textEncriptatBytes, out);
				}
				byteTempArray = signatura;
				for (int i = 0; i < byteTempArray.length; i += 117) {
					byte[] tempArray = Arrays.copyOfRange(byteTempArray, i, i + 117);

					textEncriptatBytes = asym.encryptPublic(tempArray, serverPublica);
					//// SEND TO SERVER
					ByteArrayCommunicator.sendBytes(textEncriptatBytes, out);
				}				
				//ByteArrayCommunicator.sendBytes(asym.encryptPublic(signatura, serverPublica), out);
			}

		}

	}

}
