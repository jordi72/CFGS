/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatcriptografia;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author alex
 */
public class ExempleAsimetric {

    GenerateKeys clientKeys;

    public static void main(String[] args) throws Exception {
        new ExempleAsimetric().init();
    }

    public void init() throws NoSuchAlgorithmException, NoSuchProviderException {
        InputStream in = null;
        clientKeys = new GenerateKeys(1024);
        System.out.println("Client keys generated");
        System.out.println("Public" + Arrays.toString(clientKeys.getPublicKey().getEncoded()));
        System.out.println("Private" + Arrays.toString(clientKeys.getPrivateKey().getEncoded()));
        
        
            System.out.println("***********************************************************************************");
            System.out.println("***********************************************************************************");
            System.out.println("***********************************************************************************");        
        
        
        AsymmetricCryptography asym = null;
        byte[] textEncriptatBytes = null;
        byte[] bytesDesencriptats = null;
        String textClar = "";
        byte[] textBytes = null;
              
        try {
                
            asym =new AsymmetricCryptography();
            textClar = "***** Aquest és un text que encriptarem amb public-key i desencriptarem amb prov-key ***";
            textClar = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
            System.out.println("Encripto "+textClar+"   ("+ textClar.length()+")");
            textBytes = textClar.getBytes();
            textEncriptatBytes = asym.encryptPublic(textBytes, clientKeys.getPublicKey());
            System.out.println(" >>> "+ Arrays.toString(textEncriptatBytes));
            /*
            bytesDesencriptats = asym.decryptPrivate(textEncriptatBytes, clientKeys.getPrivateKey());
            System.out.println(Arrays.toString(bytesDesencriptats));
            textClar=new String(bytesDesencriptats);
            System.out.println("Desencriptem amb clau privada >>>>>>>>>>> "+ textClar );
            
            
            System.out.println("***********************************************************************************");
            System.out.println("***********************************************************************************");
            System.out.println("***********************************************************************************");
            
                       
            textClar = "provant privat > public";
            textBytes = textClar.getBytes();
            textEncriptatBytes = asym.encryptPrivate(textBytes, clientKeys.getPrivateKey());
            System.out.println("Encripto "+textClar+" amb la clau privada >>> "+ Arrays.toString(textEncriptatBytes));
            bytesDesencriptats = asym.decryptPublic(textEncriptatBytes, clientKeys.getPublicKey());
            System.out.println("Desencriptem amb clau pública >>>>>> "+  new String(bytesDesencriptats));
            
       
            */
            
            
        } 
        catch (Exception ex) {           System.out.println("ERROR" );       } 
        //catch (NoSuchPaddingException ex) {            Logger.getLogger(ExempleAsimetric.class.getName()).log(Level.SEVERE, null, ex);        } catch (UnsupportedEncodingException ex) {            Logger.getLogger(ExempleAsimetric.class.getName()).log(Level.SEVERE, null, ex);} 
        //catch (IllegalBlockSizeException ex) {            Logger.getLogger(ExempleAsimetric.class.getName()).log(Level.SEVERE, null, ex);} 
        //catch (BadPaddingException ex) {            Logger.getLogger(ExempleAsimetric.class.getName()).log(Level.SEVERE, null, ex);} 
        //catch (InvalidKeyException ex) {            Logger.getLogger(ExempleAsimetric.class.getName()).log(Level.SEVERE, null, ex) }

    }

}
