/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatcriptografia;


import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.Certificate;
import java.security.cert.X509Certificate;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
/**
 *
 * @author alex
 */
public class SSLServer {

    /**
     * @param args the command line arguments
     */
  public static void main(String[] argv) throws Exception {

      SSLServer.func2();
  }
    
  
  /*
  public static void func1() throws Exception {
    int port = 1234;
    ServerSocketFactory ssocketFactory = SSLServerSocketFactory.getDefault();
    ServerSocket ssocket = ssocketFactory.createServerSocket(port);

    Socket socket = ssocket.accept();

    InputStream in = socket.getInputStream();
    OutputStream out = socket.getOutputStream();

    // Read from in and write to out...

    in.close();
    out.close();
  }
      
  */
  
 public static void func2() throws Exception{
    //TODO System.setProperty >> assigna el fitxer amb la clau
	    System.setProperty("javax.net.ssl.keyStore", "cacert_sarrato.jks");
	    System.setProperty("javax.net.ssl.keyStorePassword", "123456");
     System.out.println("Server...");
 
    //TODO Obté SSLSocketFactory
     SSLServerSocketFactory ssf = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
     
    //TODO Del factory crea el socket     
     SSLServerSocket ss = (SSLServerSocket)ssf.createServerSocket(5432);
     while (true) {
      SSLSocket s = (SSLSocket)ss.accept();
      //TODO Del socket obté session i llista els certificats
      SSLSession session = (SSLSession)s.getSession();
      javax.security.cert.X509Certificate[] peer = session.getPeerCertificateChain();
      for(int i = 0 ; i < peer.length; i++) {
    	
    	  peer[i].checkValidity();
    	  
      }
      
      
      Certificate[] cchain2 = (Certificate[]) session.getLocalCertificates();
      for (int i = 0; i < cchain2.length; i++) {
        System.out.println(((X509Certificate) cchain2[i]).getSubjectDN());
      }
    
      
      //SSLSession session = ((SSLSocket)s).getSession();
      
      System.out.println("Peer host is " + session.getPeerHost());
      System.out.println("Cipher is " + session.getCipherSuite());
      System.out.println("Protocol is " + session.getProtocol());
      System.out.println("ID is " + new BigInteger(session.getId()));
      System.out.println("Session created in " + session.getCreationTime());
      System.out.println("Session accessed in " + session.getLastAccessedTime());

      PrintStream out = new PrintStream(s.getOutputStream());
      out.println("Hi");
      out.close();
      s.close();
    }

  }
  
}
