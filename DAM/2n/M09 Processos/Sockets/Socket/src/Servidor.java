import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Servidor extends JFrame {
   
   
    private   final int PUERTO = 1234;
    protected  String mensajeServidor = "";
    protected  ServerSocket ss;
    protected  Socket cs;
    protected  DataOutputStream salidaCliente;
   
    private  JTextArea textArea;
    private  JTextField textField;
    private  JButton button;
    private  JPanel panel;
   
    private String text ="";
   
    public Servidor() {
        super();
        createConexion();
        createChat();
        addComponents();
        eventButton();
    }
   
    public  void addComponents () {
        panel.add(textArea);
        panel.add(textField);
        panel.add(button);
        this.add(panel);
        this.setVisible(true);
       
    }
   
   
   
    public  void createChat() {
        this.setSize(500,500);
        this.setLocationRelativeTo(null);

        panel = new JPanel();
        panel.setLayout(new FlowLayout());
       
        textArea = new JTextArea();
        textField = new JTextField();
        button = new JButton();
        textArea.setBounds(100, 100, 250, 250);
        textArea.setColumns(45);   
        textArea.setRows(20);
        textField.setSize(10,50);
        button.setSize(150,150);
        textField.setLocation(100, 200);
        textField.setColumns(10);
        textArea.setVisible(true);
        button.setText("Enviar!");
    }
   
   
    private void eventButton() {
        button.addActionListener(new ActionListener() {
           
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    System.out.println("Missatge Enviat del servidor");
                    salidaCliente.writeUTF(textField.getText() + "\n");
                    textField.setText("");

                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }
    public void mostrarMissatge(String s) {
        text = text + s;
        textArea.setText(text + "\n");
        textArea.setVisible(true);
    }
   
    public  void createConexion() {
       
        try {
            ss = new ServerSocket(PUERTO);
            cs = new Socket();
        } catch (IOException e) {
            e.printStackTrace();
        }
       
    }
   
    public  void startServer() {
         try{
                System.out.println("Esperando...");
               
                cs = ss.accept();

                System.out.println("Cliente en línea");

                salidaCliente = new DataOutputStream(cs.getOutputStream());

                DataInputStream entrada = new DataInputStream(cs.getInputStream());
               
                while((mensajeServidor = entrada.readUTF()) != null) {
                    System.out.println(mensajeServidor);
                    if (mensajeServidor.equals("quit")) {
                        System.out.println("merda");
                        break;
                    }
                    mostrarMissatge(mensajeServidor);
                }

                System.out.println("Fin de la conexión");

                ss.close();
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
    }
   
   
   
   
   
   
   
   
   
   
   
   
   
   

}