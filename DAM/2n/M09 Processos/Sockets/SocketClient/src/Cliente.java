import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Cliente extends JFrame{
   
    protected DataOutputStream salidaServidor;
    protected Socket cs;

    private  JTextArea textArea;
    private  JTextField textField;
    private  JButton button;
    private  JPanel panel;
    private  String missatgeServer = "";
    private  String text;
    private  boolean serverConnected = false;
   
    public Cliente() throws IOException{
        super("client");
        createChat();
        addComponents();
        eventButton();
        button.setEnabled(false);
    }

   
    public void startClient() {
        while (true) {
            try {
                Thread.sleep(1000);
            }catch(Exception e) {
               e.getStackTrace();
            }
           
             try{
                 cs = new Socket("localhost",1234);
                 serverConnected = true;
                 button.setEnabled(true);
                 
                 salidaServidor = new DataOutputStream(cs.getOutputStream());
                 DataInputStream entrada = new DataInputStream(cs.getInputStream());
               
                 while((missatgeServer = entrada.readUTF()) != null) {
                     
                     if (missatgeServer.equals("quit")) {
                            break;
                        }
                         mostrarMissatge(missatgeServer);
                    }
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
           
        }
      
    }
   
    private  void mostrarMissatge(String s) {
        text = text + s;
        textArea.setText(text + "\n");
        textArea.setVisible(true);
    }
   
   
    public  void addComponents () {
        panel.add(textArea);
        panel.add(textField);
        panel.add(button);
        this.add(panel);
        this.setVisible(true);
    }
   
   
   
    public  void createChat() {
        this.setSize(500,500);
        this.setLocationRelativeTo(null);

        panel = new JPanel();
        panel.setLayout(new FlowLayout());
       
        
        textArea = new JTextArea();
        textField = new JTextField();
        button = new JButton();
        textArea.setBounds(100, 100, 250, 250);
        textArea.setColumns(45);   
        textArea.setRows(20);
        textField.setSize(100,50);
        button.setSize(150,150);
        textField.setLocation(100, 200);
        textField.setColumns(10);
        textArea.setVisible(true);
        button.setText("Enviar!");

    }
   
   
    private void eventButton() {
        button.addActionListener(new ActionListener() {
           
            @Override
            public void actionPerformed(ActionEvent e) {
                    try {
                        salidaServidor.writeUTF(textField.getText() + "\n");
                        textField.setText("");
                       
                    } catch (IOException e1) {
                        e1.printStackTrace();
                }       
       
            }
        });
    }
   
   
   
}