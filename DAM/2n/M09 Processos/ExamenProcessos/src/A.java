import java.io.IOException;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TestA {
	
	static int feinasFetas = 0;

	public static void main(String[] args) {
		ScheduledExecutorService ses = Executors.newScheduledThreadPool(3);
		Runnable task = () -> {
			try {
				if(feinasFetas == 50) {
					botonazo(ses);
				}
				TimeUnit.MILLISECONDS.sleep(500);
				generarMecanic(Thread.currentThread().getName());
				feinasFetas++;
			} catch (InterruptedException e) {
				System.err.println("task interrupted");
			}
		};

		int initialDelay = 0;
		int period = 100;
		ses.scheduleAtFixedRate(task, initialDelay, period, TimeUnit.MILLISECONDS);
		ses.scheduleAtFixedRate(task, initialDelay, period, TimeUnit.MILLISECONDS);
		ses.scheduleAtFixedRate(task, initialDelay, period, TimeUnit.MILLISECONDS);
		
	}

	public static void generarMecanic(String nom) {
		String mecanic = "mecanic " + nom;
		Random ran = new Random();
		int feina = ran.nextInt(3);
		ferFeina(mecanic, feina);
	}

	public static void ferFeina(String mecanic, int feina) {
		System.out.println("Mecànic: " + mecanic + ". >>>>> realitza tasca:" + feina);
		if (feina == 2) {
			agafaGat(mecanic);
		}
	}
	public static synchronized void agafaGat(String mecanic) {
		System.out.println("Mecànic: " + mecanic + ". >>>>> Agafar el gat");
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Mecànic: " + mecanic + ". <<<<< Deixar el gat");
	}
	public static void botonazo(ScheduledExecutorService executor) {
		executor.shutdown();
	}
	

}
