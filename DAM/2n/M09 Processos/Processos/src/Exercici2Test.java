
import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertTrue;
//
//import java.util.ArrayList;
//import java.util.List;

import org.junit.Test;

//import cat.castellet.dam1.Exercici2;

public class Exercici2Test {

//	@Test
//	public void testSuma() {
//		assertEquals("7 + 6 és 13", 13, Exercici2.suma(7, 6), 0.0);
//	}

	@Test
	public void testSimple() {
		System.out.print(" TESTSIMPLE { 3, 7, 9, 8 }");
		assertEquals(9, Processos1.MesGran(new int[] { 3, 7, 9, 8 }));
		System.out.println(" .... ok");
	}

	@Test
	public void testOrden() {
		System.out.println(" TESTORDEN");
		System.out.print(" { 9, 7, 8 }");
		assertEquals(9, Processos1.MesGran(new int[] { 9, 7, 8 }));
		System.out.print(" 7,9,8");
		assertEquals(9, Processos1.MesGran(new int[] { 7, 9, 8 }));
		System.out.print(" { 7, 8, 9 }");
		assertEquals(9, Processos1.MesGran(new int[] { 7, 8, 9 }));
	}
	
	@Test
	public void testDuplicados() {
		System.out.print(" testDuplicados");
		assertEquals(9, Processos1.MesGran(new int[] {9, 7, 9, 8}));
		}
		public void testSoloUno() {
			System.out.print("testSoloUno");
		assertEquals(7, Processos1.MesGran(new int[] {7}));
		}
		public void testTodosNegativos() {
			System.out.print("testTodosNegativos");
		assertEquals(-4, Processos1.MesGran(new int[] {-4, -6, -7, 22}));
		}
		
	@Test
	public void testArrayBlanc() {
		System.out.println(" TestArrayBlanc ");
		assertEquals(0, Processos1.multipleDeTres(new int[] {}));
	}
	
	@Test
	public void testArraySenseMultiples() {
		System.out.println("TestArraySenseMultiples");
		assertEquals(0, Processos1.multipleDeTres(new int[] {1, 2, 4}));
	}
	
	@Test
	public void testArrayAmbMultiples() {
		System.out.println("testArrayAmbMultiples");
		assertEquals(1, Processos1.multipleDeTres(new int[] {1, 2, 3}));
	}
	
	//Vectors

	@Test
	public void testarrayNoMultiples() {
		System.out.println("testArrayNoMultiples");
		assertEquals(0, Vectors.MultipleTres(new int[] {1, 2, 4}));
	}
	
	@Test
	public void testarrayMultiplesBlanc() {
		System.out.println("testArrayMultiplesblanc");
		assertEquals(0, Vectors.MultipleTres(new int[] {}));
	}
	
	@Test
	public void testarraySiMultiples() {
		System.out.println("testArraySiMultiples");
		assertEquals(1, Vectors.MultipleTres(new int[] {1, 2, 3}));
	}
	
	//vectors 2
	
	@Test (expected = IllegalArgumentException.class)
	public void test1Null() {
		assertEquals(true, Vectors.equal(null, new int[] {1, 2, 3}));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void test2Null() {
		assertEquals(true, Vectors.equal(new int[] {1, 2, 3}, null));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testTotsNull() {
		assertEquals(true, Vectors.equal(null, null));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testLongDiff() {
		assertEquals(false, Vectors.equal(new int[] {1, 2}, new int[] {1, 2, 3}));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testLongIgual() {
		assertEquals(true, Vectors.equal(new int[] {1, 2, 3}, new int[] {1, 2, 3}));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testContentDiff() {
		assertEquals(false, Vectors.equal(new int[] {1, 2, 4}, new int[] {1, 2, 3}));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testcontentIgual() {
		assertEquals(true, Vectors.equal(new int[] {1, 2, 3}, new int[] {1, 2, 3}));
	}

	
	//Vectors3
	
	  @Test (expected = IllegalArgumentException.class)
	  public void testBNull() {
		  assertEquals(false, Vectors.scalarMultiplication(new int[] {1, 2, 3}, null));
	  }
	  @Test (expected = IllegalArgumentException.class)
	  public void testANull() {
		  assertEquals(false, Vectors.scalarMultiplication(null, new int[] {1, 2, 3}));
	  }
	  @Test (expected = IllegalArgumentException.class)
	  public void testLongDiffs() {
		  assertEquals(true, Vectors.scalarMultiplication(new int[] {1, 2, 3}, new int[] {1, 2}));
	  }
	  @Test (expected = IllegalArgumentException.class)
	  public void testLongEquals() {
		  assertEquals(14, Vectors.scalarMultiplication(new int[] {1, 2, 3}, new int[] {1, 2, 3}));
	  }
	  
	
	
	
	//
	// @Test
	// public void testllista() {
	// List<Integer> list = new ArrayList<Integer>();
	// list.add(42);
	// list.add(-6);
	// list.add(17);
	// list.add(99);
	//
	// //int i = list.get(2);
	// assertEquals(4, list.size());
	// assertEquals(17, (int)list.get(2));
	// assertTrue("Conté un -6",list.contains(-6));
	// assertFalse(list.isEmpty());
	//
	// }
	//
	//
	// @Test
	// public void testAddAndGet1() {
	// List<Integer> list = new ArrayList<Integer>();
	// list.add(42);
	// list.add(-3);
	// list.add(17);
	// list.add(99);
	// assertEquals(42, (int)list.get(0));
	// assertEquals(-3, (int)list.get(1));
	// assertEquals(17, (int)list.get(2));
	// assertEquals(99, (int)list.get(3));
	//
	// assertEquals("second attempt", 42, (int)list.get(0)); // make sure I can get
	// them a second time
	// assertEquals("second attempt", Integer.valueOf(99), list.get(3));
	// }
	//
	// @Test
	// public void testSize1() {
	// List<Integer> list = new ArrayList<Integer>();
	// assertEquals(0, list.size());
	// list.add(42);
	// assertEquals(1, list.size());
	// list.add(-3);
	// assertEquals(2, list.size());
	// list.add(17);
	// assertEquals(3, list.size());
	// list.add(99);
	// assertEquals(4, list.size());
	// assertEquals("second attempt", 4, list.size()); // make sure I can get it a
	// second time
	// }
	//
	// @Test
	// public void testIsEmpty1() {
	// List<Integer> list = new ArrayList<Integer>();
	// assertTrue(list.isEmpty());
	// list.add(42);
	// assertFalse("should have one element", list.isEmpty());
	// list.add(-3);
	// assertFalse("should have two elements", list.isEmpty());
	// }
	//
	// @Test
	// public void testIsEmpty2() {
	// List<Integer> list = new ArrayList<Integer>();
	// list.add(42);
	// list.add(-3);
	// assertFalse("should have two elements", list.isEmpty());
	// list.remove(1);
	// list.remove(0);
	// assertTrue("after removing all elements", list.isEmpty());
	// list.add(42);
	// assertFalse("should have one element", list.isEmpty());
	// }
	//
	//

}
