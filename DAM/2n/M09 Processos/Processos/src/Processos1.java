public class Processos1 {

	public static int MesGran(int lista[]) {
		int indice, max = Integer.MAX_VALUE;

		for (indice = 0; indice < lista.length - 1; indice++) {
			if (lista[indice] > max) {
				max = lista[indice];
			}
		}
		return max;
	}
	public static int multipleDeTres(int[] a) {
		int comptador = 0;
		for (int i = 0; i<a.length; i++) {
			
			if(a[i] % 3 == 0) {
				
				comptador = comptador + 1;
			}
		}
		return comptador;
	}
}
