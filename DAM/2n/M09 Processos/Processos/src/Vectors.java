/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//import java.util.ArrayList;
//import java.util.Iterator;

/**
 * Sample utility class for vector algebra.
 *
 * @author nb
 */
public final class Vectors {
	/**
	 * Rep un array int i escriu per pantalla els elements múltiple de 3 i retorna quants n'ha trobat
	 * @param a
	 * @return
	 */
	public static int MultipleTres(int[] a) {
		int comptador = 0;

		for (int i = 0; i < a.length; i++) {

			if (a[i] % 3 == 0) {
				System.out.println(a[i]);
				comptador = comptador + 1;
			}
		}
		return comptador;
	}	


	/**
	 * Checks whether the given vectors are equal.
	 */
	public static boolean equal(int[] a, int[] b) {
		if ((a == null) || (b == null)) {
			throw new IllegalArgumentException("null argument");
		}

		if (a.length != b.length) {
			return false;
		}

		for (int i = 0; i < a.length; i++) {
			if (a[i] != b[i]) {
				return false;
			}
		}

		return true;
	}
	
	

	/**
	 * Scalar multiplication of given vectors.
	 */
	public static int scalarMultiplication(int[] a, int[] b) {
		if ((a == null) || (b == null)) {
			throw new IllegalArgumentException("null argument");
		}

		if (a.length != b.length) {
			throw new IllegalArgumentException("different tuple of the vectors (" + a.length + ", " + b.length + ')');
		}

		int sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i] * b[i];
		}
		return sum;
	}
	
	
	

}
