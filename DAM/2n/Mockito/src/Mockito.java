
import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertTrue;
//
//import java.util.ArrayList;
//import java.util.List;

import org.junit.Test;
public class Mockito {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public Customer createDummyCustomer() {
		 County county = new County("Essex");
		 City city = new City("Romford", county);
		 Address address = new Address("1234 Bank Street", city);
		 Customer customer = new Customer("john", "dobie", address);
		 return customer;
		}
	
	@Test
	public void addCustomerTest() {
	 Customer dummy = createDummyCustomer();
	 AddressBook addressBook = new AddressBook();
	 addressBook.addCustomer(dummy);
	 assertEquals(1, addressBook.getNumberOfCustomers());
	}
}
