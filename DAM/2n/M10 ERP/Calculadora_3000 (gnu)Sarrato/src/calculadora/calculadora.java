package calculadora;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

public class calculadora extends Shell {
	private Text text;
	private double fact1;
	private String operador;
	private double fact2;
	private double resultat;
	
	Button botoMes, botoMenys, botoX, botoDividir, botoIgual;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Display display = Display.getDefault();
			calculadora shell = new calculadora(display);
			shell.open();
			shell.layout();
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the shell.
	 * @param display
	 */
	
	public calculadora(Display display) {
		super(display, SWT.SHELL_TRIM);
		
		botoIgual = new Button(this, SWT.NONE);
		botoIgual.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				calcular();
				botoMes.setEnabled(false);
				botoMenys.setEnabled(false);
				botoX.setEnabled(false);
				botoDividir.setEnabled(false);
				botoIgual.setEnabled(false);
				
			}
		});
		botoIgual.setBounds(272, 301, 169, 34);
		botoIgual.setText("=");
		
		botoMes = new Button(this, SWT.NONE);
		botoMes.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				operador = "+";
				fact1 = Double.parseDouble(text.getText());
				text.setText("");
				
				botoMes.setEnabled(false);
				botoMenys.setEnabled(false);
				botoX.setEnabled(false);
				botoDividir.setEnabled(false);
				botoIgual.setEnabled(false);
			}
		});
		botoMes.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			
			}
		});
		botoMes.setBounds(275, 261, 80, 34);
		botoMes.setText("+");
		
		botoMenys = new Button(this, SWT.NONE);
		botoMenys.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				operador = "-";
				fact1 = Double.parseDouble(text.getText());
				text.setText("");
				botoMes.setEnabled(false);
				botoMenys.setEnabled(false);
				botoX.setEnabled(false);
				botoDividir.setEnabled(false);
				botoIgual.setEnabled(false);
			}
		});
		botoMenys.setBounds(361, 261, 80, 34);
		botoMenys.setText("-");
		
		botoDividir = new Button(this, SWT.NONE);
		botoDividir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				operador = "/";
				fact1 = Double.parseDouble(text.getText());
				text.setText("");
				botoMes.setEnabled(false);
				botoMenys.setEnabled(false);
				botoX.setEnabled(false);
				botoDividir.setEnabled(false);
				botoIgual.setEnabled(false);
			}
		});
		botoDividir.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		botoDividir.setBounds(361, 221, 80, 34);
		botoDividir.setText("/");
		
		botoX = new Button(this, SWT.NONE);
		botoX.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				operador = "*";
				fact1 = Double.parseDouble(text.getText());
				text.setText("");
				botoMes.setEnabled(false);
				botoMenys.setEnabled(false);
				botoX.setEnabled(false);
				botoDividir.setEnabled(false);
				botoIgual.setEnabled(false);
			}
		});
		botoX.setText("x");
		botoX.setBounds(275, 221, 80, 34);
		
		text = new Text(this, SWT.BORDER);
		text.setBounds(10, 10, 349, 61);
		
		Button boto0 = new Button(this, SWT.NONE);
		boto0.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.setText(text.getText()+"0");
				text.getText();
			}
		});
		boto0.setBounds(54, 301, 80, 34);
		boto0.setText("0");

		Button boto1 = new Button(this, SWT.NONE);
		boto1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				text.setText(text.getText()+"1");
				text.getText();
				botoMes.setEnabled(true);
				botoMenys.setEnabled(true);
				botoX.setEnabled(true);
				botoDividir.setEnabled(true);
				botoIgual.setEnabled(true);
			}
		});
		boto1.setBounds(10, 261, 80, 34);
		boto1.setText("1");
		
		Button boto2 = new Button(this, SWT.NONE);
		boto2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.setText(text.getText()+"2");
				text.getText();
				botoMes.setEnabled(true);
				botoMenys.setEnabled(true);
				botoX.setEnabled(true);
				botoDividir.setEnabled(true);
				botoIgual.setEnabled(true);
			}
		});
		boto2.setText("2");
		boto2.setBounds(96, 261, 80, 34);
		
		Button boto3 = new Button(this, SWT.NONE);
		boto3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.setText(text.getText()+"3");
				text.getText();
				botoMes.setEnabled(true);
				botoMenys.setEnabled(true);
				botoX.setEnabled(true);
				botoDividir.setEnabled(true);
				botoIgual.setEnabled(true);
			}
		});
		boto3.setText("3");
		boto3.setBounds(182, 261, 80, 34);
		
		Button boto4 = new Button(this, SWT.NONE);
		boto4.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.setText(text.getText()+"4");
				text.getText();
				botoMes.setEnabled(true);
				botoMenys.setEnabled(true);
				botoX.setEnabled(true);
				botoDividir.setEnabled(true);
				botoIgual.setEnabled(true);
			}
		});
		boto4.setText("4");
		boto4.setBounds(10, 221, 80, 34);
		
		Button boto5 = new Button(this, SWT.NONE);
		boto5.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.setText(text.getText()+"5");
				text.getText();
				botoMes.setEnabled(true);
				botoMenys.setEnabled(true);
				botoX.setEnabled(true);
				botoDividir.setEnabled(true);
				botoIgual.setEnabled(true);
			}
		});
		boto5.setText("5");
		boto5.setBounds(96, 221, 80, 34);
		
		Button boto6 = new Button(this, SWT.NONE);
		boto6.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.setText(text.getText()+"6");
				text.getText();
				botoMes.setEnabled(true);
				botoMenys.setEnabled(true);
				botoX.setEnabled(true);
				botoDividir.setEnabled(true);
				botoIgual.setEnabled(true);
			}
		});
		boto6.setText("6");
		boto6.setBounds(182, 221, 80, 34);
		
		Button boto7 = new Button(this, SWT.NONE);
		boto7.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.setText(text.getText()+"7");
				text.getText();
				botoMes.setEnabled(true);
				botoMenys.setEnabled(true);
				botoX.setEnabled(true);
				botoDividir.setEnabled(true);
				botoIgual.setEnabled(true);
			}
		});
		boto7.setText("7");
		boto7.setBounds(10, 181, 80, 34);
		
		Button boto8 = new Button(this, SWT.NONE);
		boto8.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.setText(text.getText()+"8");
				text.getText();
				botoMes.setEnabled(true);
				botoMenys.setEnabled(true);
				botoX.setEnabled(true);
				botoDividir.setEnabled(true);
				botoIgual.setEnabled(true);
			}
		});
		boto8.setText("8");
		boto8.setBounds(96, 181, 80, 34);
		
		Button boto9 = new Button(this, SWT.NONE);
		boto9.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.setText(text.getText()+"9");
				text.getText();
				botoMes.setEnabled(true);
				botoMenys.setEnabled(true);
				botoX.setEnabled(true);
				botoDividir.setEnabled(true);
				botoIgual.setEnabled(true);
			}
		});
		boto9.setText("9");
		boto9.setBounds(182, 181, 80, 34);
		
		Button botoPunt = new Button(this, SWT.NONE);
		botoPunt.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.setText(text.getText()+".");
				text.getText();
				botoMes.setEnabled(true);
				botoMenys.setEnabled(true);
				botoX.setEnabled(true);
				botoDividir.setEnabled(true);
				botoIgual.setEnabled(true);
				
			}
		});
		botoPunt.setBounds(140, 301, 80, 34);
		botoPunt.setText(".");
		
		Button botoReset = new Button(this, SWT.NONE);
		botoReset.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				text.setText("");
				text.getText();
			}
		});
		botoReset.setBounds(96, 105, 80, 34);
		botoReset.setText("Reset");
		createContents();
		
		botoMes.setEnabled(false);
		botoMenys.setEnabled(false);
		botoX.setEnabled(false);
		botoDividir.setEnabled(false);
		botoIgual.setEnabled(false);
		
		
	}
	
	
	public void calcular() {
		String a = "";
		fact2 = Double.parseDouble(text.getText());
		if(operador == "+") {
			resultat = fact1 + fact2;
			a = String.valueOf(resultat);
			text.setText(a);
		}
		if(operador == "-") {
			resultat = fact1 - fact2;
			a = String.valueOf(resultat);
			text.setText(a);
		}
		if(operador == "*") {
			resultat = fact1 * fact2;
			a = String.valueOf(resultat);
			text.setText(a);
		}
		if(operador == "/") {
			resultat = fact1 / fact2;
			a = String.valueOf(resultat);
			text.setText(a);
		}
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setText("Calculadora 3000 v0.6 JordiSarrato");
		setSize(457, 374);
 
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
