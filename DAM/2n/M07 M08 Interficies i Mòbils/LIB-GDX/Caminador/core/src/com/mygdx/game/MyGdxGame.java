package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MyGdxGame extends ApplicationAdapter {
	// DEFINICIÓ DE VARIABLES
	private int width; // ample de la pantalla física.
	private int height; // alçada de la pantalla física.
	private OrthographicCamera cam; // La càmera que utilitzarem (amb vista ortogràfica).
	private Sprite personatge; // serà el nostre personatge, aquesta classe tindrà els mètodes i atributs
	// necessaris per tal de poder moure, rotar i escalar de manera més fàcil.
	private SpriteBatch batch; // la classe batch serà l’encarregada de dibuixar en pantalla.
	private Texture textura; // variable per guardar la nostra textura (sprite Sheet).
	private int amplepersona = 43; // Amplada del personatge dins l’sprite sheet
	private int altpersona = 64; // Alçada del personatge dins l’sprite sheet
	private int pas = 0;
	private int canvipas = 0;
	private int sentit = 0;
	private int variable = 30;
	private int speed = 0;
	private boolean sida = true;
	private Sound so = null;
	//FI DEFINICIÓ DE VARIABLES
	@Override
	public void create() {

// El mètode create(), és el PRIMER mètode que es crida al inicialitzar-se el joc
		width = Gdx.graphics.getWidth(); // Obtenim l’ ample de la pantalla.
		height = Gdx.graphics.getHeight(); // Obtenim l’alçada de la pantalla
		cam = new OrthographicCamera(width, height); // Creem una càmera amb un camp de
		//visió igual a l’amplada i l’alçada de la pantalla.
		cam.setToOrtho(false, width, height); // Posicionem la càmera a la meitat de la pantalla.
		//Orientada en sentit Y (1r paràmetre FALSE) utilitzant una visió ortogràfica.
		batch = new SpriteBatch(); // Inicialitzem el nostre dibuixador.
		textura = new Texture("caminador.png"); // carreguem la Textura des d’un PNG situat
		so = Gdx.audio.newSound(Gdx.files.internal("elevator.mp3"));

		//directament dins la carpeta assets
// Definim el comportament a la hora de redimensionar les imatges: NEAREST (pixelat)
		//BILINEAR (difuminat, més lent al dibuixar), REPEATING (repetit)
// El primer paràmetre s’aplica al minimitzar i el segon al maximitzar
		textura.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Linear);
		personatge = new Sprite(textura); //Creem un personatge i li assignem la textura.
		personatge.setSize(amplepersona, altpersona); // Establim les mides del personatge a
		//retallar dins la textura
		personatge.setRegion(pas * amplepersona, altpersona * sentit, (pas + 1) * amplepersona,
				altpersona * (sentit + 1)); // Definim quina part de la textura volem mostrar. En aquest cas
		//serà la que correspon a la posició inicial del personatge ( A dalt a l’esquerra).
// No és necessari si volem mostrar la imatge sencera.
		personatge.setPosition(10, 50); // Posicionem el personatge a la pantalla.
		so.play();
	}

	@Override
	public void dispose() {
// Quan el joc es tanca, cal alliberar tots els recursos que no s’alliberen per defecte
// imatges, dibuixador, música ...
		batch.dispose();
		textura.dispose();
	}

	@Override
	public void render() {

		if(variable>=width) {
			sentit = 1;
			if(sida) {
				speed += 5;
				variable+=speed;
				System.out.println("speed: " + speed);
				sida=false;
			}
		}
		if(variable<=0){
			sentit = 0;
			if(sida==false){
				speed += 5;
				variable+=speed;
				sida=true;
				System.out.println("speed: "+speed);
			}
		}
// Lògica del joc
		canvipas = (canvipas + 1) % 10; //Cada 10 Frames fem un canvi de postura
// Si dividim per 5, el personatge anirà el doble de ràpid
		if (canvipas == 0) {

			if(sentit==1){
				variable = variable - 10 - speed;
			}else {
				//variable+=10;
				variable = variable + 10 + speed;
			}
			pas = (pas + 1) % 6; //Seleccionem la postura del personatge de les 6 que hi ha
// en la primera línia de la textura. Cada vegada que passi per aquest punt pas
			//serà: 1, 2, 3 ... fins a 6 i tornarà a començar.

			personatge.setRegion(pas * amplepersona, altpersona * sentit, amplepersona, altpersona ); // Definim quina part de la textura volem mostrar,
		}
// Fi lògica
		Gdx.gl20.glClearColor(1, 1, 1, 1); // Seleccionem el color blanc de fons de la pantalla
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT); //Pintem el fons amb el color
		//anterior
		batch.begin(); // Abans de dibuixar, hem de cridar al dibuixador
		batch.draw(personatge, variable , 10); // Pintem el personatge en unes coordenades
		//determinades
		batch.end(); // Tanquem el dibuixador
/* ALGUNS EXEMPLES MÉS ******
Podriem situar i dibuixar el personatge també de la següent manera
personatge.setX(personatge.getX()+1);
personatge.draw(batch);
Si volem rotar el personatge: personatge.setRotation(90);
Podem assignar la càmera que volem utilitzar al dibuixador. Si no assignem càmera
a leshores utilitzarà per defecte l’escenari.
batch.setProjectionMatrix(cam.combined);
******************************
*/
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}