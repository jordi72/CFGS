<?php
session_start();
include 'conx.php';

if(isset($_SESSION['username'])) {
?>
<!DOCTYPE html>
<!Developer - Ererret>
<!v 0.1.1>
<html lang="es">
  <head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TigalSoft - Una Empresa de Gamers</title>
    <meta name="description" content="Una empresa dedicada exclusivamente a el desarrollo de juegos 3D de alta calidad, del estilo de juego MMORPG.">
    <meta name="keywords" content="mmorpg, MMORPG, Tigalsoft, tigalsoft, MythofTitans, themythoftitans, tigalsoft mmorpg, tigalsoft mythoftitans, tigalsoft mythoftitans mmorpg">
    <meta name="author" content="Ererret">
    
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">

    <!-- Slider
    ================================================== -->
    <link href="css/owl.carousel.css" rel="stylesheet" media="screen">
    <link href="css/owl.theme.css" rel="stylesheet" media="screen">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">

    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="js/modernizr.custom.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
    <!-- Navigation
    ==========================================-->
    <nav id="tf-menu" class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://www.tigalsoft.esy.es/home.html">TigalSoft</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#tf-home" class="page-scroll">Inicio</a></li>
            <li><a href="#tf-about" class="page-scroll">Nuestros Juegos</a></li>
            <li><a href="#tf-team" class="page-scroll">Trabajo a Realizar</a></li>
            <li><a href="#tf-services" class="page-scroll">Servicios</a></li>
			<li><a href="#tf-clients" class="page-scroll">Changelog</a></li>
            <li><a href="http://tigalsoft.esy.es/community" class="page-scroll">Comunidad</a></li>
			<div class="btn-group">
				<button type="button" class="btn btn-default dropdown-toggle"
					data-toggle="dropdown">
					Sesion <span class="caret"></span>
				</button>
			<ul class="dropdown-menu" role="menu">
				<li><a href="logout.php">Cerrar Sesion</a></li>
			</ul>
			</div>


				
</select>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <!-- Home Page
    ==========================================-->
    <div id="tf-home" class="text-center">
        <div class="overlay">
		    <div class="content">
                <h1>Bienvenido a <strong><span class="color">TigalSoft</span></strong></h1>
                <p class="lead">Somos una empresa joven de videojuegos.</p>
                <a href="#tf-about" class="fa fa-angle-down page-scroll"></a>
            </div>
        </div>
    </div>

    <!-- About Us Page
    ==========================================-->
    <div id="tf-about">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="img/03.png" class="img-responsive">
                </div>
                <div class="col-md-6">
                    <div class="about-text">
                        <div class="section-title">
                            <h4>Nuestros Juegos</h4>
                            <h2><strong>sobre nuestros juegos</strong></h2>
                            <hr>
                            <div class="clearfix"></div>
                        </div>
                        <p class="intro">Todos nuestros juegos, tienen un toque de fantas&iacute;a, lo que los hace m&aacute;s entretenidos y a la vez divertidos.</p>
                        <ul class="about-list">
                            <li>
                                <span class="fa fa-dot-circle-o"></span>
                                <em>Actualmente estamos en fase de desarrollo, por lo cual a&uacute;n no tenemos ning&uacute;n juego lanzado.</em>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Team Page
    ==========================================-->
    <div id="tf-team" class="text-center">
        <div class="overlay">
            <div class="container">
                
                    <h1><strong>Trabajo a Realizar</strong></h1><br>
                    <div class="line"> </div>
					<h4> 1.- Crear Juego Principal.</h4>
					<h5> 1.1.- Crear el Codigo Fuente.</h5>
					<h5> 1.2.- Crear las leyes de la fisica basicas del juego, tales como la gravedad.</h5>
					<h5> 1.3.- Crear primeros modelos 3D primitivos.</h5>
					<h5> 1.4.- Crear las primeras zonas de subida.</h5>
					<h5> 1.5.- Texturizar las zonas medianamente finalizadas.</h5>
					<h6>Esta lista se ir&aacute; ampliando pr&oacute;ximamente.</h6>
			</div>    
		</div>
    </div>
    

    <!-- Services Section
    ==========================================-->
    <div id="tf-services" class="text-center">
        <div class="container">
            <div class="section-title center">
                <h2>Hecha un vistazo a <strong>nuestros servicios</strong></h2>
                <div class="line">
                    <hr>
                </div>
                <div class="clearfix"></div>
                <small><em><strong>TODOS</strong> nuestros servicios son <strong>TOTALMENTE</strong> gratuitos. Con opci&oacute;n a compras dentro el juego.</em></small>
            </div>
            <div class="space"></div>
            <div class="row">
                <div class="col-md-3 col-sm-6 service">
                    <i class="fa fa-desktop"></i>
                    <h4><strong>Juegos para PC</strong></h4>
                    <p>En nuestra p&aacute;gina ofrecemos un launcher, para poder descargar los juegos m&aacute;s f&aacute;cilmente, y vincularlos a tu cuenta.</p>
                </div>

                <div class="col-md-3 col-sm-6 service">
                    <i class="fa fa-desktop"></i>
                    <h4><strong>Seguridad de calidad</strong></h4>
                    <p>Nuestro launcher lleva un sistema de seguridad mediante el cual solo se pueden descargar juegos, estando conectado a la cuenta, y introduciendo un c&oacute;digo personal de seguridad.</p>
                </div>

                <div class="col-md-3 col-sm-6 service">
                    <i class="fa fa-mobile"></i>
                    <h4><strong>Apps para m&oacute;viles</strong></h4>
                    <p>Nuestras apps se pueden descargar de PlayStore y AppleStore gratuitamente.</p>
                </div>
				 <div class="col-md-3 col-sm-6 service">
                    <i class="fa fa-mobile"></i>
                    <h4><strong>Seguridad de calidad</strong></h4>
                    <p>Nuestros juegos requieren de una vinculac&oacute;n con una cuenta de google, para poder proceder a jugar, as&iacute; aumentamos la seguridad.</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Changelog Section
    ==========================================-->
    <div id="tf-clients" class="text-center">
        <div class="overlay">
            <div class="container">
                
                    <h1><strong>Changelog (Lista de cambios)</strong></h1><br>
                    <div class="line"> </div>
					<li><h3><u>27/08/2016 </u></h3></li>
					<li><h3> Página de Registro actualizada v 0.5</h3></li>
					<li><h3> Página de Inicio de Sesion actualizada v 0.5</h3></li>
					<li><h3> Páginas de Inicio de Sesion y Registro actualizadas de MySQL a MySQLi</h3></li>
					<li><a href="http://tigalsoft.esy.es/changelog.html">Changelog Completo.</a></li>
			</div>    
		</div>
    </div>
	
	<!-- Zona de copyright -->
	
    <nav id="footer">
        <div class="container">
            <div class="pull-left fnav">
                <p>TODOS LOS DERECHOS RESERVADOS. COPYRIGHT © 2016. Hecho por Ererret</p>
            </div>
            <div class="pull-right fnav">
                <ul class="footer-social">
                    <li><a href="https://www.facebook.com/tigalsoft"><i class="fa fa-facebook"></i></a></li>
					<li><a href="https://www.twitter.com/tigalsoft"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCgFRMMWP_tH0R7ZdM-D-uyg"><i class="fa fa-youtube"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>					
					<li><a href="https://www.twitch.tv/tigalsoft"><i class="fa fa-twitch"></i></a></li>				   
                </ul>
            </div>
        </div>
    </nav>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.1.11.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/SmoothScroll.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.js"></script>

    <script src="js/owl.carousel.js"></script>

    <!-- Javascripts
    ================================================== -->
    <script type="text/javascript" src="js/main.js"></script>

  </body>
</html>
<?php
}else{
	echo '<script> window.location="index.php"; </script>';
}
?>              