<?php 
session_start();
	require("conf_log_env.php");
	if(isset($_SESSION['username'])){
	echo '<script> window.location="home.php"; </script>';
	}
?>
<head>
	<title> Inicia Sesion en TigalSoft </title>
	<meta charset="UTF-8">
	<meta name="author" content="TigalSoft">
	<meta name="description" content="Pagina de inicio de sesion, TigalSoft.">
	<!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">
	<!-- CSS
	================================================== -->
	<link href="css/login_style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<section id="formulario">
		<p id="titulo">Inicia Sesion</p>
    
        <form action='conf_log_env.php' method='POST'>
			<input type="text" id="username" name="username" maxlength="32" placeholder="Nombre de Usuario">
			<input type="password" id="password" name="password" maxlength="16" placeholder="Contrase&ntilde;a">
			<input type="submit" name="login" value="Iniciar Sesion">
		</form>
		
		<form method="POST" action="/registro.php">
			<button type="submit">¿No Tienes Cuenta?</button>
		</form>
		<form method="POST" action="/index.html">
			<button type="submit">Página Principal de TigalSoft</button>
		</form>
	</section>
</body>