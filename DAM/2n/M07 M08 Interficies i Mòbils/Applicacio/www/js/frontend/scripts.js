$(document).ready(function(){
    var lightbox = lity();

    if($('body.homepage').length && images.length >0 ){
        /****** Image background slider ******/
        var i = 0;
        $('.header-wrapper.homepage .header-texte-title span').text(titles[i]);
        $('.header-wrapper.homepage .header-texte-content span').text(textes[i]);

        var sliderInterval = 0;
        setTimeout(
            function()
            {
                $('.header-wrapper.homepage .ht').animate({opacity:1}, 500);
                $('.header-wrapper.homepage .ht').removeClass('invisible');
                $('#slider-nav').removeClass('invisible');
                startSlider(i);
                jQuery('.header-wrapper.homepage #slider-nav .nav-dots div').click(function(){
                    i= parseInt($(this).attr('id'));
                    clearInterval(sliderInterval);
                    changeBackground(i);
                    startSlider(i);
                    //setTimeout(function() {}, 5000);
                });
                preload(images);
            }, 5000);

        function preload(arrayOfImages) {
            $(arrayOfImages).each(function(){
                $('<img/>')[0].src = this;
            });
        }

        function startSlider(i){
            changeBackground(i);
            sliderInterval = setInterval(function () {
                i++;
                if (i == images.length) {
                    i = 0;
                }
                changeBackground(i);
            }, 8000);
        }

        function changeBackground(i){
            $('.header-wrapper.homepage #slider-nav .nav-dots div.active').removeClass('active');
            $('.header-wrapper.homepage #slider-nav .nav-dots div:nth-child('+(i+1)+')').addClass('active');
            $('.header-wrapper.homepage').css({
                'background-image' : "url('"+images[i]+"')",
                //'background' : "url('"+images[i]+"') center center no-repeat",
                //'-webkit-background-size' : "cover",
                //'background-size' : "cover",
                'transition' : 'background-image 1s ease-in-out'
            });
            $('.header-wrapper.homepage .logo img').animate({opacity:0}, 500, function(){
                $('.header-wrapper.homepage .logo img').attr('src', './images/header/titans_logo_' + extensions[i] + '.png');
                $('.header-wrapper.homepage .logo img').animate({opacity:1}, 500);
            });

            $('.header-wrapper.homepage .header-texte').animate({opacity:0}, 500, function(){
                $('.header-wrapper.homepage .header-texte-title span').text(titles[i]);
                $('.header-wrapper.homepage .header-texte-content span').text(textes[i]);
                $('.header-wrapper.homepage .header-texte').animate({opacity:1}, 500);
            });

        }
        /****** END Image background slider ******/
    }

    /****** Ouverture/Fermeture login form ******/
    var container = $('.show-dropdown');
    var windowWidth = $('body').width();
    var menuResponsive = $('#frontend-navbar-collapse');
    var mouseOverActiveElement = false;

    adaptNavbars();
    $(window).resize(function(){
        windowWidth = $('body').width();
        adaptNavbars();
    });

    if(!container.hasClass('dropdown-button')) {
        container.click(function (e){
            if(container.hasClass('active')){
                $('.header-wrapper form.form-login').css('display', 'none');
                container.removeClass('active');
            }
            else{
                container.addClass('active');
                $('.header-wrapper form.form-login').css({
                    'display' : 'block'
                });
                // Ouverture / Fermeture si ecran < 514px
                if(windowWidth < 514) {
                    menuResponsive.css('height', '0px');
                    menuResponsive.attr('aria-expanded', 'false');
                    menuResponsive.removeClass('in');
                }
            }

        });

        // Vérifie si on clique en dehors du menu
        container.on('mouseenter', function(){
            mouseOverActiveElement = true;
        }).on('mouseleave', function(){
            mouseOverActiveElement = false;
        });
        $('#login-wrapper').on('mouseenter', function(){
            mouseOverActiveElement = true;
        }).on('mouseleave', function(){
            mouseOverActiveElement = false;
        });
        $("html").click(function(){
            if (!mouseOverActiveElement) {
                $('.header-wrapper form.form-login').css('display', 'none');
                container.removeClass('active');
            }
        });
    }
    /****** END Ouverture/Fermeture login form ******/

    /***** Affichage de la modale de fuision de compte ****/
    $('#mergeModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var extension = button.data('extension');
        var modal = $(this);
        var url = modal.data('ajaxurl');
            $.ajax({
            method: "GET",
            url: url,
            data: { extension: extension }
        })
        .done(function( data ) {
            $('.modal-body', modal).html(data);
        });

        modal.find('.modal-title .account_number').text(extension);

    });

    /***** Affichage de la modale de détail d'une commande ****/
    $('#orderModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var order = button.data('order');
        var modal = $(this);
        var url = modal.data('ajaxurl');
        $.ajax({
            method: "GET",
            url: url,
            data: { order: order }
        })
            .done(function( data ) {
                $('.modal-body', modal).html(data);
            });

        modal.find('.modal-title .order_id').text(order);

    });


    /*filtre des actus */
    $('.actu-menu .actu').click(function(){
        var actu = $(this);
        var url = actu.parent().data('ajaxurl');
        var extension = actu.data('extension');
        $('#actu-content').animate({opacity:0}, 500);
        $('.actu-menu .actu.active').removeClass('active');
        $.ajax({
            method: "GET",
            url: url,
            data: { extension: extension }
        })
            .done(function( data ) {
                actu.addClass('active');
                $('#actu-content').html(data);
                $('#actu-content').animate({opacity:1}, 500);

            });
    });


    $('#createModal').on('hide.bs.modal', function (event) {
        window.location.reload();
    });

    /* définition d'un personnage en personnage principal */
    $('.characters .update_main_character').click(function(){
        if($(this).hasClass('fa-star-o')){

            var character_id = $(this).data('character');
            var url = $('.characters').data('ajaxurl');
            var character = $('#character_'+character_id);
            $.ajax({
                method: "GET",
                url: url,
                data: { character: character_id }
            })
            .done(function( data ) {
                if(data.status){
                    $('.characters .character_name i.fa.fa-star').addClass('fa-star-o');
                    $('.characters .character_name i.fa.fa-star').removeClass('fa-star');

                    $('.character_name i.fa', character).addClass('fa-star');
                    $('.character_name i.fa', character).removeClass('fa-star-o');
                    $('.ajax-info', character).text(data.message);
                    $('#sommaire').html(data.summary_view);
                    toggleAjaxInfo($('.ajax-info', character), 'success')
                }else{
                    toggleAjaxInfo($('.ajax-info', character), 'error')
                }
            });
        }
    });

    /* effacement automatique du message d'alerte */
    if($('#alert-messages .alert').length){
        hideErrorMessage();
    }

    $('form.form-login').submit(function(e){

        e.preventDefault();
        var form = $(this);
        var data = $(this).serialize();
        var url = $(this).data('ajaxurl');

        $.ajax({
            method: "POST",
            url: url,
            data: data
        })
            .done(function(data) {
                if(data.userIsLogIn){
                    form.unbind('submit').submit();
                    return true;
                }else{
                    addErrorMessage(data.message, 'error');
                }
            });
    });

    /* Clic sur le bouton de vote */
    $('.vote-site-vote-button').click(function(e){
        var site_id = $(this).data('vote-site');
        var url = $(this).data('ajaxurl');
        var button = $(this);
        //e.preventDefault();
        $.ajax({
            method: "POST",
            url: url,
            data: {'site_id':site_id}
        })
            .done(function(data) {
                if(data.status){
                    addErrorMessage(data.message, 'success');
                    button.hide();
                    button.parent().append('<span class="next-vote-in">'+data.textbutton+'</span>')
                    $('.storepoints').html(data.points)
                }else{
                    addErrorMessage(data.message, 'error');
                }
            })
    });

    $('.redirect').click(function(){
        var redirectUrl = $(this).data('redirect');
        if(redirectUrl != 'undefined'){
            setTimeout(function () {
                document.location = redirectUrl;
            }, 1000);
        }
    });

    $('#convertVotesModal .convert-vote-points').click(function(){
        var action = $(this).data('action');
        var extension = $(this).data('extension');
        var url = $('#convertVotesModal form').data('ajaxurl');
        console.log(action, extension);
        $.ajax({
            method: "POST",
            url: url,
            data: {'action':action, 'extension':extension}
        })
            .done(function(data) {
                if(data.status){
                    $('#new_extension_credits_'+extension).text('');
                    if(data.credits_add > 0){
                        $('#new_extension_credits_'+extension).text('+'+data.credits_add+'pts');
                    }
                    $('#convertVotesModal .storepoints').text(data.votes_points_sum);
                }else{
                    addErrorMessage(data.message, 'error');
                }
            });
    })

    /* Changement des catégories de forums
    $('.forum-page .forum-menu .category').click(function(){
        var category = $(this);
        var url = category.parent().data('ajaxurl');
        var extension = category.data('extension');
        $('.forum-menu .category.active').removeClass('active');
        $.ajax({
            method: "GET",
            url: url,
            data: { extension: extension }
        })
            .done(function(data) {
                category.addClass('active');
                $('#forum-content').html(data);
            });
    });*/

    //$('.forum-page .forums-langs-switcher').bootstrapToggle('on');

    $('.forums-langs-switcher').change(function(){
        var extension = $('.forum-menu .category.active').data('extension');
        loadForums(extension);
    });

    //search
    AutoComplete({
        _Select: function(item) {
            console.log($(item).attr('data-autocomplete-value'));
            window.location.href = $(item).attr('data-autocomplete-value');
        }
    });


    /***** Affichage de la modale d'achat rapide ****/
    $('#quickShopModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var product = button.data('product');
        var modal = $(this);
        var url = modal.data('ajaxurl');
        $.ajax({
                method: "GET",
                url: url,
                data: { product: product }
            })
            .done(function( data ) {
                $('.modal-body', modal).html(data);
            });


    });


    /***** Affichage de la modale d'achat de crédits ****/
    $('#addCreditsModal').on('show.bs.modal', function (event) {
        var modal = $(this);
        var url = modal.data('ajaxurl');
        console.log(url);
        $.ajax({
                method: "GET",
                url: url
            })
            .done(function( data ) {
                $('.modal-body', modal).html(data.html);
            });
    });


    $('#addCreditsModal').on('hide.bs.modal', function (event) {
        $('.modal-body', this).html('');
    });


    $('#showCreditMenu').on('shown.bs.dropdown', function () {
        var creditMenu = $('#credits-menu');
        var creditMenuWidth = 0;

        $('.extensions .extension', creditMenu).each(function(i){
            console.log(i, $(this).outerWidth());
            creditMenuWidth = creditMenuWidth + $(this).outerWidth()+10;
        });
        creditMenu.css({width:creditMenuWidth+'px'});
    });

    /* ouverture auto des onglets en fonction de l'ancre dans l'url */
    // Javascript to enable link to tab
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }

    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        var scrollmem = $('html,body').scrollTop();
        window.location.hash = e.target.hash;
        $('html,body').scrollTop(scrollmem);
    });

    //blocage du scroll si une ancre est présente dans l'url
    if (location.hash) {
        setTimeout(function() {
            window.scrollTo(0, 0);
        }, 1);
    }

    /***** Affichage de la modale de modération de message sur le forum ****/
    $('#moderateMessage').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var message = button.data('message');
        var modal = $(this);
        var url = modal.data('ajaxurl');
        $.ajax({
            method: "GET",
            url: url,
            data: {message:message}
        })
            .done(function( data ) {
                $('.modal-body', modal).html(data);
            });
    });
});

//chargement des forums par langues
function loadForums(extension){
    var url = $('.forum-menu').data('ajaxurl');
    var kingdom = $('.forum-menu').data('kingdom');
    $.ajax({
        method: "GET",
        url: url,
        data: {
            extension: extension,
            kingdom: (kingdom == 'undefined')?null:kingdom,
            fr : $('#fr').prop('checked'),
            en : $('#en').prop('checked'),
            es : $('#es').prop('checked')
        }
    })
        .done(function(data) {
            $('.forum-menu .category[data-extension='+extension+']').addClass('active');
            $('#forum-content').html(data);
        });
}


/*
    Enregistre le statut de l'inscription à la newsletter
 */
function saveNewsletterRegistration(field){
    var newsletter = $(field).is(':checked')?1:0;
    var url = $(field).data('ajaxurl');
    $.ajax({
        method: "GET",
        url: url,
        data: { newsletter: newsletter }
    })
    .done(function( data ) {
        var ajaxInfo = $(field).parent().find('.ajax-info');
        toggleAjaxInfo(ajaxInfo, 'success');
    });
}

/*
    Affiche et cache le message de confrmation après une action ajax
 */
function toggleAjaxInfo(el, status, keepShowing){
    el.attr('class', 'ajax-info');
    el.addClass(status);
    if(keepShowing == 'undefined' || keepShowing == false){
        el.animate({opacity:0}, 5000, function(){
            el.addClass('hide');
            el.removeClass(status);
            el.css({opacity: 1});
        });
    }
}

function activaTab(tab){
    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
}

var messagesQueu = [];
var runMessageInterval = false;

function addErrorMessage(message, type){
    var html = '<div class="alert alert-danger alert-dismissible fade in" role="alert" >';
    if(type == 'success'){
        html = '<div class="alert alert-success alert-dismissible fade in" role="alert" >';
    }
    html += '<div class="container">';
    html += '<button type=button class=close data-dismiss=alert aria-label=Close><span aria-hidden=true>&times;</span></button>';
    html += message;
    html += '</div></div>';

    //ajout du message dans la file d'attente
    messagesQueu.push(html);
    //on lance la boucle uniquement si elle n'est pas déjà lancée
    if(!runMessageInterval){
        runMessages();
    }

}

//gestion de la boucle d'affiche des messages
function runMessages(){
    //si on a au moins un message dans la liste
    if(messagesQueu.length > 0) {
        showMessage();
        if(messagesQueu.length > 0) {
            messagesQueu.splice(0, 1);
            if(!runMessageInterval) {
                runMessageInterval = setInterval(runMessages, 5000);
            }
        }
    }
    else{
        clearInterval(runMessageInterval);
        runMessageInterval = false;
    }
}
 function showMessage(){
     var html = messagesQueu[0];
     $('#alert-messages').html(html);
     $('#alert-messages').css({
         top: '-'+$('#alert-messages').height()+'px',
         opacity:1
     });

     $('#alert-messages').animate({
         top: 0,
         opacity:1
     }, 300, function(){
         hideErrorMessage();
     });
}

function hideErrorMessage(){
    $('#alert-messages').delay(3000).animate({
        top: '-'+$('#alert-messages').height()+'px',
        opacity:0
    }, 600, function(){
        $('#alert-messages').html('');
    });
}
function adaptNavbars(){
    windowWidth = $('body').width();
    var leftNavWidth = $('#header .nav-wrapper .links.left').width();
    var rightNavWidth = $('#header .nav-wrapper .links.right').width();

    if(leftNavWidth > rightNavWidth){
        var adjustLiWidth = $('#header .nav-wrapper .links.right .adjustLi').width();
        var newAdjustLiWidth = adjustLiWidth+(leftNavWidth-rightNavWidth);

        $('#header .nav-wrapper .links.right .adjustLi').width(newAdjustLiWidth)
    }
    if(leftNavWidth < rightNavWidth){
        var adjustLiWidth = $('#header .nav-wrapper .links.right .adjustLi').outerWidth();
        var newAdjustLiWidth = adjustLiWidth-(rightNavWidth-leftNavWidth);
        if(newAdjustLiWidth > 0){
            $('#header .nav-wrapper .links.right .adjustLi').outerWidth(newAdjustLiWidth)
        }

    }
}

function submitExtensionFormForCredits(){

        var form = $('#selectPaymentExtension');
        var data = $(form).serialize();
        var url = $(form).data('ajaxurl');
        $.ajax({
            method: "POST",
            url: url,
            data: data
        })
            .done(function(data) {
                if(data.html){
                    $('#addCreditsModal .modal-body').html(data.html);
                }else{
                }
            });
        return false;

}

function loadPaymentWallWidget(){

    var form = $('#selectPaymentExtension');
    var data = $(form).serialize();
    var url = $(form).data('ajaxurlpaymentwall');

    console.log(url, data);
    $.ajax({
        method: "POST",
        url: url,
        data: data
    })
        .done(function(data) {
            if(data.html){
                $('#addCreditsModal .modal-body').html(data.html);
            }else{
            }
        });
    return false;

}

//function loadPaymentForm(event, form){
//    event.preventDefault();
//    var data = $(form).serialize();
//    var url = $(form).attr('action');
//
//    $.ajax({
//        method: "GET",
//        url: url,
//        data: data
//    })
//        .done(function(data) {
//            console.log(data.status, data.html);
//            if(data.status){
//                $('#addCreditsModal .modal-body').html(data.html);
//            }else{
//                addErrorMessage(data.message, 'error');
//            }
//        });
//}