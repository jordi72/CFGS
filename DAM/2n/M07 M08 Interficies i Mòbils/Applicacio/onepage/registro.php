<?php
$link = @mysqli_connect(
'sql302.byethost18.com',
'b18_21016040',
'tornado12',
'b18_21016040_users');
if(!$link){
echo '<p>Error al conectar amb la base de dades: '
. mysqli_connect_error();
echo '</p>';
exit;
}
// Preguntaremos si se han enviado ya las variables necesarias
    if (isset($_POST["username"])) {
    $username = $_POST["username"];
    $password = $_POST["password"];
    $cpassword = $_POST["cpassword"];
    $email = $_POST["email"];
	  $respostaS = $_POST["respostaS"];
    // Hay campos en blanco
    if($username==NULL|$password==NULL|$cpassword==NULL|$email==NULL|$respostaS==NULL) {
    echo "<center>Algún campo está vacio.</center>";
    }else{
    // ¿Coinciden las contraseñas?
    if($password!=$cpassword) {
    echo "<center>Las contraseñas no coinciden</center>";
    }else{
    // Comprobamos si el nombre de usuario o la cuenta de correo ya existían
	$sentenciauser = "SELECT username FROM users WHERE username='$username'";
    $checkuser = mysqli_query($link, $sentenciauser);
    $username_exist = mysqli_num_rows($checkuser);

	$sentenciamail = "SELECT email FROM users WHERE email='$email'";
    $checkemail = mysqli_query($link, $sentenciamail);
    $email_exist = mysqli_num_rows($checkemail);

    if ($email_exist>0|$username_exist>0) {
    echo "<center>EL nombre de usuario o la cuenta de correo ya existen<center>";
    }else{
    //Todo parece correcto procedemos con la inserccion
	$passwordSha256 = sha256($password);
	$respostaSSha256 = sha256($respostaS);
    mysqli_query($link, "INSERT INTO users (username, password, email, respostaS) VALUES('$username','$passwordSha256','$email','$respostaS')");
    echo "<bottom><center>El usuario $username ha sido registrado de manera satisfactoria.</center></bottom>";
    header("Location: /OnePage.html");
    }
	}
    }
    }
?>
<html>
<head>
	<!-- Author: TigalSoft -->
	<meta charset="UTF-8">
	<title>TigalSoft Registro</title>
	<meta name="author" content="TigalSoft">
	<meta name="description" content="Pagina de registro de TigalSoft.">
	<!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

    <!-- Necesarios
    ================================================== -->
	<link rel="stylesheet" href="/css/registro_style.css">
</head>
<body>
	<section id="formulario">
		<p id="titulo">Benvingut</p>

		<form action="registro.php" method="POST">
			<input type="text" id="nombre" name="username" maxlength="32" placeholder="Nom d'usuari">
			<input type="mail" id="mail" name="email" maxlength="320" placeholder="Direcci&oacute; d'e-mail">
			<input type="password" id="pass" name="password" maxlength="16" placeholder="Contrasenya">
			<input type="password" id="cpass" name="cpassword" maxlength="16" placeholder="Confirmar Contrasenya">
				<div class="control-group">
					<div class="row-secret-question">
						<div class="mobile-arrow"></div>
							<select class="grid-100" id="question1" name="question1" required="required">
								<option value="">Selecciona una Pregunta</option>
								<option value="19">Quin animal va ser la teva primera mascota?</option>
								<option value="20">A quin carrer vivias de petit?</option>
								<option value="21">A quina ciutat vaz volar per primera vegada?</option>
								<option value="22">Quin es el teu joc preferit?</option>
								<option value="23">Com es diu la teva mascota?</option>
								<option value="24">Quin es el teu esport preferit?</option>
							</select>
							<span id="question1-error-inline" class="help-block"></span>
						</div>
					</div>
				<div class="control-group row-answer1">
				<input type="text" id="respostaS" name="respostaS" value="" placeholder="Resposta Secreta" maxlength="40" autocapitalize="off" autocomplete="off" autocorrect="off" class="grid-100" required="required" spellcheck="false" /> <span id="answer1-error-inline" class="help-block">
				</span>
				</div>
				<form onsubmit="return checkForm(this);">
				<p><input type="checkbox" required="required" name="terms"> Accepto la <u><a href="#" target="blank" >Politica de Privacitat</a></u></p>
				<p><input type="submit" name="Crear Cuenta" value="Crear Cuenta"></p>
				</form>
				<form method="POST" action="/Applicacio/onepage/login.php">
				<button type="submit">Ja tens un compte?</button>
				</form>
		</form>
	</section>
</body>
