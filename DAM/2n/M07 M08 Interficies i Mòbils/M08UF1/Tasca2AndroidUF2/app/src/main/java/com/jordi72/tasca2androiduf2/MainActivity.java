package com.jordi72.tasca2androiduf2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Animation animacio ;
    private TextView vista;
    private ImageView imatge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vista = (TextView) findViewById(R.id.runningStop);
        imatge = (ImageView) findViewById(R.id.imatge);
    }

    public void fadeIn(View v){
        animacio = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fadein);
        long duracio = 1000;
        animacio.setDuration(duracio);
        imatge.startAnimation(animacio);
    }
    public void fadeOut(View v){
        animacio = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fadeout);
        long duracio = 1000;
        animacio.setDuration(duracio);
        imatge.startAnimation(animacio);
    }
    public void zoomIn(View v){
        animacio = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.zoomin);
        long duracio = 1000;
        animacio.setDuration(duracio);
        imatge.startAnimation(animacio);
    }
    public void zoomOut(View v){
        animacio = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.zoomout);
        long duracio = 1000;
        animacio.setDuration(duracio);
        imatge.startAnimation(animacio);
    }
    public void leftRight(View v){
        animacio = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.leftright);
        long duracio = 1000;
        animacio.setDuration(duracio);
        imatge.startAnimation(animacio);
    }
    public void topBottom(View v){
        animacio = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.topbottom);
        long duracio = 1000;
        animacio.setDuration(duracio);
        imatge.startAnimation(animacio);
    }
    public void bounce(View v){
        animacio = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.bounce);
        long duracio = 1000;
        animacio.setDuration(duracio);
        imatge.startAnimation(animacio);
    }
    public void flash(View v){
        animacio = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.flash);
        long duracio = 1000;
        animacio.setDuration(duracio);
        imatge.startAnimation(animacio);
    }
    public void rotate(View v){
        animacio = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate);
        long duracio = 1000;
        animacio.setDuration(duracio);
        imatge.startAnimation(animacio);
    }
    public void several(View v){
        animacio = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.several);//change to several
        long duracio = 1000;
        animacio.setDuration(duracio);
        imatge.startAnimation(animacio);
    }
}
