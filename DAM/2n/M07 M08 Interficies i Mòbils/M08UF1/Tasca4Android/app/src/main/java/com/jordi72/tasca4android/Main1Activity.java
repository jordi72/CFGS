package com.jordi72.tasca4android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Main1Activity extends AppCompatActivity {

    private Button boto;
    private TextView retorn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        retrieveViews();
        setEvents();
        retorn.setText(getIntent().getStringExtra("keyRetorn"));

    }

    private void retrieveViews(){
        boto = findViewById(R.id.boto);
        retorn = findViewById(R.id.textReton);
    }

    private void setEvents(){
        boto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main1Activity.this, Main2Activity.class);
                startActivityForResult(i, 1);
            }
        });

    }
}
