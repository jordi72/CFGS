package com.jordi72.tasca4android;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import org.w3c.dom.Text;

public class Main2Activity extends Activity {
        private Spinner spinner;
        private Button botoTornar;
        private String info;


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main2);
            spinner = (Spinner) findViewById(R.id.spinner);
            ArrayAdapter adapter = ArrayAdapter.createFromResource(this,
                    R.array.intents, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            retrieveViews();
            setEvents();
        }

        private void retrieveViews(){
            botoTornar = findViewById(R.id.tornar);
        }

        private void setEvents(){
            botoTornar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Main2Activity.this, Main1Activity.class);
                    i.putExtra("keyRetorn", info);
                    startActivity(i);
                }
            });
        }


        public void onClick(View view) {
            int position = spinner.getSelectedItemPosition();
            Intent intent = null;
            switch (position) {
                case 0:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://www.vogella.com"));
                    info = "Internet";
                    break;
                case 1:
                    intent = new Intent(Intent.ACTION_DIAL,
                            Uri.parse("tel:(+49)12345789"));
                    info = "Telefon";
                    break;
                case 2:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("geo:50.123,7.1434?z=19"));
                    info = "Mapa";
                    break;
                case 3:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("geo:0,0?q=query"));
                    info = "Mapa";
                    break;
                case 4:
                    intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    info = "Camara";
                    break;
                case 5:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("content://contacts/people/"));
                    info = "Agenda de COntactes";
                    break;
                case 6:
                    intent = new Intent(Intent.ACTION_EDIT,
                            Uri.parse("content://contacts/people/1"));
                    info = "Primer Contacte";
                    break;

            }
            if (intent != null) {
                startActivity(intent);
            }
        }
    }