package com.jordi72.tasca1android;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity {
    private float size = 10;
    private TextView tv;
    private Button buttonSumar, buttonRestar, buttonAugmentar, buttonDisminuir, buttonMostrar, buttonAmagar, buttonColorText, buttonColorFons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

       retriveViews();
       setEvents();
    }

    private void retriveViews(){
        buttonSumar = findViewById(R.id.sumar);
        buttonRestar = findViewById(R.id.restar);
        buttonAugmentar = findViewById(R.id.augmentar);
        buttonDisminuir = findViewById(R.id.disminuir);
        buttonMostrar = findViewById(R.id.mostrar);
        buttonAmagar = findViewById(R.id.amagar);
        buttonColorText = findViewById(R.id.colorText);
        buttonColorFons = findViewById(R.id.colorFons);
        tv = findViewById(R.id.editText);



    }

    private void setEvents(){

        buttonSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumar();
            }
        });
        buttonRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restar();
            }
        });
        buttonAugmentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                augmentar();
            }
        });
        buttonDisminuir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disminuir();
            }
        });
        buttonMostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrar();
            }
        });
        buttonAmagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amagar();
            }
        });
        buttonColorText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorText();
            }
        });
        buttonColorFons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorFons();
            }
        });

    }


    private void sumar(){
        int i;
        i = Integer.parseInt(tv.getText().toString());
        i++;
        tv.setText(String.valueOf(i));
    }
    private void restar(){
        int i;
        i = Integer.parseInt(tv.getText().toString());
        i--;
        tv.setText(String.valueOf(i));
    }
    private void augmentar(){
        tv.setTextSize(size=size+3);
    }
    private void disminuir() { tv.setTextSize(size=size-3); }
    private void mostrar(){
        tv.setVisibility(View.VISIBLE);
    }
    private void amagar(){
        tv.setVisibility(View.INVISIBLE);
    }
    private void colorText(){
        tv.setTextColor(Color.BLUE);
    }
    private void colorFons(){
        tv.setBackgroundColor(Color.RED);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        TextView tv2 = (TextView) findViewById(R.id.editText);
        CharSequence auxSeq = tv2.getText();
        outState.putString("text", auxSeq.toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        TextView tv3 = (TextView) findViewById(R.id.editText);
        tv3.setText(savedInstanceState.getCharSequence("text"));
    }
}
