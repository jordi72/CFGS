package com.jordi72.tasca6android;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private NoteAdapter mNoteAdapter;
    private List<Note> noteList = new ArrayList<Note>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNoteAdapter = new NoteAdapter();
        ListView listNote = (ListView) findViewById(R.id.listview);
        listNote.setAdapter(mNoteAdapter);

        listNote.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int whichItem, long id) {
                Note tempNote = mNoteAdapter.getItem(whichItem);
                DialogShowNote dialog = new DialogShowNote();
                dialog.sendNoteSelected(tempNote);
                dialog.show(getSupportFragmentManager(), "");
            }
        });

        SharedPreferences shared = getPreferences(Context.MODE_PRIVATE);
        String json= shared.getString(getResources().getString(R.string.saved), "");
        Gson gson = new Gson();
        Type tipus = new TypeToken<List<Note>>(){
        }.getType();
        List<Note> notas = gson.fromJson(json, tipus);
        noteList = notas;


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.botoMenu) {
            DialogNewNote dialog = new DialogNewNote();
            dialog.show(getSupportFragmentManager(), "");
            return true;

        }
        return true;
    }

    public class NoteAdapter extends BaseAdapter {

        public void addNote(Note n){
            noteList.add(n);

            SharedPreferences shared = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor =shared.edit();

            Gson gson = new Gson();
            String json = gson.toJson(noteList);
            editor.putString(getResources().getString(R.string.saved), json.toString());
            editor.commit();


            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return noteList.size();
        }

        @Override
        public Note getItem(int whichItem) {
            return noteList.get(whichItem);
        }
        @Override
        public long getItemId(int whichItem) {
            return whichItem;
        }
        @Override
        public View getView(int whichItem, View view, ViewGroup viewGroup)
        {
// Has view been inflated already
            if(view == null){
// If not, do so here
// First create a LayoutInflater
                LayoutInflater inflater = (LayoutInflater)
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
// Now instantiate view using inflater.inflate
// using the listitem layout
                view = inflater.inflate(R.layout.layout_list_item, viewGroup,false);
// The false parameter is neccessary
// because of the way that we want to use listitem
            }
// Grab a reference to all our TextView and ImageView widgets
            TextView txtTitle = (TextView) view.findViewById(R.id.txtTitleItem);
            TextView txtDescription = (TextView) view.findViewById(R.id.txtDescriptionItem);
            ImageView ivImportant = (ImageView) view.findViewById(R.id.imageViewImportantItem);
// Hide any ImageView widgets that are not relevant
            Note tempNote = noteList.get(whichItem);
            ivImportant.setVisibility(View.INVISIBLE);
            if (tempNote.isImportant()){
                ivImportant.setVisibility(View.VISIBLE);
            }
// Add the text to the heading and description

            txtTitle.setText(tempNote.getTitle());
            txtDescription.setText(tempNote.getDescription());
            return view;}
        // This class is not overriden. We are the owner.



    }


    public void createNewNote(Note n){
        Log.d("test","horda=asko");
        mNoteAdapter.addNote(n);
    }
}
