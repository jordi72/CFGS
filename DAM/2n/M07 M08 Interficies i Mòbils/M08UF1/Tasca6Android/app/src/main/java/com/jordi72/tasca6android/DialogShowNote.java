package com.jordi72.tasca6android;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jordi72 on 12/02/18.
 */

public class DialogShowNote extends DialogFragment {
    private Note mNote;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_show_note, null);TextView txtTitle = (TextView)
                dialogView.findViewById(R.id.txtTitleShow);
        TextView txtDescription = (TextView)
                dialogView.findViewById(R.id.txtDescriptionShow);
        txtTitle.setText(mNote.getTitle());
        txtDescription.setText(mNote.getDescription());
        ImageView ivImportant = (ImageView)
                dialogView.findViewById(R.id.imageViewImportantShow);
        if (!mNote.isImportant()){
            ivImportant.setVisibility(View.GONE);
        }
        Button btnOK = (Button) dialogView.findViewById(R.id.btnOk
        );
        builder.setView(dialogView).setMessage("Nota!");
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return builder.create();
    }
    // Receive a note from the MainActivity
    public void sendNoteSelected(Note noteSelected) {
        mNote = noteSelected;
    }
}