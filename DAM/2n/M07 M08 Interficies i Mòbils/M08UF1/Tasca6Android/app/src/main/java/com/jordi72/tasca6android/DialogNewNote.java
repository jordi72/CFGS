package com.jordi72.tasca6android;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

/**
 * Created by jordi72 on 09/02/18.
 */

public class DialogNewNote extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View newNote = inflater.inflate(R.layout.dialog_new_note, null);
        final EditText editTitle = newNote.findViewById(R.id.txtTitle);
        final EditText editDescription = newNote.findViewById(R.id.txtDescription);
        final CheckBox checkBoxImportant = newNote.findViewById(R.id.checkBoxImportant);
        Button btnCancel = (Button) newNote.findViewById(R.id.btnCancel);
        Button btnOK = (Button) newNote.findViewById(R.id.btnCreate);
        builder.setView(newNote).setMessage("Afegeix una nova anotació");
// Handle the cancel button
        btnCancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
// Handle the Create button
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// Create a new note
                Note newNote = new Note();
// Set its variables to match the users entries on the form
                newNote.setTitle(editTitle.getText().toString());
                newNote.setDescription(editDescription.getText().toString());
                newNote.setImportant(checkBoxImportant.isChecked());
// Pass newNote back to MainActivity
                MainActivity mainLink = (MainActivity)getActivity();
                mainLink.createNewNote(newNote);
// Quit the dialog
                dismiss();
            }
        });
        return builder.create();
    }
}