package com.jordi72.tasca1androiduf2;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private SoundPool sp;
    private int idFX1;
    private int idFX2;
    private int nowPlaying;
    private SeekBar volumen =  null;
    //Button so1 = findViewById(R.id.so1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        piscina();
        volumen = (SeekBar) findViewById(R.id.seekBar);
        volumen.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChanged = 0;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChanged = progress/100;
                sp.setVolume(1,progressChanged,progressChanged);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void piscina(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            sp = new SoundPool.Builder()
                    .setMaxStreams(1)
                    .setAudioAttributes(audioAttributes)
                    .build();
        }else{
            sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        }

        try{
            AssetManager assetManager = this.getAssets();
            AssetFileDescriptor description;
            AssetFileDescriptor description2;
            description = assetManager.openFd("sirena1.mp3");
            description2 = assetManager.openFd("so2.mp3");
            idFX1 = sp.load(description, 0);
            idFX2 = sp.load(description2, 0);
        }catch(IOException e){
            Log.e("error", "failed to load sound files");
        }





    }

    public void Repso1(View v){
        nowPlaying = sp.play(idFX1, 1, 1,0, 1, 1);
    }
    public void Repso2(View v){
        nowPlaying = sp.play(idFX2, 1, 1,0, 1, 1);
    }
}
