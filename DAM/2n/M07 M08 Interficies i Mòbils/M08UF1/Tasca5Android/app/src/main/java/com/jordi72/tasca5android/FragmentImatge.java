package com.jordi72.tasca5android;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by jordi72 on 02/02/18.
 */

public class FragmentImatge extends android.support.v4.app.Fragment {

    private ImageView img;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragmentimatge, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        img = getView().findViewById(R.id.imageView);
        registerForContextMenu(img);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Esborra la imatge");
        //getMenuInflater().inflate(R.menu.opcionsimg, menu);
        MenuInflater inflador = getActivity().getMenuInflater();
        inflador.inflate(R.menu.opcionsimg, menu);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.esborrarImg:
                img.setVisibility(View.INVISIBLE);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
