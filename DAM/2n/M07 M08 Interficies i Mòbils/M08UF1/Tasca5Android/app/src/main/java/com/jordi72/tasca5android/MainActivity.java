package com.jordi72.tasca5android;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView img ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        FragmentManager handler = getSupportFragmentManager();
        FragmentTransaction transaction = handler.beginTransaction();

        switch (item.getItemId()) {
            case R.id.imatge:
                transaction.replace(R.id.frameLayout, new FragmentImatge()).commit();
                return true;
            case R.id.llista:
                transaction.replace(R.id.frameLayout, new FragmentLlista()).commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
