package com.jordi72.tasca5android;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by jordi72 on 05/02/18.
 */

public class FragmentLlista extends android.support.v4.app.Fragment {

    private View vista;
    private ListView vistaLlista;
    private ListView llistaOpcions;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vista =  inflater.inflate(R.layout.fragmentllista, container, false);
        obtenirLlista();
        crearLlista();
        return vista;
    }

    private void obtenirLlista(){
        vistaLlista = vista.findViewById(R.id.list);
    }
    private void crearLlista(){
        ArrayAdapter<String> array = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.arrayLlista));
        vistaLlista.setAdapter(array);
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        llistaOpcions = getView().findViewById(R.id.list);
        registerForContextMenu(llistaOpcions);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Esborra la imatge");
        //getMenuInflater().inflate(R.menu.opcionsimg, menu);
        MenuInflater inflador = getActivity().getMenuInflater();
        inflador.inflate(R.menu.opcionsllista, menu);
    }
    @Override
    //va tot el background need fix
    public boolean onContextItemSelected(MenuItem item) {
        int pos = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position;
        switch (item.getItemId()) {
            case R.id.colorVermell:
                llistaOpcions.getChildAt(pos).setBackgroundColor(Color.RED);
                return true;
            case R.id.colorVerd:
                llistaOpcions.getChildAt(pos).setBackgroundColor(Color.GREEN);
                return true;
            case R.id.colorBlau:
                llistaOpcions.getChildAt(pos).setBackgroundColor(Color.BLUE);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
