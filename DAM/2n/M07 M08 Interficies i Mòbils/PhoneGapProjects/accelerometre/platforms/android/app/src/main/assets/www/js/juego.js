var app={
  inicio: function(){
    DIAMETRO_BOLA = 50;
    dificultad = 0;
    velocidadX = 0;
    velocidadY = 0;
    puntuacion = 0;
    isColisionated = false;
    lotemps = 10;
    gameover = "Game Over Noob";

    alto  = document.documentElement.clientHeight;
    ancho = document.documentElement.clientWidth;

    app.vigilaSensores();
    app.iniciaJuego();
  },

  iniciaJuego: function(){

    function preload() {
      game.physics.startSystem(Phaser.Physics.ARCADE);

      game.stage.backgroundColor = '#f27d0c';
      game.load.image('bola', 'assets/bola.png');
      game.load.image('objetivo', 'assets/calabera.gif');
      game.load.audio('audio', 'assets/nain.mp3');
    }

    function create() {
      scoreText = game.add.text(16, 16, puntuacion, { fontSize: '100px', fill: '#757676' });

      objetivo = game.add.sprite(app.inicioX(), app.inicioY(), 'objetivo');
      bola = game.add.sprite(app.inicioX(), app.inicioY(), 'bola');
      audio = game.add.audio('audio');
      tempsText = game.add.text(ancho/2,30,lotemps, {fontSize:'50px', fill: '#ffffff'});


      game.physics.arcade.enable(bola);
      game.physics.arcade.enable(objetivo);

      bola.body.collideWorldBounds = true;
      bola.body.onWorldBounds = new Phaser.Signal();
      bola.body.onWorldBounds.add(app.decrementaPuntuacion, this);

      setInterval(function(){
        if(lotemps===0){
          /*destruirItems();    */
             bola.destroy();
              objetivo.destroy();
             audio.destroy();
              tempsText.destroy();
        //      scoreText.destroy();
      //    sida = game.add.text(ancho/2, alto/2,gameOver, {fontSize:'50px', fill: '#ffffff'});
        //  sida.text = gameOver;
        scoreText.text="GAME OVER!";
      }else{
        lotemps--;
        tempsText.text = lotemps;
      }
      }, 1000);

    }

    function update(){
      var factorDificultad = (300 + (dificultad * 100));
      bola.body.velocity.y = (velocidadY * factorDificultad);
      bola.body.velocity.x = (velocidadX * (-1 * factorDificultad));

      game.physics.arcade.overlap(bola, objetivo, app.incrementaPuntuacion, null, this);

      if(isColisionated){
        game.stage.backgroundColor = '#ff0000';
        isColisionated = false;
      }else{
        game.stage.backgroundColor = '#00ff00';

      }

    }

    var estados = { preload: preload, create: create, update: update };
    var game = new Phaser.Game(ancho, alto, Phaser.CANVAS, 'phaser',estados);
  },

  decrementaPuntuacion: function(){
    /*game.stage.backgroundColor = '#ff0000';*/
    puntuacion = puntuacion-1;
    scoreText.text = puntuacion;
    audio.play();
    isColisionated = true;
  },

  incrementaPuntuacion: function(){
    puntuacion = puntuacion+1;
    scoreText.text = puntuacion;

    objetivo.body.x = app.inicioX();
    objetivo.body.y = app.inicioY();

    if (puntuacion > 0){
      dificultad = dificultad + 1;
    }



  },

  inicioX: function(){
    return app.numeroAleatorioHasta(ancho - DIAMETRO_BOLA );
  },

  inicioY: function(){
    return app.numeroAleatorioHasta(alto - DIAMETRO_BOLA );
  },

  numeroAleatorioHasta: function(limite){
    return Math.floor(Math.random() * limite);
  },

  vigilaSensores: function(){

    function onError() {
        console.log('onError!');
    }

    function onSuccess(datosAceleracion){
      app.detectaAgitacion(datosAceleracion);
      app.registraDireccion(datosAceleracion);
    }

    navigator.accelerometer.watchAcceleration(onSuccess, onError,{ frequency: 10 });
  },

  detectaAgitacion: function(datosAceleracion){
    var agitacionX = datosAceleracion.x > 10;
    var agitacionY = datosAceleracion.y > 10;

    if (agitacionX || agitacionY){
      setTimeout(app.recomienza, 1000);
    }
  },

  recomienza: function(){
    document.location.reload(true);
  },

  registraDireccion: function(datosAceleracion){
    velocidadX = datosAceleracion.x ;
    velocidadY = datosAceleracion.y ;
  },

  destruirItems: function(){
    bola.destroy();
    objetivo.destroy();
    audio.destroy();
    tempsText.destroy();
    scoreText.destroy();
  }

};

if ('addEventListener' in document) {
    document.addEventListener('deviceready', function() {
        app.inicio();
    }, false);
}
