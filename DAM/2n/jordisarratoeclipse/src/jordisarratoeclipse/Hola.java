package jordisarratoeclipse;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class Hola extends Shell {

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Display display = Display.getDefault();
			Hola shell = new Hola(display);
			shell.open();
			shell.layout();
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the shell.
	 * @param display
	 */
	public Hola(Display display) {
		super(display, SWT.SHELL_TRIM);
		
		
		Button btnNewButton = new Button(this, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int ras1 = AMenysBMenysB(1, 1);
				int ras2 = AMenysBMenysB(2, 2);
				int ras3 = AMenysBMenysB(4, 4);
				System.out.println(ras1);
				System.out.println(ras2);
				System.out.println(ras3);
			}
		});
		btnNewButton.setBounds(186, 125, 94, 34);
		btnNewButton.setText("Article 155!!");
		createContents();
	}
	
	public static int AMenysBMenysB(int val1, int val2) {
		
		int resultat = val1 + val2;
		return resultat;
		
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setText("SWT Application");
		setSize(450, 300);

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
