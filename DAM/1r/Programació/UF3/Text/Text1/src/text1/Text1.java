package text1;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Text1 {

    public static void main(String[] args) throws IOException {
        
        File fitxer = new File ("text.txt");
        fitxer.createNewFile();
        Scanner s = new Scanner(fitxer);
        PrintWriter text = new PrintWriter(fitxer);
        obrirGravar(text);
        obrirLlegir(s);
        s.close();
    }
    
    public static void obrirGravar(PrintWriter text){ 
        text.println("Hola, món!!");
        text.close();
    }
    public static void obrirLlegir( Scanner s){
        System.out.println(s.nextLine());
    }

}
