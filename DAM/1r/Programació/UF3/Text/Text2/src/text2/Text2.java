package text2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Text2 {

    public static void main(String[] args) throws FileNotFoundException {

        Scanner user = new Scanner(System.in);
        String text = "";
        String text2 = "";
        System.out.println("Diga'm el nom del document a llegir");
        text = user.nextLine();
        File f = new File("text.txt");
        System.out.println("Diga'm el nom del document a escriure");
        text2 = user.nextLine();

        Scanner s = new Scanner(f);
        File fd = new File("text2.txt");
        PrintWriter pw = new PrintWriter(fd);
        String frase = "";
        int numero = 0;
        while (s.hasNext()) {
            frase = s.nextLine();
            numero++;
            pw.println("/*" + numero + "*/ " + frase);
            System.out.println(("/*" + numero + "*/ " + frase));
        }
        s.close();
        pw.close();
    }

}
