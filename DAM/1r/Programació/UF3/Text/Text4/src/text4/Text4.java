package text4;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Text4 {

    public static void main(String[] args) throws FileNotFoundException{
        
        File f = new File("test.txt");
        Scanner C = new Scanner(f);
        Scanner P = new Scanner(f);
        Scanner L = new Scanner(f);
        caracters(C);
        paraules(P);
        lineas(L);
        
    }
    public static void caracters(Scanner C){
        int c = 0;
        String g = "";
        while(C.hasNext()){
            g=C.next();
            c=c+g.length();
        }
        System.out.println(c + " Caracters Totals.");
    }
    public static void paraules(Scanner P){
        int p = 0;
        while(P.hasNext()){
            P.next();
            p++;
        }
        System.out.println(p + " Paraules Totals.");
    }
    public static void lineas(Scanner L){
        int l = 0;
        while(L.hasNext()){
            L.nextLine();
            l++;
        }
        System.out.println(l + " Lineas Totals.");
    }

}
