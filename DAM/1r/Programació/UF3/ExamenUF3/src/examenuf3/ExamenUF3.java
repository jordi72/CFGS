package examenuf3;
//examen de 10
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Scanner;

public class ExamenUF3 {

    static final String notesDat = "entrada/notes.dat";
    static final String notesTxt = "entrada/notes.txt";
    static final String registreNotesTxt = "registreNotes.txt";

    public static void main(String[] args) {
        try {
            ArrayList<String> registreNotes = generarRegistreNotesiButlletins();
            guardarRegistreNotes(registreNotes);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void guardarRegistreNotes(ArrayList<String> registreNotes) throws FileNotFoundException {
        File file = new File(registreNotesTxt);
        PrintWriter outNotes = null;
        try {
            outNotes = new PrintWriter(file);

            for (int i = 0; i < registreNotes.size(); i++) {
                outNotes.println(registreNotes.get(i));
            }
        } finally {
            if (outNotes != null) {
                outNotes.close();
            }
        }
    }

    public static ArrayList<String> generarRegistreNotesiButlletins() throws FileNotFoundException, IOException {
        File fileDat = new File(notesDat);
        RandomAccessFile inDat = null;
        ArrayList<String> registreNotes = new ArrayList();

        try {
            inDat = new RandomAccessFile(fileDat, "r");

            while (inDat.getFilePointer() != inDat.length()) {
                int id = inDat.readInt();
                int nota = inDat.readInt();

                String nomAlumne = buscarNomAlumne(id);
                String alumneNota = nomAlumne.trim() + " " + String.valueOf(nota);
                registreNotes.add(alumneNota);
                generarButlleti(id, nomAlumne, nota);
            }
        } finally {
            if (inDat != null) {
                inDat.close();
            }
        }

        return registreNotes;
    }

    public static String buscarNomAlumne(int id) throws FileNotFoundException {
        Scanner inTxt = null;

        File fileTxt = new File(notesTxt);
        inTxt = new Scanner(fileTxt);
        while (inTxt.hasNextLine()) {
            String[] linia = inTxt.nextLine().split(",");
            if (String.valueOf(id).equals(linia[0].trim())) {
                return linia[1]; // Hem trobat el nom
            }
        }
        inTxt.close();
        return "Id " + String.valueOf(id) + " desconegut";
    }

    public static void generarButlleti(int id, String nom, int nota) throws FileNotFoundException {
        File butlletinsDir = new File("./butlletins/");
        butlletinsDir.mkdir();
        Scanner in = null;
        PrintWriter out = null;
        try {
            File plantilla = new File("plantillaCertificat.html");
            in = new Scanner(plantilla);
            String nomFitxer = id + "_" + nom.replace(" ", "") + ".html";
            File butlleti = new File("./butlletins/" + nomFitxer);
            out = new PrintWriter(butlleti);

            while (in.hasNextLine()) {
                String line = in.nextLine();
                line = line.replace("##nom##", nom);
                line = line.replace("##nota##", String.valueOf(nota));
                out.println(line);
            }
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }

    }
}
