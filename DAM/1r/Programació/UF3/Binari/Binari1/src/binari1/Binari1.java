package binari1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Binari1 {

    public static void main(String[] args) throws IOException{
        File text1 = new File("text2.txt");
        File text2 = new File("text3.pdf");
        FileInputStream fitxer = null;
        FileOutputStream encriptat = null;
        int llegir = 0;
        int escriure = 0;
        try {
            fitxer = new FileInputStream(text1);
            encriptat = new FileOutputStream(text2);
            try {
                while (llegir != -1) {
                    llegir = fitxer.read();
                    escriure = ~llegir;
                    encriptat.write(escriure);
                }
            } finally {
                encriptat.close();
            }
        } catch (IOException ex) {
            System.out.println("El prigrama a petat.");
        }
    }

}
