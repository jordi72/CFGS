package binari8;

import java.io.RandomAccessFile;
import java.io.File;

public class Binari8 {

    public static void main(String[] args) {
        try {
            File fitxer1 = new File("patching1.jar");
            File fitxer2 = new File("patching2.jar");
            File different = new File("differencies.dat");
            RandomAccessFile vell = new RandomAccessFile(fitxer1, "rw");
            RandomAccessFile nou = new RandomAccessFile(fitxer2, "rw");
            RandomAccessFile diff = new RandomAccessFile(different, "rw");
            long posicio1 = vell.getFilePointer();
            long posicio2 = nou.getFilePointer();
            for(int i=0; i<nou.length(); i++){
                int byteVell=vell.readByte();
                byte byteNou=nou.readByte();
                if(byteVell!=byteNou){
                    diff.writeByte(byteNou);
                    diff.writeLong(posicio2);
                }
            }
            diff.close();
            vell.close();
            nou.close();
        } catch (Exception e) {
            System.out.println("No s'ha trobat el fitxer" + e);
        }

    }

}
