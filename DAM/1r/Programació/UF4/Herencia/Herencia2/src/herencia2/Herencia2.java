package herencia2;

/**
 *
 * @author Familia
 */
public class Herencia2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Poema p = new Poema("Poema", 2);
        Decima d = new Decima("Decima");
        Haiku h = new Haiku("Haiku");
        Lira l = new Lira("Lira");
        System.out.println("Poema: " + p.getNom() + ", Versos: " + p.getVersos());
        System.out.println("Decima: " + d.getNom() + ", Versos: " + d.getVersos());
        System.out.println("Haiku: " + h.getNom() + ", Versos: " + h.getVersos());
        System.out.println("Lira: " + l.getNom() + ", Versos: " + l.getVersos());
    }
    
}
