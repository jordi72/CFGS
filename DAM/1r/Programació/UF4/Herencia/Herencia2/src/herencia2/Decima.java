package herencia2;

/**
 *
 * @author Familia
 */
public class Decima extends Poema{
    private final int versos=10;
    
    public Decima(String s){
        super(s);
        super.setVersos(versos);
    }
}
