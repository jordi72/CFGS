package herencia2;

/**
 *
 * @author Familia
 */
public class Poema {
    private String nom="";
    private int versos=0 ;
    
    public Poema(String n, int v){
        this.nom=n;
        this.versos=v;
    }
    public Poema(String a){
        this.nom=a;
    }
    public String getNom(){
        return this.nom;
    }
    public int getVersos(){
        return this.versos;
    }
    protected void setVersos(int a){
        this.versos=a;
    }
}
