package herencia1;

/**
 *
 * @author Familia
 */
public class Herencia1 {

    public static void main(String[] args) {
        Espelma e = new Espelma("Blau", 10);
        System.out.println("Color: " + e.getColor() + ", " + "Alçada: " + e.getAlcada() + "cm");
        System.out.println("Preu per espelma: " + e.calcularPreu() + "€");
        EspelmaPerfumada ep = new EspelmaPerfumada("Groc", 10);
        System.out.println("Color: " + ep.getColor() + ", " + "Alçada: " + ep.getAlcada() + "cm");
        System.out.println("Preu per espelma perfumada: " + ep.calcularPreu() + "€");
    }

}
