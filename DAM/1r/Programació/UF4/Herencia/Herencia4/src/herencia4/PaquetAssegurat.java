package herencia4;

public class PaquetAssegurat extends Paquet {

    public PaquetAssegurat(double p, int mE) {
        super(p, mE);
    }

    @Override
    public void calcularCost() {
        super.calcularCost();
        if (getPreu() >= 1 && getPreu() <= 5) {
            double a = getPreu() + 2.3;
            setPreu(a);
        } else if (getPreu() >= 5.01 && getPreu() <= 8) {
            double a = getPreu() + 4.5;
            setPreu(a);
        } else {
            double a = getPreu() + 6.4;
            setPreu(a);
        }
    }
}
