package herencia4;

public class Paquet {

    private double pes = 0;
    private int aire = 1;
    private int missatgeria = 2;
    private int correus = 3;
    private int metodeEnviament;
    private double preu = 0;

    public Paquet(double p, int mE) {
        this.pes = p;
        if (mE >= 1 && mE <= 3) {
            this.metodeEnviament = mE;
        }
        calcularCost();
    }

    public void calcularCost() {
        if (this.metodeEnviament == 1) {
            if (this.pes >= 1 && this.pes <= 9) {
                preu = 8;
            } else if (this.pes >= 10 && this.pes <= 14) {
                preu = 10;
            } else {
                preu = 15;
            }
        }
        if (this.metodeEnviament == 2) {
            if (this.pes >= 1 && this.pes <= 9) {
                preu = 5.5;
            } else if (this.pes >= 10 && this.pes <= 14) {
                preu = 7.5;
            } else {
                preu = 9.5;
            }
        }
        if (this.metodeEnviament == 3) {
            if (this.pes >= 1 && this.pes <= 9) {
                preu = 3;
            } else if (this.pes >= 10 && this.pes <= 14) {
                preu = 4.5;
            } else {
                preu = 8;
            }
        }
    }
    @Override
    public String toString(){
        String a = ("Metode Enviament:" + this.metodeEnviament + " Pes:" + this.pes  + " Preu:" + this.preu); 
        return a;
    }
    public double getPreu(){
        return this.preu;
    }
    public void setPreu(double p){
        this.preu=p;
    }
}
