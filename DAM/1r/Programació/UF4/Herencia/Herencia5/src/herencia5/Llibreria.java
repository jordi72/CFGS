package herencia5;

import java.util.ArrayList;

public class Llibreria {
    private ArrayList<Llibre>llibre = new ArrayList<>();
    public void addLlibre(Llibre nouLlibre){
        llibre.add(nouLlibre);
    }
    @Override
    public String toString(){
        String a="";            
        for(int i=0;i<llibre.size();i++){
          a = a + llibre.get(i).toString() + "\n";            
        }
        return a;
    }
}
