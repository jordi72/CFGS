package herencia5;

public class LlibreFiccio extends Llibre {
    public LlibreFiccio(String tit){
        super(tit);
        setPreu(17.99);
    }
    @Override
    public void setPreu(double nouPreu) {
        super.preu=nouPreu;
    }
    @Override
    public String toString(){
        return "Titol: " + getTitol() + ", " + "Preu: " + getPreu();
    }
}
