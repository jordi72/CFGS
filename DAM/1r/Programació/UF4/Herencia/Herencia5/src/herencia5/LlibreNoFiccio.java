package herencia5;

public class LlibreNoFiccio extends Llibre {

    public LlibreNoFiccio(String tit) {
        super(tit);
        setPreu(22.5);
    }

    @Override
    public void setPreu(double nouPreu) {
        super.preu = nouPreu;
    }
    @Override
    public String toString(){
        return "Titol: " + getTitol() + ", " + "Preu: " + getPreu();
    }
}
