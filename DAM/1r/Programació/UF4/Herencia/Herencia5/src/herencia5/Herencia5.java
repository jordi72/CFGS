package herencia5;

public class Herencia5 {

    public static void main(String[] args) {
        LlibreNoFiccio lnf = new LlibreNoFiccio("Sida");
        System.out.println(lnf.toString());
        LlibreFiccio lf = new LlibreFiccio("Sida 2");
        LlibreFiccio lf2 = new LlibreFiccio("Sida 3");
        LlibreFiccio lf3 = new LlibreFiccio("Sida 4");
        LlibreFiccio lf4 = new LlibreFiccio("Sida 5");
        System.out.println(lf.toString() + "\n");
        Llibreria ll = new Llibreria();
        ll.addLlibre(lf);
        ll.addLlibre(lf2);
        ll.addLlibre(lf3);
        ll.addLlibre(lf4);
        ll.addLlibre(lnf);
        System.out.println("Imprimint Llibreria \n" + ll.toString());
    }

}
