package herencia5;

public abstract class Llibre {
    private String titol = "";
    protected double preu = 0;
    
    public Llibre(String tit){
        this.titol=tit;
    }
    public double getPreu(){
        return this.preu;
    }
    public String getTitol(){
        return this.titol;
    }
    public abstract void setPreu(double nouPreu);
}
