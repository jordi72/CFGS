package herencia3;

/**
 *
 * @author jordi72
 */
public class Herencia3 {
    public static void main(String[] args) {
        KillerHeroPlayer khp = new KillerHeroPlayer(0, "Jordi");
        PremiumKillerHeroPlayer pkhp = new PremiumKillerHeroPlayer(0, "Sida", 2.99); //if mod < peta. GG
        System.out.println(khp.getId() + ", " +  khp.getNom());
        khp.setLvls(0, 100);
        khp.setLvls(1, 100);
        System.out.println(pkhp.getId() + ", " +  pkhp.getNom());
        pkhp.setLvls(0, 110);
        pkhp.setLvls(1, 110);
        pkhp.setLvls(2, 110);
        pkhp.setLvls(3, 110);
        pkhp.setLvls(4, 110);
        pkhp.setLvls(5, 110);
        pkhp.setLvls(6, 110);
        pkhp.setLvls(7, 110);
        pkhp.setLvls(8, 110);
        pkhp.setLvls(9, 110);
        pkhp.setLvls(10, 110);
        System.out.println(khp.getNom() + ", " + khp.getLvls(0));
        System.out.println(pkhp.getNom() + ", " + pkhp.getLvls(10));
    }
    
}
