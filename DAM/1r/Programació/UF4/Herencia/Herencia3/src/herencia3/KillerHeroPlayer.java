package herencia3;

/**
 *
 * @author jordi72
 */
public class KillerHeroPlayer {

    private int id = 0;
    private String nom = "";
    protected int[] nivells;

    public KillerHeroPlayer(int id, String nom) {
        nivells = new int[10];
        this.id = id;
        this.nom = nom;

    }

    public String getNom() {
        return this.nom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLvls(int lvl) {
        return nivells[lvl];
    }

    public void setLvls(int lvls, int punts) {
        if (lvls == 0) {
            nivells[lvls] = punts;
        } else if (lvls > 0 && nivells[lvls - 1] >= 100) {
            nivells[lvls] = punts;
        } else {
            throw new IllegalArgumentException("Necessites minim 100 punts per al nivell anterior");
        }
    }
}
