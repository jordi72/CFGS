package herencia3;

/**
 *
 * @author jordi72
 */
public class PremiumKillerHeroPlayer extends KillerHeroPlayer {

    private int id = 0;
    private String nom = "";
    private double diners = 0;
    private boolean pagat = false;

    public PremiumKillerHeroPlayer(int id, String nom, double diners) {
        super(id, nom);
        if (diners == 2.99) {
            pagat = true;
            nivells = new int[40];
        }else{
            nivells = new int[10];
        }
    }
}
