package binarifitxertests;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *
 * @author Familia
 */
public class BinariFitxerTests {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            File f = new File("numerosEncriptats.txt");
            File f2 = new File("numerosDesencriptats.txt");
            RandomAccessFile raf = new RandomAccessFile(f, "r");
            RandomAccessFile raf2 = new RandomAccessFile(f2, "rw");
            try {
                for (int i = 1; i < f.length(); i++) {
                    int valor = 0;
                    valor = raf.readInt();
                    valor = ~valor;
                    raf2.writeInt(valor);
                    System.out.println("S'ha escrit el valor " + valor);
                }
            } catch (IOException ioex) {
                System.out.println(ioex);
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        }
    }
}
