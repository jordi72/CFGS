package canvas;

import java.awt.Color;
import java.awt.Graphics;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class CanvasScreen extends JFrame {

    private static CanvasScreen canvas;
    private ArrayList<CanvasDrawable> drawables = new ArrayList<CanvasDrawable>();

    public static CanvasScreen getCanvas() {
        return canvas;
    }

    public static void start() {
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    canvas = new CanvasScreen();
                    canvas.setVisible(true);
                }
            });
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        }
    }

    private CanvasScreen() {
        setSize(600, 400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void paint(Graphics canvas) {
        for (CanvasDrawable drawable : drawables) {
            drawable.paint(canvas);
        }
    }

    public void addDrawable(CanvasDrawable d) {
        drawables.add(d);
    }

    public void removeDrawable(CanvasDrawable d) {
        drawables.remove(d);
    }

    public CanvasDrawable getDrawable(int i) {
        return drawables.get(i);
    }

    public static void main(String[] args) {
        CanvasScreen.start();
        CanvasScreen c = CanvasScreen.getCanvas();
        CanvasDrawable d = new CanvasDrawable() {
            @Override
            public void paint(Graphics g) {
                g.setColor(Color.GREEN);
                g.drawRect(0, 0, 200, 200);
                g.setColor(Color.RED);
                g.drawOval(200, 200, 200, 200);
            }
        };
        c.addDrawable(d);
        c.repaint();
        System.out.println("finish");
    }
}
