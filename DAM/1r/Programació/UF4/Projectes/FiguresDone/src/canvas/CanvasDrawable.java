package canvas;

import java.awt.Graphics;

public interface CanvasDrawable {

    void paint(Graphics g);
}
