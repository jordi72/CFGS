package canvas;

import figures.RectangleBase;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Familia
 */
public class RectangleCanvas extends RectangleBase implements CanvasDrawable{

    public RectangleCanvas(int x, int y, Color color, int amp, int alc) {
        super(x, y, color, amp, alc);
    }

    @Override
    public void paint(Graphics g) {
        g.drawRect(getX(), getY(), amplada, alcada);
    }
    
}
