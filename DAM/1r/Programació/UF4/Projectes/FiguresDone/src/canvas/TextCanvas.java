package canvas;

import figures.TextBase;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Familia
 */
public class TextCanvas extends TextBase implements CanvasDrawable{
    
    public TextCanvas(int x, int y, Color color, boolean horz, String text) {
        super(x, y, color, horz, text);
    }

    @Override
    public void paint(Graphics g) {
        g.draw3DRect(getX(), getY(), 100, 25, true);
    }
    
}
