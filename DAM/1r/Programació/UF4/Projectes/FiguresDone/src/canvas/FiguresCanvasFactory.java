package canvas;
import figures.FiguresFactory;
import figures.RectangleBase;
import figures.TextBase;
import java.awt.Color;
/**
 *
 * @author Familia
 */
public class FiguresCanvasFactory extends FiguresFactory{

    @Override
    public RectangleBase crearRectangle(int x, int y, Color color, int amp, int alc) {
        RectangleCanvas rc = new RectangleCanvas(x, y, color, amp, alc);
        return rc;
    }

    @Override
    public TextBase crearText(int x, int y, Color color, boolean horz, String text) {
        TextCanvas tc = new TextCanvas(x, y, color, horz, text);
        return tc;
    }
    
}
