package main;

import console.ConsoleGraphics;
import console.ConsoleScreen;
import console.FiguresConsoleFactory;
import console.RectangleConsole;
import console.TextConsole;
import figures.FiguresFactory;
import figures.RectangleBase;
import figures.TextBase;
import java.awt.Color;
/**
 *
 * @author jordi72
 */
public class Main {
    public static void main(String[] args){
        
        FiguresFactory ff = new FiguresConsoleFactory();
        ConsoleGraphics cg = new ConsoleGraphics(15, 15);
        ConsoleScreen cs = new ConsoleScreen(cg);
        RectangleConsole rc = (RectangleConsole) ff.crearRectangle(1, 1, Color.black, 10, 10);
        TextConsole tc = (TextConsole) ff.crearText(0, 0, Color.black, true, "sida");
        cs.addDrawable(rc);
        cs.addDrawable(tc);
        cs.repaint(); 
    }
}
