package figures;

/**
 *
 * @author Familia
 */
public abstract class FiguresFactory {

    public abstract RectangleBase crearRectangle(int x, int y, java.awt.Color color, int amp, int alc);

    public abstract TextBase crearText(int x, int y, java.awt.Color color, boolean horz, String text);

}
