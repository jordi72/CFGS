package figures;

import java.awt.Color;

/**
 *
 * @author jordi72
 */
public class RectangleBase extends Grafic2D {
    protected int amplada;
    protected int alcada;

    public RectangleBase(int x, int y, Color color, int amp, int alc) {
        super(x, y, color);
        this.amplada=amp;
        this.alcada=alc;
    }
}
