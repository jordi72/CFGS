package figures;

import java.awt.Color;

/**
 *
 * @author jordi72
 */
public class TextBase extends Grafic2D{
    private boolean horitzontal;
    private String text;

    public TextBase(int x, int y, Color color, boolean horz, String text) {
        super(x, y, color);
        this.horitzontal=horz;
        this.text=text;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the horitzontal
     */
    public boolean isHoritzontal() {
        return horitzontal;
    }

    /**
     * @param horitzontal the horitzontal to set
     */
    public void setHoritzontal(boolean horitzontal) {
        this.horitzontal = horitzontal;
    }
}
