package figures;

/**
 *
 * @author jordi72
 */
public abstract class Grafic2D {
    private int x;
    private int y;
    protected java.awt.Color color;
    
    public Grafic2D(int x, int y, java.awt.Color color){
        this.x=x;
        this.y=y;
        this.color=color;
    }

    /**
     * @return the x
     */
    public int getX() {
        return this.x;
    }

    /**
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return this.y;
    }

    /**
     * @param y the y to set
     */
    public void setY(int y) {
        this.y = y;
    }
    
}
