package console;

/**
 *
 * @author Familia
 */
public interface ConsoleDrawable {
    public void paint(ConsoleGraphics graphics);
}
