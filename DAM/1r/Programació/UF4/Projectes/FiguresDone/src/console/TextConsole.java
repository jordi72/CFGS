package console;

import figures.TextBase;
import java.awt.Color;

/**
 *
 * @author jordi72
 */
public class TextConsole extends TextBase implements ConsoleDrawable {

    public TextConsole(int x, int y, Color color, boolean horz, String text) {
        super(x, y, color, horz, text);
    }

    @Override
    public void paint(ConsoleGraphics graphics) {
        for (int i = 0; i < getText().length(); i++) {
            if (isHoritzontal()) {
                graphics.setChar(getY(), getX() + i, getText().charAt(i));
            } else {
                graphics.setChar(getY() + i, getX(), getText().charAt(i));
            }
        }
    }

}
