package console;
import java.awt.Color;
import figures.RectangleBase;
import figures.TextBase;
import figures.FiguresFactory;
/**
 *
 * @author Familia
 */
public class FiguresConsoleFactory extends FiguresFactory{

    @Override
    public RectangleBase crearRectangle(int x, int y, Color color, int amp, int alc) {
       RectangleConsole rc = new RectangleConsole(x, y, color, amp, alc, '*');
       return rc;
    }

    @Override
    public TextBase crearText(int x, int y, Color color, boolean horz, String text) {
        TextConsole tc = new TextConsole(x, y, color, horz, text);
        return tc;
    }
    
}
