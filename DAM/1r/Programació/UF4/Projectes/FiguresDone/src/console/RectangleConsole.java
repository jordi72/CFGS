package console;

import figures.RectangleBase;
import java.awt.Color;
/**
 *
 * @author jordi72
 */
public class RectangleConsole extends RectangleBase implements ConsoleDrawable{

    private char sprite;

    public RectangleConsole(int x, int y, Color color, int amp, int alc, char sprite) {
        super(x, y, color, amp, alc);
        this.sprite=sprite;
    }
    
    @Override
    public void paint(ConsoleGraphics graphics) {
        for(int i=0;i<amplada;i++){
            for(int j=0;j<alcada;j++){
                graphics.setChar(getY() +i, getX() +j, sprite);
            }
        }
    }

    /**
     * @return the spire
     */
    public char getSprite() {
        return sprite;
    }

    /**
     * @param spire the spire to set
     */
    public void setSprite(char sprite) {
        this.sprite = sprite;
    } 
}
