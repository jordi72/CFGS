package console;

/**
 *
 * @author Familia
 */
public class ConsoleGraphics {
    private int rowsNum;
    private int colsNum;
    private char[][] screen;
    public ConsoleGraphics(int rows, int cols){
        this.rowsNum=rows;
        this.colsNum=cols;
        this.screen = new char[rowsNum][colsNum];
    }

    public ConsoleGraphics() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public void clear(){
        for(int i=0;i<screen.length;i++){
            for(int j=0;j<screen.length;j++){
                screen[i][j]=' ';
            }
        }
    }
    public void setChar(int row, int col, char c){
        if ((row < rowsNum && row >= 0) && (col < colsNum && col >= 0)){
            this.screen[row][col] = c;
        }
    }
    public char getChar(int row, int col){
        return screen[row][col];
    }
    public int getRowsNum(){
        return this.rowsNum;
    }
    public int getColsNum(){
        return this.colsNum;
    }
}
