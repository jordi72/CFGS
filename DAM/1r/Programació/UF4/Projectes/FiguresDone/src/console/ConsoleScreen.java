package console;

import java.util.ArrayList;

/**
 *
 * @author Familia
 */
public class ConsoleScreen {
    private ConsoleGraphics graphics;
    private ArrayList<ConsoleDrawable>drawables = new ArrayList<>();

    public ConsoleScreen(ConsoleGraphics cg) {
        this.graphics=cg;
    }
    
    public void repaint(){
        graphics.clear();
        for(int i=0;i<drawables.size();i++){
            drawables.get(i).paint(graphics);
        }
        for(int i=0;i<graphics.getRowsNum();i++){
            for(int j=0;j<graphics.getColsNum();j++){
                System.out.print(graphics.getChar(i, j));
            }
            System.out.println("");
        }
    }
    public void addDrawable(ConsoleDrawable d){
        drawables.add(d);
    }
    public void removeDrawable(ConsoleDrawable d){
        drawables.remove(d);
    }
    /*public ConsoleDrawable getDrawable(int i){
        return drawables.get(i);
    }*/
    //public ConsoleGraphics getGraphic(){return null;}
    
    
}
