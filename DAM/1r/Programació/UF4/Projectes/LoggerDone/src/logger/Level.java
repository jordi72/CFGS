package logger;

/**
 * 
 * @author Familia
 */

public class Level {
    public static final Level OFF = new Level("OFF",0);
    public static final Level INFO = new Level("INFO",1);
    public static final Level WARNING = new Level("WARNING",2);
    public static final Level ERROR = new Level("ERROR",3);
    
    private String name;
    private int value;
    
    protected Level(String name, int value){
        this.name = name;
        this.value = value;
    }
    @Override
    public boolean equals(Object o){
        if (o == null)
            return false;
        if (o.getClass() != this.getClass())
            return false;
        Level lvl = (Level) o;
        return lvl.getValue() == this.getValue();
    }

    @Override
    public int hashCode() {
        return value;
    }
    @Override
    public String toString(){
       String s = ("Nom: " + this.name + " Valor: " + this.value);
       return s;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }
    
}
