package logger;

/**
 *
 * @author Familia
 */
public class Main {

    public static void main(String[] args) {
        Logger log = Logger.getLogger();
        log.setLevel(Level.WARNING);
        log.register(new ConsoleHandler());
        log.register(new FileHandler());
        log.println("Warning", Level.WARNING);
        System.out.println(log.getLevel().toString());
    }
}
