package logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Familia
 */
public class FileHandler implements Handler {

    @Override
    public void println(String message, Level level) {
        File f = new File("log.txt");
        FileWriter fw = null;
        try {
            fw = new FileWriter(f, true);
            fw.append("Nivell: " + level.getName() + "\n Missatge: " + message + "\n");
        } catch (IOException ex) {
            System.out.println(ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                System.out.println(ex);
            }
        }
    }

    @Override
    public void print(String message, Level level){      
        File f = new File("log.txt");
        FileWriter fw = null;
        try {
            fw = new FileWriter(f, true);
            fw.append("Nivell: " + level.getName() + "\n Missatge: " + message + "\n");
        } catch (IOException ex) {
            System.out.println(ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                System.out.println(ex);
            }
        }
    }
}
