package logger;

/**
 *
 * @author Familia
 */
public class ConsoleHandler implements Handler {

    @Override
    public void println(String message, Level level) {
        System.out.println("Nivell: " + level.getName() + ", Missatge:  " + message);
    }

    @Override
    public void print(String message, Level level) {
        System.out.println("Nivell: " + level.getName() + ", Missatge:  " + message);
    }
    
}
