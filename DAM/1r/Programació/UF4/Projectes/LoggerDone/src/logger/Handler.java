package logger;

/**
 *
 * @author Familia
 */
public interface Handler {
    
    public void println(String message, Level level);
    public void print(String message, Level level);
}
