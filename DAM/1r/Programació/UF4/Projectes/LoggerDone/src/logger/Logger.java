package logger;

import java.util.ArrayList;

public class Logger {

    private static Logger logger;

    private Level level;
    private ArrayList<Handler> handler = new ArrayList<>();

    private Logger() {

    }
    
    public static Logger getLogger() {
        Logger log;
        if(logger==null){
            log = new Logger();
            
        }else{
            log=logger;
        }
        return log;
    }
    public void register(Handler h){
        handler.add(h);
    }
    
    public void unregister(Handler h){
        handler.remove(h);
    }
    
    public void println(String message, Level lvl){
        if(this.getLevel().getValue()>0){
            for(Handler h : handler){
                if(lvl.getValue()>=this.getLevel().getValue()){
                    h.println(message, level);
                }
            }
        }
    }
    
    public void print(String message, Level lvl){
        if(this.getLevel().getValue()>0){
            for(Handler h : handler){
                if(lvl.getValue()>=this.getLevel().getValue()){
                    h.print(message, level);
                }
            }
        }
    }
    
    public Level getLevel(){
        return this.level;
    }
    
    public void setLevel(Level level){
        this.level=level;
    }

}
