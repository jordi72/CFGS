package jardí;

/**
 *
 * @author Familia
 */
public class Declinus extends Planta {

    private int aux;
    private boolean decreix;

    @Override
    public char getChar(int pos) {
        char c = ' ';
        if (pos == getAltura()) {
            c = '*';
        } else if (pos < getAltura()) {
            c = ':';
        }
        return c;
    }

    @Override
    public Planta creix() {
        aux++;
        Llavor llavor = null;
        if (!decreix) {
            if (aux % 2 == 0) {
                this.altura++;
            }
            if (altura == 3) {
                decreix = true;
                llavor = new Llavor(new Declinus());
            }
        } else {
            if (altura == 3) {
                llavor = new Llavor(new Declinus());
            }
            if (aux % 2 == 0) {
                altura--;
            }
            if (altura == 0) {
                setEsViva(false);
            }
        }
        return llavor;
    }

}
