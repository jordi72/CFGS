package jardí;

/**
 *
 * @author Familia
 */
public class Llavor extends Planta{
    public Planta planta;
    public int temps;
    
    public Llavor(Planta planta){
        if(planta instanceof Llavor){
            IllegalArgumentException ex = new IllegalArgumentException("Ep, d'una llavor no pot neixer una altre llavor!");
            System.out.println(ex);
        }
        this.planta=planta;
        this.setEsViva(true);
        
    }
    @Override
    public Planta creix(){
        if(this.getAltura()<5){
           this.altura++;
        }else{
            return planta;
        }
        return null;
    }
    public char getChar(int lvl){
        char c = ' ';
        if(lvl == 0){
            c='.';
        }
        return c;
    }
}
