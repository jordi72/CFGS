package jardí;

import java.util.Scanner;

/**
 *
 * @author Familia
 */
public class Main {

    public static void main(String[] args) {
        Jardi jardi = new Jardi(10);
        Planta planta1 = (new Altibus());
        Planta planta2 = (new Declinus());
        Planta planta3 = (new Altibus());

       String user = "";
       Scanner s = new Scanner (System.in);
       
       jardi.ficarPlanta(planta1, 2);
       jardi.ficarPlanta(planta2, 9);
       jardi.ficarPlanta(planta3, 5);
       
       while(!user.equals("surt")){
           jardi.temps();
           System.out.println(jardi.toString());
           user = s.nextLine();
       }
    }
    
}
