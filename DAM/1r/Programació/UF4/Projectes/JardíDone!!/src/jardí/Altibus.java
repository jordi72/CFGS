package jardí;

/**
 *
 * @author Familia
 */
public class Altibus extends Planta {

    @Override
    public char getChar(int posicio) {
        char c = ' ';
        if (posicio == getAltura()) {
            c = 'O';
        } else if (posicio < getAltura()){
            c = '|';
        }
        return c;
    }

    @Override
    public Planta creix() {
        super.creix();
        Llavor llavor = null;
        if (getAltura() > 7) {
            Altibus alt = new Altibus();
            llavor = new Llavor(alt);
        }
        return llavor;
    }

}
