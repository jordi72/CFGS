package jardí;

/**
 *
 * @author Familia
 */
public class Jardi {

    public static final int ALTURA_JARDÍ = 10;
    protected Planta[] jardi;
    private String terra = "";
    private int mida;

    public Jardi(int mida) {
        if (mida < 1) {
            this.mida = 10;
        } else {
            this.mida = mida;
        }
        jardi = new Planta[mida];
        for (int i = 0; i < this.mida; i++) {
            terra = terra + "_";
        }
    }

    public void temps() {
        int posicio;
        Planta planta;
        for (int i = 0; i < jardi.length; i++) {
            if (jardi[i] != null) {
                planta = jardi[i].creix();
                if (planta instanceof Llavor) {
                    posicio = jardi[i].escamparLlavor();
                    plantarLlavor(planta, i + posicio);
                } else if (planta instanceof Planta) {
                    jardi[i] = planta;
                }
                if (!jardi[i].isEsViva()) {
                    jardi[i] = null;
                }
            }
        }
    }

    @Override
    public String toString() {
        int maxAltura = 0;
        String retorn = "";
        for (int i = 0; i < jardi.length; i++) {
            if (jardi[i] != null) {
                if (jardi[i].getAltura() > maxAltura) {
                    maxAltura = jardi[i].getAltura();
                }
            }
        }

        for (int i = maxAltura; i >= 0; i--) {
            for (int j = 0; j < jardi.length; j++) {
                if (jardi[j] != null) {
                    retorn = retorn + jardi[j].getChar(i);
                } else {
                    retorn = retorn + " ";
                }
            }
            retorn = retorn + "\n";
        }
        retorn = retorn + terra;
        return retorn;
    }

    public void ficarPlanta(Planta planta, int posicio) {
        jardi[posicio] = planta;
    }

    private boolean plantarLlavor(Planta planta, int posicio) {
        if (posicio > 0 && posicio < jardi.length) {
            if (jardi[posicio] == null) {
                jardi[posicio] = planta;
            }
        }
        return true;
    }
}
