package jardí;

import java.util.Random;

/**
 *
 * @author Familia
 */
public abstract class Planta {

    private boolean esViva;
    protected int altura;

    public abstract char getChar(int pos);
    
    public Planta(){
        this.esViva=true;
        this.altura=0;
    }

    public Planta creix() {
        if (getAltura() < 10) {
            this.altura++;
        } else {
            esViva = false;
        }
        return null;
    }

    public int escamparLlavor() {
        Random r = new Random();
        int pos = r.nextInt(5) - 2;
        return pos;
    }

    public int getAltura() {
        return this.altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public boolean isEsViva() {
        return this.esViva;
    }

    public void setEsViva(boolean siNo) {
        this.esViva = siNo;
    }

}
