package classes1;

/**
 *
 * @author jordi72
 */
public class Rectangle {

    private double alcada;
    private double amplada;

    /**
     *
     * @param alcada 1.0 per defecte
     * @param amplada 1.0 per defecte
     */
    public Rectangle() {
        alcada = 1.0;
        amplada = 1.0;
    }

    /**
     *
     * @param alcadaConstructor serà l'alçada del rectange, l'usuari tria el
     * valor.
     * @param ampladaConstructor serà l'amplada del rectangle, l'usuari tria el
     * valor.
     */
    public Rectangle(double alcadaConstructor, double ampladaConstructor) {
        if (alcadaConstructor < 0 || ampladaConstructor < 0) {
            throw new IllegalArgumentException("El valor pasat es inferior a 0");
        } else {
            alcada = alcadaConstructor;
            amplada = ampladaConstructor;
        }
    }

    /**
     *
     * @return retorna la alçada del rectangle.
     */
    public double getAlcada() {
        return this.alcada;
    }

    /**
     *
     * @param alcadaNova l'usuari estableix el nou valor de l'alcada.
     * @return
     */
    public double setAlcada(double alcadaNova) {
        return alcada = alcadaNova;
    }

    /**
     *
     * @return retorna l'amplada del rectangle.
     */
    public double getAmplada() {
        return this.amplada;
    }

    /**
     *
     * @param ampladaNova l'usuari estableix el nou valor de l'amplada.
     * @return
     */
    public double setAmplada(double ampladaNova) {
        return amplada = ampladaNova;
    }

    /**
     *
     * @return Retorna l'area del rectangle.
     */
    public double getArea() {
        return alcada * amplada;
    }

    /**
     *
     * @return Retorna el perimetre del rectangle.
     */
    public double getPerimetre() {
        return alcada + amplada * 2;
    }

    static Rectangle calcularMaximRectangle(Rectangle[] rectangles) {
        Rectangle maxim = rectangles[0];
        for (int i = 1; i < rectangles.length; i++) {
            if (rectangles[i].getArea() > maxim.getArea()) {
                maxim = rectangles[i];
            } 
        }
        return maxim;
    }

}
