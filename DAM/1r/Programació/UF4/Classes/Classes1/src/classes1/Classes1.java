package classes1;

import java.util.Scanner;

/**
 *
 * @author jordi72
 */
public class Classes1 {


    public static void main(String[] args) {
        
        Scanner usuariInput = new Scanner(System.in);
        
        Rectangle rect1 = new Rectangle();
        Rectangle rect2 = new Rectangle(2, 2.5);
        rect2.setAlcada(-1);
        Rectangle [] rectangles = new Rectangle[2];
        rectangles[0]=rect1;
        rectangles[1]=rect2;
        Rectangle rect3 = new Rectangle();
        rect3=Rectangle.calcularMaximRectangle(rectangles);
        System.out.println(rect3.getArea());
    }    
}
