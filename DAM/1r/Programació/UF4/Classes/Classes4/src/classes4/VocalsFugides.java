package classes4;

/**
 *
 * @author jordi72
 */
public class VocalsFugides {
    
    private String solucio;
    private char [] matriu = {'a','e','i','o','u'};
    public VocalsFugides(String s){
        solucio=s;
    }
    public boolean comprovarSolucio(String possibleSolucio){
        String a = possibleSolucio;
        boolean res;
        if(solucio.equals(a)){
            res = true;
        }else{
            res = false;
        }
        return res;
    }
    public String extreuVocals(String s){
        String resultat="";
        char g;
        for(int i=0;i<s.length();i++){
            g=s.charAt(i);
            if(!esVocal(g)){
                resultat=resultat+g;
            }  
        }
        return resultat;
    }
    public boolean esVocal(char c){
        boolean result=false;
        for(int i=0;i<matriu.length;i++){
            if(matriu[i]==c){
                result=true;
            }
        }
        return result;
    }
    public String getEnunciat(){
        return solucio;
    }
}
