package classes4;
import java.util.Scanner;
/**
 *
 * @author jordi72
 */
public class Classes4 {

    public static void main(String[] args) {
        String resposta="";
        VocalsFugides frase = new VocalsFugides("Hola, com estas?");
        Scanner s = new Scanner(System.in);
        while(!frase.comprovarSolucio(resposta)){
            System.out.println("Escriu '.' per acabar o completa amb les vocals que manquen:");
            System.out.println(frase.extreuVocals(frase.getEnunciat()));
            resposta=s.nextLine();
            if(resposta.equals(".")){
                System.out.println("Adeu");
                break;
            }
        }
        System.out.println("Molt bé has encertat!");
    }
    
}
