package classes2;

/**
 *
 * @author jordi72
 */
public class cronometre {
    
    private long temps;
    //private long stop;
    
    public void start(){
        temps=System.currentTimeMillis();
    }
    public void stop(){
      temps= System.currentTimeMillis()-temps;
    }
    public long obtenirIntervalDeTemps(){
        return System.currentTimeMillis()-temps;
    }
}
