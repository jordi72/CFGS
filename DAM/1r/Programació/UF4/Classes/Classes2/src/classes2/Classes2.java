package classes2;


/**
 *
 * @author jordi72
 */
public class Classes2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        cronometre crono = new cronometre();
        crono.start();
        Thread.sleep(2000);
        System.out.println(crono.obtenirIntervalDeTemps());
        crono.stop();
    }

}
