package classes3;

/**
 *
 * @author jordi72
 */
public class Classes3 {


    public static void main(String[] args) {
        Comptador c = new Comptador();
        c.comptar();
        c.comptar();
        System.out.println(c.getComptador());
        c.undo();
        System.out.println(c.getComptador());
        c.reset();
        System.out.println(c.getComptador());
    }
    
}
