package classes3;

public class Comptador {
   
    private int comptador=0;
    private int max=Integer.MAX_VALUE;
    private int undo=1;
    
    public void comptar(){
        comptador++;
    }
    public void reset(){
        comptador=0;
    }
    public void undo(){
        if(comptador>0){
            if(undo>0){
                comptador--;
                undo--;
            }
        }
    }
    public void setLimit(int a){
       
        if(comptador>a){
             throw new IllegalArgumentException("El valor introduit es massa gran");
        }
        max=a;
    }
    public int getComptador(){
        return this.comptador;
    }
}
