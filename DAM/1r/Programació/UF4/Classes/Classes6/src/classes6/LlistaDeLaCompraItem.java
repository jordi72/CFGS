package classes6;

/**
 *
 * @author jordi72
 */
public class LlistaDeLaCompraItem {
    private String name;
    private int quantity;
    private double price;
    
    
    public LlistaDeLaCompraItem(String nom, int quantitat, double preuUnitari){
    this.name=nom;
    this.price=preuUnitari;
    this.quantity=quantitat;
    }
    public double getImportTotal(){
       double a = price*quantity;
       return a;
    }
    public void setQuantitat(int quantitat){
        this.quantity=quantitat;
    }
    public String getItemTiquetCompre(){
        String a="";
        a = this.name + ", " + String.valueOf(quantity) + ", " + String.valueOf(price);
        return a;
    }
}
