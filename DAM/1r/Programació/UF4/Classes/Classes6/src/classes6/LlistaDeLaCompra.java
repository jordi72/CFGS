package classes6;

import java.util.ArrayList;

/**
 *
 * @author jordi72
 */
public class LlistaDeLaCompra {

    ArrayList<LlistaDeLaCompraItem> llistaCompraItems = new ArrayList<>();

    public LlistaDeLaCompra() {

    }

    public void afegir(LlistaDeLaCompraItem item) {
        llistaCompraItems.add(item);
    }

    public double getImportTotal() {
        double a = 0;
        for (int i = 0; i < llistaCompraItems.size(); i++) {
            a = a + llistaCompraItems.get(i).getImportTotal();
        }
        return a;
    }

    public String getTiquetCompra() {
        String a = "";
        for (int i = 0; i < llistaCompraItems.size(); i++) {
            a = a + llistaCompraItems.get(i).getItemTiquetCompre();
        }
        return a;
    }
}
