package objectes3;

/**
 *
 * @author Familia
 */
public class Fraccio {

    private int numerador;
    private int denominador;

    public Fraccio(int a, int b) {
        if (b == 0) {
            this.denominador = 1;
            this.numerador = a;
        }
        if (b < 0 && a > 0) {
            this.numerador = a;
            this.numerador *= -1;
            this.denominador = b;
            this.denominador *= -1;
        }
        if (b < 0 && a < 0) {
            this.denominador = b;
            this.denominador *= -1;
            this.numerador = a;
            this.numerador *= -1;
        }
        if (b > 0) {
            this.denominador = b;
            this.numerador = a;
        }
        if (a == 0 || b == 0) {
            this.numerador = 1;
            this.denominador = 1;
        }
        reduir();
    }

    private void reduir() {
        int i = mcd(this.denominador, this.numerador);
        this.denominador = this.denominador / i;
        this.numerador = this.numerador / i;
    }

    private int mcd(int num1, int num2) {
        num1=Math.abs(num1);
        num2=Math.abs(num2);
        // Algoritme d'Euclides
        while (num1 != num2) {
            if (num1 > num2) {
                num1 = num1 - num2;
            } else {
                num2 = num2 - num1;
            }
        }

        return num1;
    }

    @Override
    public String toString() {
        String result = "";
        int num1 = this.numerador;
        int num2 = this.denominador;
        result = String.valueOf(num1) + "/" + String.valueOf(num2);
        return result;
    }
    
    @Override
    public boolean equals(Object obj){
        boolean result=false;
        if (obj instanceof Fraccio) {
            Fraccio op2 = (Fraccio) obj;
            if (this.numerador==op2.numerador && this.denominador==op2.denominador)
                result = true;
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + this.numerador;
        hash = 71 * hash + this.denominador;
        return hash;
    }

    public int getNumerador() {
        return this.numerador;
    }

    public int getDenominador() {
        return this.denominador;
    }

    public Fraccio negar() {
        this.numerador = this.numerador *= -1;
        Fraccio f = new Fraccio(this.numerador, this.denominador);
        return f;
    }

    public Fraccio reciproc() {
        int aux = this.denominador;
        this.denominador = this.numerador;
        this.numerador = aux;
        Fraccio f = new Fraccio(this.numerador, this.denominador);
        return f;
    }

    public Fraccio sumar(Fraccio op2) {
        if (this.denominador == op2.denominador) {
            int a = this.numerador + op2.numerador;
            int b = this.denominador;
            Fraccio f = new Fraccio(a, b);
            return f;
        } else {
            int aux = this.denominador * op2.denominador;
            int a = this.numerador * op2.denominador;
            int b = op2.numerador * this.denominador;
            int result = a + b;
            Fraccio s = new Fraccio(result, aux);
            return s;
        }
    }

    public Fraccio restar(Fraccio op2) {
        if (this.denominador == op2.denominador) {
            int a = this.numerador + op2.numerador;
            int b = this.denominador;
            Fraccio f = new Fraccio(a, b);
            return f;
        } else {
            int aux = this.denominador * op2.denominador;
            int a = this.numerador * op2.denominador;
            int b = op2.numerador * this.denominador;
            int result = a - b;
            Fraccio s = new Fraccio(result, aux);
            return s;
        }
    }

    public Fraccio multiplicar(Fraccio op2) {
        int a = this.numerador * op2.numerador;
        int b = this.denominador * op2.denominador;
        Fraccio f = new Fraccio(a, b);
        return f;
    }

    public Fraccio dividir(Fraccio op2) {
        int a = this.numerador * op2.denominador;
        int b = this.denominador * op2.numerador;
        Fraccio f = new Fraccio(a, b);
        return f;
    }
}
