package objectes3;

/**
 *
 * @author Familia
 */
public class Objectes3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here       
        Fraccio f = new Fraccio(2, 4);
        Fraccio s = new Fraccio(1, 3);
        System.out.println("Totels les Fraccions mostrades a continuació estan simplificades");
        System.out.println("(" + f + ")" + "+" + "(" +  s + ")" + "=" + f.sumar(s));
        System.out.println("(" + f + ")" + "-" + "(" +  s + ")" + "=" + f.restar(s));
        System.out.println("(" + f + ")" + "*" + "(" +  s + ")" + "=" + f.multiplicar(s));
        System.out.println("(" + f + ")" + "/" + "(" +  s + ")" + "=" + f.dividir(s));
        System.out.println("Negació" + f.negar() + ", " + s.negar());
        System.out.println("Reciproc: " + f.reciproc() + ", " + s.reciproc());
        System.out.println("Equals: " + f.equals(s));
    }

}
