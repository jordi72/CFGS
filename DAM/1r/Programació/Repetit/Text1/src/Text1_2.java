import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Text1_2 {

	public static void main(String[] args) {
		
		funcio1();

	}
	
	public static void funcio1() {
		Scanner in = new Scanner(System.in);
		
		String fila;
		int numero = 1;
		
		PrintWriter writer = null;
		BufferedReader reader = null;
		
		String fitxer1, fitxer2;
		
		
		System.out.println("Nom del fitxer a llegir: ");
		fitxer1 = in.nextLine();
		
		System.out.println("Nom del fitxer a escriure: ");
		fitxer2 = in.nextLine();
		
		File file1 = new File(fitxer1);
		File file2 = new File(fitxer2);
		
		
		try {
			 reader = new BufferedReader(new FileReader(file1));
			 writer = new PrintWriter(new FileWriter(file2));
			while((fila = reader.readLine()) != null) {
				writer.printf("/* %5d */ %s",numero, fila+="\n");
				numero++;
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			writer.close();
		}
	}

}
