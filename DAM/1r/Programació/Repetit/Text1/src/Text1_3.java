import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

public class Text1_3 {

	public static void main(String[] args) {

		funcio1(5, 3);

	}

	public static void funcio1(int filas, int columnas) {
		Random ran = new Random();
		// i horitzontal
		// j vertical
		float num ;
		PrintWriter escriptor = null;
		try {
			escriptor = new PrintWriter(new FileWriter(new File("ex3.txt")));
			for (int j = 0; j < filas; j++) {
				for (int i = 0; i < columnas; i++) {
					escriptor.printf("%15f", num = ran.nextFloat());
					System.out.print(num*100 + " ");
				}
				System.out.println();
				escriptor.println();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			escriptor.close();
		}
	}
}
