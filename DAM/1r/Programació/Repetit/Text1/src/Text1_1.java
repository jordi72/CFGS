import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class Text1_1 {

	public static void main(String[] args) {
		
		try {
			funcio1(12);
			funcio2();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void funcio1(int n) throws IOException {
		String hola = "Hola, món!!\n";
		File file = new File("hola.txt");
		Writer writer = new BufferedWriter(new FileWriter(file));
		for (int i = 0 ; i<n ; i++) {
			writer.write(hola);
		}
		writer.close();
	}
	
	public static void funcio2() {
		String fila;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("hola.txt")));
			while((fila = reader.readLine()) != null) {
				System.out.println(fila);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
