import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class Text1_6 {

	public static void main(String[] args) {

		String word = checkMisspellings();
		System.out.println(word);

	}

	public static String checkMisspellings() {
		String toPrint = "";
		try {
			
			File file2 = new File("ContesAndersen.txt");
			
			Scanner toBeReaded = new Scanner(file2);
			String word = "";

			while (toBeReaded.hasNext()) {
				word = toBeReaded.next();
				boolean isWord = checkWord(word);
				if(isWord) {
					toPrint = " " + word + " ";
				}else {
					toPrint = "<span style=\"color:red\">" + word + "</span>";
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return toPrint;
	}
	
	public static boolean checkWord(String word) {
		File file1 = new File("catala.txt");
		Scanner dictionari = null;
		try {
			dictionari = new Scanner(file1);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean retorn = false;
		while (dictionari.hasNext()) {
			String dictionariWord = dictionari.next();
			if (word.equals(dictionariWord)) {
				//toPrint = "<span>" + word + "</span>";
				retorn = true;
				break;
			} else {
				//toPrint = "<span style=\"color:red\">" + word + "</span>";
				retorn = false;
			}
		
		}
		dictionari.close();
		return retorn;
	}

}
