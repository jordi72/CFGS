import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Text1_7 {
	
	//public static List<>

	public static void main(String[] args) {
	
		switch(args.length) {
		case 1 : {
			ajuda();
			break;
		}
		case 4 : {
			encriptar(args);
			break;
		}
		case 5 : {
			//desencriptar()
		}
		default : {
			System.err.println("Mala Sintaxis, revisa la ajuda escrivint: java Text1_7");
		}
		}
	}
	
	
	public static void encriptar(String[] args) {
		
		File origen = new File(args[2]);
		File desti = new File(args[3]);
		String opcio = args[0];
		int numero = Integer.valueOf(args[1]);
		
		
		Scanner in = null;
		PrintWriter out = null;
		try {
			in = new Scanner(origen);
			out = new PrintWriter(new FileWriter(desti));
			
			while(in.hasNext()) {
				String paraula = in.next();
				char[] lletras = paraula.toLowerCase().toCharArray();
				String toWrite = "";
				for(int i = 0 ; i< lletras.length; i++) {
					char lletra = lletras[i];
					String lletraString = "";
					lletraString.valueOf(lletra);
					toWrite += contador(lletraString, numero);
				}
				out.write(toWrite);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	
	public static char contador(String lletra, int numero) {
		String alfabet = "abcdefghijklmnopqrstuvwxyz";
		char[] lletrasAlfa = alfabet.toCharArray();
		
		int posicio = (alfabet.indexOf(lletra)+numero)%26;
		
		char retorn = alfabet.charAt(posicio);		
		
		return retorn;
	}
	
	public static void ajuda() {
		System.err.println("Per encriptar: java Caesar -k numero fitxerAEncriptar fitxerfinal\n Per Desencriptar: java Caesar -d -k numero fitxerADesencriptar fitxerResultant");
	}
	
	

}
