import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Text1_4 {

	public static void main(String[] args) {
		Scanner read = new Scanner(System.in);
		System.out.println("Fitxer a llegir: ");
		String file = read.nextLine();
		try {
			Scanner in = new Scanner(new File(file));
			Scanner in2 = new Scanner(new File(file));
			Scanner in3 = new Scanner(new File(file));
			chars(in);
			words(in2);
			lines(in3);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void chars(Scanner in) {
		int total = 0;
		String paraula;
		while(in.hasNext()) {
			paraula = in.next();
			total += paraula.length();
		}
		System.out.println("Caracters totals: " + total);
	}
	
	public static void words(Scanner in) {
		int total =  0;
		while(in.hasNext()) {
			in.next();
			total++;
		}
		System.out.println("Paraules totals: " + total);
	}
	
	public static void lines(Scanner in) {
		int total = 0;
		while(in.hasNext()) {
			in.nextLine();
			total++;
		}
		System.out.println("Lineas totals: " + total);
	}

}
