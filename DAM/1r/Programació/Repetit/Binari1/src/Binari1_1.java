import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Binari1_1 {

	public static void main(String[] args) {
		
		try {
			File file = new File("catala.txt");
			FileInputStream llegidor = new FileInputStream(file);
			FileOutputStream escriptor = new FileOutputStream(new File("encriptat.sarrato"));
			int i =  llegidor.read();
			while(i != -1) {
				i = ~i;
				escriptor.write(i);
				System.out.println(i);
				i = llegidor.read();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
