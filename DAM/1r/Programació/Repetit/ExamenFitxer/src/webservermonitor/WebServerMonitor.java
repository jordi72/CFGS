package webservermonitor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Scanner;

public class WebServerMonitor {

    public final static String WEB_SERVER_LOGS_DIRECTORY = "./webserverlogs/";
    public final static String ERROR_CODES_PATH = "./errorcodes/errorcodes.txt";
    public final static String REPORTS_DIRECTORY = "./reports/";
    public final static String PLANTILLA_PATH = "./reports/plantilla.html";

    public static void main(String[] args) {

        File logsPath = new File(WEB_SERVER_LOGS_DIRECTORY);
        for (File f : logsPath.listFiles()) {

            int totalConnections = getTotalConnections(f);
            int totalOK = getTotalOK(f);
            String fileName = f.getName();
            ArrayList<String> errors = getErrorList(f);

            //debug(fileName, totalConnections, totalOK, errors);
            generateReport(fileName, totalConnections, totalOK, errors);

        }
    }

    public static void debug(String fileName, int totalConnections, int totalOK, ArrayList<String> errors) {
        System.out.println();
        System.out.println();
        System.out.println(fileName);
        System.out.println("--------------------------");
        System.out.printf("Total connections: %d %n", totalConnections);
        System.out.printf("Total OK: %d %n", totalOK);

        for (String error : errors) {
            System.out.println(error);
        }
    }

    /**
     * Retorna el nombre de parelles id, codiError del fitxer f
     *
     * @param f Fitxer webserver_xxx_.dat d'entrada
     * @return El nombre de conexions realitzades
     */
    public static int getTotalConnections(File f) {
    	int count = 0;
    	try {
			RandomAccessFile random = new RandomAccessFile(f, "rw");

			int milis = 0;
			int code = 0;
			long apuntador = random.getFilePointer();
			while(apuntador!=f.length()) {
				random.seek(apuntador);
				milis = random.readInt();
				code = random.readInt();
				count++;
				apuntador+=8;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	System.out.println(f.getName());
    	System.out.println("Total Errors: " + count);
        return count;
    }

    /**
     * Retorna el nombre de conexions amb codi d'error 200 realitzades
     *
     * @param f Fitxer webserver_xxx_.dat d'entrada
     * @return El nombre de connexions realitzades amb exit
     */
    public static int getTotalOK(File f) {
    	int count = 0;
    	try {
			RandomAccessFile random = new RandomAccessFile(f, "rw");

			int milis = 0;
			int code = 0;
			long apuntador = random.getFilePointer();
			while(apuntador!=f.length()) {
				random.seek(apuntador);
				milis = random.readInt();
				code = random.readInt();
				if(code==200) {
				count++;
				}
				
				apuntador+=8;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	//System.out.println(f.getName());
    	//System.out.println("Total Oks: " + count);
        return count;
    }

    /**
     * Retorna la llista d'errors (junt amb les seves descripcions) auditats al
     * fitxer f excepte els errors de codi 200. Per exemple si el fitxer .dat
     * conte la següent informació: 0 200 4 304 12 200 15 100
     *
     * La funció retornaria un ArrayList amb:
     *
     * 304 Not Modified (RFC 7232) 100 Continue
     *
     * Aquest mètode utilitza el mètode getErrorDescription(int).
     *
     * @param f Fitxer webserver_xxx_.dat d'entrada
     * @return La llista dels codis junt amb les descripcions dels errors.
     */
    public static ArrayList<String> getErrorList(File f) {
    	ArrayList<String> matriu = new ArrayList<>();
    	try {
			RandomAccessFile random = new RandomAccessFile(f, "rw");
			File errors = new File(ERROR_CODES_PATH);
			Scanner in = new Scanner(errors);
			int milis = 0;
			int code = 0;
			long apuntador = random.getFilePointer();
			while(apuntador!=f.length()) {
				random.seek(apuntador);
				milis = random.readInt();
				code = random.readInt();
				if(code!=200) {
					while(in.hasNextLine()) {
						String linea = in.nextLine();
						String[] tempArray = linea.split(" ");
						if(code==Integer.valueOf(tempArray[0])) {
							matriu.add(linea);
							System.out.println(code);
							System.out.println(linea);
							break;

						}
					}
				}
				
				apuntador+=8;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	System.out.println("List Size: " + matriu.size());
        return matriu;
    }

    /**
     * Retorna una cadena amb el codi d'error i la seva descripció segons les
     * dades ammagatzemades al fitxer errorcodes.txt.
     *
     * Per exemple: getErrorDescription(100) retornaria 100 Continue
     *
     * @param errorCode Codi d'error del qual es vol saber la descripció.
     * @return Codi d'error junt amb la descripció.
     */
    public static String getErrorDescription(int errorCode) {
    	String error = "";
    	File f = new File(ERROR_CODES_PATH);
    	try {
			Scanner in = new Scanner(f);
			while(in.hasNextLine()) {
				String linea = in.nextLine();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return "";
    }

    /**
     * Genera un informe omplint els camps de la plantilla PLANTILLA_PATH amb
     * els valors passats com a paràmetres.
     *
     * @param datFileName Nom del fitxer webserver_xxx_.dat origen de les dades
     * de l'informe. Valor del camp #filename#
     * @param totalConnections Valor del camp #totalcon#
     * @param totalOK Valor del camp #totalok#
     * @param errors Valor del camp #errorlist#
     */
    public static void generateReport(String datFileName, int totalConnections, int totalOK, ArrayList<String> errors) {

        String plantillaBuida = "";
        String plantillaPlena = "";
        try {
            plantillaBuida = loadPlantilla();
            plantillaPlena = omplirPlantilla(plantillaBuida, totalConnections, totalOK, errors, datFileName);
            guardarPlantilla(datFileName, plantillaPlena);

        } catch (FileNotFoundException ex) {
            System.out.println("No s'ha pogut generar l'informe correctament");
            ex.printStackTrace();
        }

    }

    /**s
     * Carrega la plantilla indicada a PLANTILLA_PATH i la retorna en una
     * cadena.
     *
     * @return plantilla carregada.
     */
    public static String loadPlantilla() {
    	File f = new File(PLANTILLA_PATH);
    	String plantilla = "";
    	try {
			Scanner in = new Scanner(f);
			while(in.hasNext()) {
				plantilla += in.nextLine();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	System.out.println(plantilla);
        return plantilla;
    }

    /**
     * Omple la plantilla emmagatzemada a plantillaBuida amb els camps
     * corresponents.
     *
     * @param plantillaBuida Plantilla amb els camps per substituir.
     * @param totalConnections Valor del camp #totalcon#
     * @param totalOK Valor del camp #totalok#
     * @param errors Valor del camp #errorlist#
     * @param datFileName Nom del fitxer webserver_xxx_.dat origen de les dades
     * de l'informe. Valor del camp #filename#
     * @return Una cadena amb els camps de la plantilla substituits pesl seus
     * valors.
     */
    public static String omplirPlantilla(String plantillaBuida, int totalConnections, int totalOK, ArrayList<String> errors, String datFileName) {
    	
    	Scanner in = new Scanner(plantillaBuida);
    	in.findInLine("#");
    	
        return "";
    }

    /**
     * Guarda la cadena plantillaPlena a REPORTS_DIRECTORY amb nom
     * plantillaFileName.html
     *
     * @param plantillaFileName Nom del fitxer .dat del qual s'ha generat la
     * plantilla.
     * @param plantillaPlena Cadena amb la plantilla plena amb la informació
     * extreta del fitxer .dat plantillaFileName
     */
    public static void guardarPlantilla(String plantillaFileName, String plantillaPlena) throws FileNotFoundException {
    	File f = new File(plantillaFileName + ".html");
    	
    	PrintWriter escriptor = new PrintWriter(f);
    	escriptor.write(plantillaPlena);
    	    }

}
