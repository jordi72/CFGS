package ciutats;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ciutats {

    public static final String RUTA_FITXER_CITIES = "./src/data/city.txt";
    public static final String RUTA_FITXER_COUNTRIES = "./src/data/country.txt";
    public static final String RUTA_CARPETA_CONSULTES = "./queries";
    public static Scanner in = null;

    public static void main(String[] args) {
        // TODO
        showMenu();
    }

    /**
     * Mostra el menú amb les diferents opcions del programa per pantalla.
     */
    public static void showMenu() {
        System.out.println();
        System.out.println("1.- Mostrar Paisos");
        System.out.println("2.- Mostrar ciutats d'un país");
        System.out.println("3.- Buscar ciutats per nom");
        System.out.println("4.- Guardar última consulta de ciutats");
        System.out.println("5.- Sortir");
        chooseOption();
    }
    
    public static void chooseOption(){
        in = new Scanner(System.in);
        String chooseAnOption = "Introdueix una opció: ";
        System.out.println(chooseAnOption);
        int number = 0;
            if(in.hasNextInt()){
            number = in.nextInt();
        }
        switch(number){
            case 1 : {
                getCountries();
            }
            case 2 : { 
                getCities("");
            }
            case 5 : {
                break;
            }
            default : {
                showMenu();
            }
        }
    }
    
    /**
     * Retorna un ArrayList amb tots els paisos del fitxer de paisos.
     *
     * @return ArrayList amb tots els paisos del fitxer de paisos.
     */
    public static ArrayList<Country> getCountries() {
        // TODO
        in = new Scanner(RUTA_FITXER_COUNTRIES);
        ArrayList<Country> countries = new ArrayList<>();
        Country country = null;
        String[] currentCountry = null;
        while(in.hasNextLine()){
            country = new Country();
            currentCountry = in.nextLine().split("\t");
            country.code = currentCountry[0];
            country.name = currentCountry[1];
            country.continent = currentCountry[2];
            country.region = currentCountry[3];
            country.localName = currentCountry[10];
            countries.add(country);
        }
        return countries;
    }

    public static ArrayList<City> getCities(String countryCode) {
        // TODO
        return null;
    }

    public static ArrayList<City> findCities(String cityNameFilter) {
        // TODO 
        return null;
    }

    /**
     * Crea la carpeta on emmagatzemar les consultes en cas que no existeixi.
     */
    public static void createQueriesDirectory() {
        // TODO
    }

    /**
     * Guarda la última consulta realitzada referent a ciutats a la carpeta de
     * consultes.
     *
     * @param fileName Nom del fitxer de la consulta
     * @return -1 Si no existeix una última consulta o si hi ha hagut algun
     * error en la creació del fitxer. 0 Si el fitxer s'ha creat correctament.
     */
    public static int saveLastQuery(String fileName) {
        // TODO
        return 0;
    }

    /**
     * Retorna una estructura Country a partir d'una cadena amb els camps de
     * l'estructura separats per un separador.
     *
     * @param registreCountry Cadena amb els camps de l'estructura.
     * @param separator Separador dels diferents camps de la cadena.
     * @return Una estructura Country amb els camps actualitzats.
     */
    public static Country registreToCountry(String registreCountry, String separator) {
        // TODO
        return null;

    }

    /**
     * Retorna una estructura City a partir d'una cadena amb els camps de
     * l'estructura separats per un separador.
     *
     * @param registreCity Cadena amb els camps de l'estructura.
     * @param separator Separador dels diferents camps de la cadena.
     * @return Una estructura City amb els camps actualitzats.
     */
    public static City registreToCity(String registreCity, String separator) {
        // TODO 
        return null;
    }

    /**
     * Retorna una cadena amb els camps d'una estructura Country amb les
     * sangries i separadors adients per a la seva impressió.
     *
     * @param c Estructura amb les dades.
     * @return Cadena amb les dades de la estructura amb format.
     */
    public static String formatCountry(Country c) {
        // TODO 
        return null;
    }

    /**
     * Retorna una cadena amb els camps d'una estructura City amb les sangries i
     * separadors adients per a la seva impressió.
     *
     * @param c Estructura amb les dades.
     * @return Cadena amb les dades de la estructura amb format.
     */
    public static String formatCity(City c) {
        // TODO 
        return null;
    }

    /**
     * Mostra per pantalla el contingut, amb format, de la col·lecció cities.
     *
     * @param cities
     */
    public static void showCities(ArrayList<City> cities) {
        // TODO
    }

    /**
     * Mostra per pantalla el contingut, amb format, de la col·lecció countries.
     *
     * @param countries
     */
    public static void showCountries(ArrayList<Country> countries) {
        // TODO
    }
}

class City {

    int id; // Field 0
    String name; // Field 1 length 35
    String countryCode; // Field 2 length 3
    String district; // Field 3 length 20
    int population; // Field 4
}

class Country {

    String code; // Field 0 length 3
    String name; // Field 1 length 52
    String continent; // Field 2 length 15
    String region; // Field 3 length 26
    String localName; // Field 10 length 45
}
