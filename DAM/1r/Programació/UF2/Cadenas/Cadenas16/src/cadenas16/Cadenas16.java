package cadenas16;

import java.util.Scanner;

/**
 *
 * @author joker
 */
public class Cadenas16 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        String binari = "";
        String lletraConvertida = "";
        int multiplcador = 1;
        int valor = 0;
        int numeroDecimal = 0;
        boolean fiBucle = false;
        char lletra = 'a';
        int joker = 0;
        int limit = 0;
        int llargada = 0;
        String hexadecimalSenseGirar = "";
        String hexadecimalGirat = "";

        int[] taula = {10, 11, 12, 13, 14, 15};
        String[] taulaLletras = {"A", "B", "C", "D", "E", "F"};

        System.out.println("binari");
        binari = s.nextLine();

        llargada = binari.length();
        while (llargada % 4 != 0) {
            binari = "0" + binari;
            llargada = binari.length();
        }

        for (int i = binari.length() - 1; fiBucle == false; i--) {

            lletra = binari.charAt(i);
            lletraConvertida = String.valueOf(lletra);
            valor = Integer.parseInt(lletraConvertida);
            if (valor == 0) {

            } else {
                numeroDecimal = numeroDecimal + multiplcador;
            }

            if (joker == 0) {
                multiplcador = multiplcador + 1;
            } else {
                multiplcador = multiplcador * 2;
            }
            limit++;
            joker++;

            if (limit == 4) {
                limit = 0;
                joker = 0;
                multiplcador = 1;

                if (numeroDecimal > 9) {
                    for (int j = 0; j < taula.length; j++) {
                        if (numeroDecimal == taula[j]) {
                            hexadecimalSenseGirar = hexadecimalSenseGirar + taulaLletras[j];
                        }

                    }
                } else {
                    hexadecimalSenseGirar = hexadecimalSenseGirar + numeroDecimal;
                }
                numeroDecimal = 0;
            }

            
            if (i == 0) {
                fiBucle = true;
            }

        }

        fiBucle = false;
        for (int i = hexadecimalSenseGirar.length() - 1; fiBucle == false; i--) {
            lletra = hexadecimalSenseGirar.charAt(i);
            hexadecimalGirat = hexadecimalGirat + lletra;

            if (i == 0) {
                fiBucle = true;
            }
        }

        System.out.println(hexadecimalGirat);
    }

}
