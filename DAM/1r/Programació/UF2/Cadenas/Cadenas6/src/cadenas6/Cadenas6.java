package cadenas6;

import java.util.Scanner;

/**
 *
 * @author joker
 */
public class Cadenas6 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        String frase1 = "";
        String frase2 = "";
        String comprobacio = "";
        char lletraBuscar2 = 'a';
        char lletraBuscar1 = 'a';
        boolean existeix = false;
        int llargada = 0;
        int posicio1 = 0;
        

        System.out.println("cadena  1");
        frase1 = s.nextLine();

        System.out.println("cadena 2");
        frase2 = s.nextLine();

        llargada = frase2.length();

        for (int i = 0; i < frase1.length(); i++) {
            posicio1++;
            lletraBuscar2 = frase2.charAt(0);
            lletraBuscar1 = frase1.charAt(i);

            if (lletraBuscar2 == lletraBuscar1 && existeix == false) {
                comprobacio = frase1.substring(posicio1 - 1, posicio1 + llargada - 1);
                if (comprobacio.equals(frase2)) {
                    existeix = true;
                }
            }

        }

        if (existeix) {
            System.out.println("la cadena existeix");
        } else {
            System.out.println("la cadena no existeix ");
        }

    }

}
