/*@author: Jordi72*/
package cadenas7;

import java.util.Scanner;

public class Cadenas7 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        String pass;
        char letter = 'd';
        boolean caps = false;
        boolean minus = false;
        boolean digits = false;
        int count = 0;
        boolean total = false;

        System.out.println("Dona'm una contraseña");
        pass = s.nextLine();

        if (pass.length() > 7) {
            for (int i = 0; i < pass.length(); i++) {
                letter = pass.charAt(i);
                if (Character.isUpperCase(letter)) {
                    caps = true;
                }
                if (Character.isLowerCase(letter)) {
                    minus = true;
                }
                if (Character.isDigit(letter)){
                    count++;
                }
            }
        }
        if(caps==true&&minus==true&&count>=2){
           total=true; 
        }
        if(total==true){
            System.out.println("Contraseña Correcte");
        }else{
            System.out.println("Contraseña Incorrecte");
        }

    }

}
