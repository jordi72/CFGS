//@author: Jordi72
package cadenas2;
import java.util.Scanner;
public class Cadenas2 {
   public static void main(String[] args) {
       Scanner s = new Scanner(System.in);
       
       String paraula;
       
       System.out.println("Dona'm una paraula.");
       paraula = s.nextLine();
       
       for(int i=0; i<paraula.length(); i++){
           if(Character.isUpperCase(paraula.charAt(i))){
               System.out.print(Character.toLowerCase(paraula.charAt(i)));
           }else{
               System.out.print(Character.toUpperCase(paraula.charAt(i)));
           }
       }
    }   
}