package cadenas19;

import java.util.Scanner;

/**
 *
 * @author joker
 */
public class Cadenas19 {

    public static void main(String[] args) {

        /*
        El nostre codi esta dividit en seccions
        Exemple de una seccio
        Seccio Exemple
        "Comentari de que fa aquesta seccio de codi"
        Linia18 <codi necesari per fer la seccio Exemple, dins del codi pot tenir mes comentaris> Linia 19
        Fi Seccio Exemple
        
        Seccio1 
        "Aquesta seccio demenem una paraula al usuari, si no ens posa cap paraula agafarem la paraula joker, si ens posa una cadena de
         text, nomes agafarem la primera paraula"
         Linia 86 fins la Linia 101
        
        Seccio2 
        "Aquesta seecio inicia el bucle principal, i ensenya una unica vegada la paraula amb asteriscos i el dibuix del penjat sense la persona. 
         Tambè anira mostraran las lletres que ha escrit l'usuari apartir de la primera volta"
         Linia 103 fins la Linia 138
        
        Seccio3
        "Aquesta seccio demanem la lletra, i comprobarem si la lletra esta repetida"
         Linia 140 fins la Linia 168
        
        Seccio4
        "Aquesta seccio mirarem si la lletra que ens diu l'usuari es correcta, si la lletra es correcta la guardarem en una matriu anomenada
         lletrasTrobades , si la lletra no es correcta i es repetida no la guardarem i aumentarem el contador de intents que te l'usuari"
         Linia 170 fins la Linia 185
        
        Seccio5
        "Aquesta seccio imprimirem el dibuix del penjat"
         Linia 187 fins la Linia 227
        
        Seccio 6
        "Aquesta seccio ensenya la paraula amb asteriscos o be sense depenen si l'usuari ha encertat amb la lletra"
         Linia 229 fins la Linia 248
        
        Seccio 7
        "Aquesta seccio controla quan l'usuari guanya o be perd, en aquesta seccio hi ha una variable anomenada victoria que s'anira aumentan
         cada cop que trobi una lletra de la paraula en la matriu lletresTrobades, si les troba totes l'usuari guanya"
         Linia 250 fins la Linia 275
        
        La idea principal del prorgrama es la seguent: agafem la paraula que posa l'usuari i la guardem amb una matriu de char, despres 
        anirem demenant lletres. Quan trobem que una lletra es igual a qualsevol lletra de la matriu de char, aquesta lletra la guardarem amb una
        altre matriu(lletraTrobada), la matriu lletraTrobada es la que utlitizarem per imprimir la paraula i per saber quan l'usuari guanya  
         */
        Scanner s = new Scanner(System.in);

        char lletra = 'a';
        String[] personaPenjada = {"|", "o", "/", "\\", ""};
        String lletraUsuari = "";
        String paraula = "";

        boolean lletraTrobada = false;
        boolean lletraCorrecta = false;
        boolean fiPenjat = false;
        boolean lletraRepetida = true;
        char[] lletresTrobades = new char[27];
        char[] lletresEscrites = new char[27];
        char[] lletresCorrectas;

        int contadorLletresTrobades = 0;
        int contadorLletresEscrites = 0;
        int intents = 0;
        int victoria = 0;
        int corda = 4;
        int cap = 4;
        int cos = 4;
        int esquerra = 4;
        int dreta = 4;
        int cintura = 4;
        int camaEsquerra = 4;
        int camaDreta = 4;
        int espai = 0;

        // Seccio 1
        System.out.print("paraula del penjat ");
        paraula = s.nextLine().toLowerCase().trim();
        
        if (paraula.length() == 0) {
            paraula = "joker";
        } else {
            paraula = paraula + " ";
            espai = paraula.indexOf(" ");
            paraula = paraula.substring(0, espai).toLowerCase();
        }

        // lletresCorrectas es una matriu que tindra la paraula secreta
        lletresCorrectas = paraula.toCharArray();

        // Fi Seccio1
        
        // Seccio2
        for (int i = 0; fiPenjat == false; i++) {
            // fem un reset de variables perque el programa funcioni correctament
            victoria = 0;

            if (i == 0) {
                System.out.println("   ____");
                System.out.println("  |");
                System.out.println("  |");
                System.out.println("  |");
                System.out.println("  |");
                System.out.println("  |");
                System.out.println(" _|_");
                System.out.println("|   |______");
                System.out.println("|          |");
                System.out.println("|__________|");
                System.out.println("");
            }

            if (i == 0) {
                System.out.print("Paraula ");
                for (int j = 0; j < paraula.length(); j++) {
                    System.out.print("*");
                }
                System.out.println("");
                System.out.println("Lletres ");
            }

            if (i > 0) {
                System.out.print("Lletres ");
                for (int j = 0; j < lletresEscrites.length; j++) {
                    System.out.print(lletresEscrites[j]);
                }
                System.out.println("");
            }
            // Fi Seccio2

            //Seccio 3
            //demanem la lletra al usuari i la posem minuscula
            System.out.print("Introdueix lletra ");
            lletraUsuari = s.nextLine();
            lletra = lletraUsuari.charAt(0);
            lletra = Character.toLowerCase(lletra);

            //la primera lletra que escrigui l'usuari la guardarem , ja que es impossible que estigui repetida i aumentarem el seu contador
            if (i == 0) {
                lletresEscrites[contadorLletresEscrites] = lletra;
                contadorLletresEscrites++;
            }

            // mirem si la lletra que ens diu l'usuari ja existeix, si no la troba lletraRepetida valdra false 
            if (i > 0) {
                lletraRepetida = false;
                for (int j = 0; j < lletresEscrites.length; j++) {
                    if (lletra == lletresEscrites[j]) {
                        lletraRepetida = true;
                    }
                }
            }

            // si lletraRepetida val false guardarem la lletra en la matriu de les lletres repetidas i aumentarem el seu contador
            if (lletraRepetida == false) {
                lletresEscrites[contadorLletresEscrites] = lletra;
                contadorLletresEscrites++;
            }
            //Fi Seccio 3

            // Seccio 4
            // bucle per mirar si la lletra es correcta
            lletraTrobada = false;
            for (int j = 0; j < lletresCorrectas.length && lletraTrobada == false; j++) {
                if (lletra == lletresCorrectas[j]) {
                    lletresTrobades[contadorLletresTrobades] = lletra;
                    contadorLletresTrobades++;
                    lletraTrobada = true;
                }
            }

            // si la lletra no es correcta i no esta repetida aumentarem la variable intents
            if (lletraTrobada == false && lletraRepetida == false) {
                intents++;
            }
            // Fi Seccio4

            // Seccio 5
            // aquest condicional i es per solucionar un error que tenia, quan iniciabas el programa i la lletra no era correcta no es dibuixaba
            // la corda
            if (i==0 && lletraTrobada == false){
                intents++;            
            }
            
            // aquestes bestias i son per nomes tindra que dibuixar  un cop el penjat
            if (intents == 1) {
                corda = 0;
            } else if (intents == 2) {
                cap = 1;
            } else if (intents == 3) {
                esquerra = 2;
            } else if (intents == 4) {
                cos = 0;
            } else if (intents == 5) {
                dreta = 3;
            } else if (intents == 6) {
                cintura = 0;
            } else if (intents == 7) {
                camaEsquerra = 2;
            } else if (intents == 8) {
                camaDreta = 3;
            }
            
            

            System.out.println("   ____");
            System.out.println("  |    " + personaPenjada[corda]);
            System.out.println("  |    " + personaPenjada[cap]);
            System.out.println("  |   " + personaPenjada[esquerra] + personaPenjada[cos] + personaPenjada[dreta]);
            System.out.println("  |    " + personaPenjada[cintura]);
            System.out.println("  |   " + personaPenjada[camaEsquerra] + " " + personaPenjada[camaDreta]);
            System.out.println(" _|_");
            System.out.println("|   |______");
            System.out.println("|          |");
            System.out.println("|__________|");

            System.out.print("Paraula  ");
            // Fi Seccio5

            // Seccio 6
            // amb aquest doble bucle agafem lletra a lletra de la paraula i despres mirem si existeix en la matriu on guardem
            // les lletres que son correctes
            for (int k = 0; k < paraula.length(); k++) {
                lletra = paraula.charAt(k);
                lletraCorrecta = false;
                for (int l = 0; l < lletresTrobades.length && lletraCorrecta == false; l++) {
                    if (lletra == lletresTrobades[l]) {
                        System.out.print(lletresTrobades[l]);
                        lletraCorrecta = true;
                        victoria++;
                    }

                }
                if (lletraCorrecta == false) {
                    System.out.print("*");
                }

            }
            // Fi Seccio 6

            // Seccio 7
            // quan l'usuari guanya ensenyarem el recompte de les lletres que habia escrit
            if (victoria == paraula.length()) {
                System.out.println("");
                System.out.print("Lletres ");
                for (int j = 0; j < lletresEscrites.length; j++) {
                    System.out.print(lletresEscrites[j]);
                }

                System.out.println("");
                System.out.println("el joker et felicita");
                fiPenjat = true;
            }

            // quan l'usuari perd ensenyarem el recompte de lletres que habia escrit 
            if (intents == 8) {
                System.out.println("");
                System.out.print("Lletres ");
                for (int j = 0; j < lletresEscrites.length; j++) {
                    System.out.print(lletresEscrites[j]);
                }
                System.out.println("");
                System.out.println("El joker et fara una visita per la nit");
                fiPenjat = true;
            }
            // Fi Seccio 7
            
            System.out.println("");

        }

    }

}
