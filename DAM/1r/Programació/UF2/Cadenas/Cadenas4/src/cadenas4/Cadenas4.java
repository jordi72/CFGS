/*
@author: jordi72
 */
package cadenas4;
import java.util.Scanner;
public class Cadenas4 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        
        String frase;
        
        String[] matriu;
        
        System.out.println("Dona'm una frase");
        frase = s.nextLine();
        
        matriu = frase.split(" ");
      
        for(int i=matriu.length-1; i>=0; i--){
            System.out.print(matriu[i] + " ");
        }
    }    
}
