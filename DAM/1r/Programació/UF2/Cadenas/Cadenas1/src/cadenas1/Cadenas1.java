//@author: Jordi72
package cadenas1;
import java.util.Scanner;
public class Cadenas1 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        
        char caracter;
        String cadena;
        int total = 0;
        
        
        System.out.println("Dona'm un caracter.");
        caracter= s.nextLine().charAt(0);
                
        System.out.println("Dona'm una cadena.");
        cadena = s.nextLine();
        
        for(int i=0; i<cadena.length();i++){
            if(cadena.charAt(i)==caracter){
            total++;
            }
        }
        System.out.println(total);
    }   
}
