package cadenas13;

import java.util.Scanner;

/**
 *
 * @author joker
 */
public class Cadenas13 {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);

        int binari = 0;
        int original = 0;
        String binariSenseGirar = "";
        String binariGirat = "";
        boolean fiBucle = false;
        char lletra = 'a';

        System.out.println("decimal");
        original = s.nextInt();

        while (original != 0) {
            binari = original % 2;
            original = original / 2;
            binariSenseGirar = binariSenseGirar + binari;

        }

        for (int i = binariSenseGirar.length() - 1; fiBucle == false; i--) {
            lletra = binariSenseGirar.charAt(i);
            binariGirat = binariGirat + lletra;

            if (i == 0) {
                fiBucle = true;
            }
        }

        System.out.println(binariGirat);

    }

}
