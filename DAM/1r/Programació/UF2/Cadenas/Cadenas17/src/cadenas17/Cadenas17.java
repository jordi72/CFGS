package cadenas17;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author joker
 */
public class Cadenas17 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Random r = new Random();

        String respostaUsuari = "";
        String respostaCorrecta = "";
        int numeroAleatori = 0;
        int intents = 0;
        int encerts = 0;
        boolean eleccioCorrecta = false;

        String[] taulaPais = {"Alemania", "China", "España", "Francia", "Grecia", "Italia", "Peru", "Portugal", "Suiza", "Russia"};
        String[] taulaCapital = {"Berlin", "Pekin", "Madrid", "Paris", "Atenas", "Roma", "Lima", "Lisboa", "Berna", "Moscu"};
        int[] taulaPaisRepetit = new int[taulaPais.length];
       //int[] taulaPaisRepetit = {99,99,99,99,99,99,99,99,99,99};

       
       for(int i =0; i<taulaPaisRepetit.length;i++){
           taulaPaisRepetit[i] = 500;
       }
       
        while (intents < 10) {
            if (intents == 0) {
                numeroAleatori = r.nextInt(taulaPais.length);
                System.out.print("Capital de " + taulaPais[numeroAleatori] + "  ");
                respostaUsuari = s.nextLine().trim();
                respostaCorrecta = taulaCapital[numeroAleatori];
            }

            if (intents > 0) {
                eleccioCorrecta = true;
                while (eleccioCorrecta == true) {
                    numeroAleatori = r.nextInt(taulaPais.length);
                    eleccioCorrecta=false;
                    for (int i = 0; i < taulaPaisRepetit.length; i++) { 
                        //if (i==0){
                            if (numeroAleatori == taulaPaisRepetit[i]) {
                                 eleccioCorrecta = true;
                            } 
                        //}
                        /*if (i>0){
                            if ( taulaPaisRepetit[i]!=0 && numeroAleatori == taulaPaisRepetit[i] ) {
                                 eleccioCorrecta = true;
                            } 
                        }*/
                       
                    }
                }
                System.out.print("Capital de " + taulaPais[numeroAleatori] + "  ");
                respostaUsuari = s.nextLine().trim();
                respostaCorrecta = taulaCapital[numeroAleatori];               
            }

            if (respostaUsuari.equalsIgnoreCase(respostaCorrecta)) {
                System.out.println("resposta correcta");
                encerts++;
            } else {
                System.out.println("resposta incorrecta");
            }

            taulaPaisRepetit[intents] = numeroAleatori;
            intents++;
        }

        System.out.println("encerts total: " + encerts);

    }

}
