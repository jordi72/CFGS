/*@author: Jordi72*/
package cadenas8;
import java.util.Scanner;
public class Cadenas8 {
    public static void main(String[] args) {
        Scanner s = new Scanner (System.in);
        
        String frase;
        String aux;
        String [] matriu;
        int contador=0;
        
        System.out.println("Dona'm una frase");
        frase = s.nextLine();
        
        System.out.println("Ara una lletra");
        aux = s.nextLine();
        char lletra = aux.charAt(0);
        matriu = frase.split(" ");
        
        for (int i = 0; i < matriu.length; i++) {
               if(lletra==(matriu[i].charAt(matriu[i].length()-1))){
                   contador++;
            }
        }
        System.out.println("En total hi ha " + contador + " paraules que acaben en " + lletra);
    }
    
}
