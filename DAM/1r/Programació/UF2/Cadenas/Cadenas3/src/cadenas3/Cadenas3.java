//@author: Jordi72
package cadenas3;
import java.util.Scanner;
public class Cadenas3 {
    public static void main(String[] args) {
       Scanner s = new Scanner(System.in);
       
       String [] vocals ={"a","e","i","o","u"};
       String userInput;
       boolean vocal=false;
      // boolean noVocal=false;
       
       System.out.println("Dona'm una lletra.");
       userInput = s.nextLine();
        
       for(int i=0; i<vocals.length; i++){
           if(userInput.equals(vocals[i])){
               vocal=true;
           }
       }
       if(vocal==true){
        System.out.println(userInput + " es una vocal.");
       }else{
        System.out.println(userInput + " es una consonant.");
       }
    }  
}
