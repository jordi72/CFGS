package cadenas10;

import java.util.Scanner;

/**
 *
 * @author joker
 */
public class Cadenas10 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        String frase = "";
        int suma = 0;
        char lletra = 'a';
        String lletraTransformada = "";

        System.out.println("frase");
        frase = s.nextLine();

        for (int i = 0; i < frase.length(); i++) {
            lletra = frase.charAt(i);

            if (Character.isDigit(lletra)) {
                lletraTransformada = String.valueOf(lletra);
                suma = suma + Integer.parseInt(lletraTransformada);

            }

        }

        System.out.println("suma " + suma);

    }

}
