package cadenas18;

import java.util.Scanner;

public class Cadenas18 {

    public static void main(String[] args) {
        String nif;
        String numeros;
        int numero;
        char lletra;
        int posicioGuio;
        int residu;
        String lletresDni = "TRWAGMYFPDXBNJZSQVHLCKE";
        char lletraCorrecta;
        Scanner s = new Scanner(System.in);

        System.out.println("Dni ");
        nif = s.nextLine().toUpperCase();

        posicioGuio = nif.indexOf("-");

        numeros = nif.substring(0, posicioGuio);
        numero = Integer.parseInt(numeros);

        lletra = nif.charAt(posicioGuio + 1);

        residu = numero % 23;

        lletraCorrecta = lletresDni.charAt(residu);

        if (lletra == lletraCorrecta) {
            System.out.println("dni correcte");
        } else {
            System.out.println("dni no correcte");
        }

    }

}
