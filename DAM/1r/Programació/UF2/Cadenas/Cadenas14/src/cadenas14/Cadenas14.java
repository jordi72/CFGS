package cadenas14;

import java.util.Scanner;

/**
 *
 * @author joker
 */
public class Cadenas14 {


    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        
        String frase="";
        char [] taulaLletras = {'a','b','c','d','e','f','g','h','i','j'};
        int  [] taulaAparicions = {0,0,0,0,0,0,0,0,0,0};
        char lletra ='a';
        System.out.println("frase");
        frase = s.nextLine();
        
        
        for (int i=0; i<frase.length(); i++){
            lletra = frase.charAt(i);
            for (int j=0; j<taulaLletras.length; j++){
                if (lletra==taulaLletras[j]){
                    taulaAparicions[j]=taulaAparicions[j]+1;
                }
            }
        }
        
        for (int i =0; i<taulaLletras.length; i++){
            if (taulaAparicions[i]!=0) {
            System.out.println(taulaLletras[i] + " apareix " + taulaAparicions[i]);
            }
        }
        
    }
    
}
