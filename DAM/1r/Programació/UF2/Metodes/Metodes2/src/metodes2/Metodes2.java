/*@author:Jordi72*/
package metodes2;

public class Metodes2 {

    public static void main(String[] args) {

        double minim = min(5,4,8);
        System.out.println("Minim: " + minim);
        
        double mitjana = mitjana(10.0,10.0,10.0);
        System.out.println("Mitjana Aritmetica: " + mitjana);
    }

    public static double min(double a, double b, double c) {
        double aux = a;
        if (b<a) {
            aux = b;
        } else if(c < b) {
            aux = c;
        }
        return aux;
    }

    public static double mitjana(double a, double b, double c) {
        double total = 0;

        total = (a + b + c) / 3.0;

        return total;
    }
}
