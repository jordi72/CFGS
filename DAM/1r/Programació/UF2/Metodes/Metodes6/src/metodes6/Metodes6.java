/*@author:Jordi72*/
package metodes6;

import java.util.Random;

public class Metodes6 {

    public static void main(String[] args) {
        int[][] sida;
        sida = generarMatriuBinaria(10);
        mostrarMatriu(sida);
    }

    public static int[][] generarMatriuBinaria(int dimensio) {
        Random rn = new Random();

        int[][] m = new int[dimensio][dimensio];
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m.length; j++) {
                m[i][j] = rn.nextInt(2);
            }
        }
        return m;
    }

    public static void mostrarMatriu(int m[][]) {

        for (int i = 0; i < m.length; i++) {

            for (int j = 0; j < m.length; j++) {
                System.out.print(m[i][j]);
            }
            System.out.println();
        }
    }
}
