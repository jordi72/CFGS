/*@author:Jordi72*/
package metodes1;

public class Metodes1 {

    public static void main(String[] args) {
       
        int total=sumaRang(4, 7);
        System.out.println(total);
    }

    public static int sumaRang(int n, int m) {
        int total =0;
        for(int i=n; i<=m; i++ ){
            total += i;
        }
        return total;
    }

}
