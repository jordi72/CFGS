/*@author:Jordi72*/
package metodes3;

public class Metodes3 {

    public static void main(String[] args) {

        boolean identics = totsIguals(5,5,2);
        if(identics==true){
            System.out.println("Totes els numeros son iguals");
        }else{
            System.out.println("Els numeros no son iguals");
        }
        
        boolean noIdentics = totsDiferents(1,2,2);
        if(noIdentics==true){
            System.out.println("Totes els numeros son tots diferents");
        }else{
            System.out.println("Els numeros no son tots diferents");
        }
        
        boolean ordenacio = estanOrdenats(1,3,2);
        if(ordenacio==true){
            System.out.println("Totes els numeros estan ordenats creixentment");
        }else{
            System.out.println("Els numeros no estan ordenats creixentment");
        }
    }

    public static boolean totsIguals(double a, double b, double c) {
        boolean iguals = false;

        if ((a == b) && (b == c)) {
            iguals = true;
        }
        return iguals;
    }

    public static boolean totsDiferents(double a, double b, double c) {
        boolean diferents = false;

        if ((a != b) && ((c != b))) {
            diferents = true;
        }
        return diferents;
    }

    public static boolean estanOrdenats(double a, double b, double c) {
        boolean ordenats = false;

        if ((a < b) && (b < c)) {

            ordenats = true;
        }
        return ordenats;
    }
}
