/*@author:Jordi72*/
package metodes7;

public class Metodes7 {

    public static void main(String[] args) {

        int [] m1 = {1,2,3,4,5,6,7,8,9};
        int [] m2 = {1,2,3,4,5,6,7,8,9};
        
        boolean m = equals(m1, m2);
        
        if(m == false){
            System.out.println("Les matrius no son iguals.");
        }else{
            System.out.println("Les matrius son iguals.");
        }
    }

    public static boolean equals(int[] a, int[] b) {
        
        boolean iguals = true;
        
        for(int i=0;i<a.length;i++){
            if(a[i] != b[i]){
                iguals = false;
            }
            
        }
        return iguals;
    }
}
