//Developer: Jordi Sarrato Mercad�
import java.util.Scanner;
public class Avet{
   public static void main(String[] args){
      Scanner s = new Scanner(System.in);
      
      String avet;
      boolean avetPetit = false;
      final double PREU_AVET_PETIT = 20;
      double preuAvet =0;
      boolean avetGran = false;
      final double PREU_AVET_GRAN = 25;
      final double PREU_AVET_NULL = 0;
      final double QUILO_O_FRACCIO = 4;
      boolean complements = false;
      double bolesUser;
      double bolesNadal;
      final double PREU_BOLES = 11;
      final double BOLES_DESCOMPTE = 5.5;
      String enviament ="";
      final double ENVIAMENT_PETIT = 15;
      final double ENVIAMENT_GRAN = 25;
      final double ENVIAMENT_NULL = 0;
      double preu_total_petit;
      double preu_total_gran;
      double preu_total_null; 
            
      System.out.println("Avet [G]ran, Avet [P]etit, Passar a [C]omplements:");
      avet = s.nextLine();
      
      if(avet.equals("g") || (avet.equals("G"))){
         avetGran = true;
      }    
      else if(avet.equals("p") || (avet.equals("P"))){
         avetPetit = true;
      }
      else if(avet.equals("c") || (avet.equals("C"))){
         complements = true;
      }
      else {
         System.out.println("Entrada Incorrecte!!");
      }
      
      //Capses
      if(avet.equals("c") || avet.equals("C")){
         complements = true;
      }
      System.out.println("Quantes capses de boles?");
      bolesUser = s.nextDouble();
      s.nextLine();
      bolesNadal = bolesUser*PREU_BOLES;
      
      //Descomptes
      
      if(avetPetit){
         if(bolesUser==1){
            preuAvet=BOLES_DESCOMPTE;
         } else if (bolesUser>1){
            preuAvet=bolesUser*PREU_BOLES-BOLES_DESCOMPTE;
         }     
      }
      
      if(avetGran){
         if(bolesUser==1){
            preuAvet=BOLES_DESCOMPTE;
         }
         else if(bolesUser==2){
            preuAvet=BOLES_DESCOMPTE*2;
         }
          else if (bolesUser>2){
            preuAvet=bolesUser*PREU_BOLES-BOLES_DESCOMPTE*2;
         }     
      }
      
      //Enviament
      if(avetGran || avetPetit){
      System.out.println("Enviament? s/N");
      enviament = s.nextLine();
      }
      
      
      //Output final
      
      System.out.println("--------------------------");
      System.out.println("Avet G/P: " + avet);
      if(avetPetit){
         System.out.println("Total Avet: " + PREU_AVET_PETIT);
      }
      else if(avetGran){
         System.out.println("Total Avet: " + PREU_AVET_GRAN);
      }else{
         System.out.println("Total Avet: " + PREU_AVET_NULL);
      }
      if(complements == false){
         System.out.println("Total Boles: " + preuAvet + " euros");
      } 
      else if (complements == true) {
         System.out.println("Total Boles: " + bolesNadal + " euros");
      }
      
      
      if(avetPetit){
         if((enviament.equals("s")) || (enviament.equals("S"))){
            System.out.println("Total Enviament " + ENVIAMENT_PETIT);  
         }
         if((enviament.equals("n")) || (enviament.equals("N"))){
            System.out.println("Total Enviament " + ENVIAMENT_NULL);
         }
      }
       if(avetGran){
         if((enviament.equals("s")) || (enviament.equals("S"))){
            System.out.println("Total Enviament " + ENVIAMENT_GRAN);  
         }
         if((enviament.equals("n")) || (enviament.equals("N"))){
            System.out.println("Total Enviament " + ENVIAMENT_NULL);
         }
      }
      if(avetPetit){
         if(enviament.equals("s") || enviament.equals("S")){
         preu_total_petit = PREU_AVET_PETIT + preuAvet + ENVIAMENT_PETIT;
         System.out.println("Total a Pagar " + preu_total_petit);
         }
         else{
         preu_total_petit = PREU_AVET_PETIT + preuAvet;
         System.out.println("Total a Pagar " + preu_total_petit);
         } 
      }
      if(avetGran) {
         if(enviament.equals("s") || enviament.equals("S")){
         preu_total_gran = PREU_AVET_GRAN + preuAvet + ENVIAMENT_GRAN;
         System.out.println("Total a Pagar " + preu_total_gran);
         }
         else{
         preu_total_gran = PREU_AVET_GRAN + preuAvet;
         System.out.println("Total a Pagar " + preu_total_gran);
         } 
      }
      if(complements){
         System.out.println("Total a Pagar " + bolesNadal);
      }
         
      System.out.println("--------------------------");   
   }
}