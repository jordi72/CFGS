import java.util.Scanner;
public class Seleccio7{
   public static void main(String[] args){
      Scanner sis = new Scanner(System.in);
      
      int numero1;
      int numero2;
      
      System.out.println("Indrodueix 1 numero");
      numero1 = sis.nextInt();
      
      System.out.println("Introdueix un altre numero");
      numero2 = sis.nextInt();
      
      if ((numero1 == 6) || (numero2 == 6)){
         System.out.println("Un dels 2 numeros es 6");
      }
      
      if (numero1 + numero2 == 6){
         System.out.println("La suma dels 2 numeros es 6");
      
      }    
      
      if (numero1 - numero2 == 6){
         System.out.println("La resta dels 2 numeros es 6");
      }
   }
}