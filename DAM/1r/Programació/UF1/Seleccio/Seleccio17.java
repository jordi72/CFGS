import java.util.Scanner;
public class Seleccio17{
   public static void main(String[] args){
      Scanner s = new Scanner(System.in);
      
      int user1;
      final String NOTA1 = "A";
      final String NOTA2 = "B";
      final String NOTA3 = "C";
      final String NOTA4 = "D";
      final String NOTA5 = "F";
      
      System.out.println("Digam una nota");
      user1 = s.nextInt();
      
      if((user1 >=90) && (user1 <=100)){
         System.out.println(NOTA1);
      }
      else if((user1 >=80) && (user1 <=89)){
         System.out.println(NOTA2);
      }
      else if((user1 >=70) && (user1 <=79)){
         System.out.println(NOTA3);
      }
      else if ((user1 >=60) && (user1 <=69)){
         System.out.println(NOTA4);
      }
      else if (user1 <=59) {
         System.out.println(NOTA5);
      }
      else {
         System.out.println("T'has passat");
      }
   }  
}