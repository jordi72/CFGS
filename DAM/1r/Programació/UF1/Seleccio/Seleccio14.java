import java.util.Random;
public class Seleccio14{
   public static void main(String[] args){
      Random r = new Random();
      
      int aleatori = r.nextInt(101);
      int aleatori2 = r.nextInt(101);
      int aleatori3 = r.nextInt(101);
      int aleatori4 = r.nextInt(101);
      
      int gran1;
      int gran2;
      int supergran;
      int petit1;
      int petit2;
      int superpetit;
      
      System.out.println(aleatori);
      System.out.println(aleatori2);
      System.out.println(aleatori3);
      System.out.println(aleatori4);
      
      /* Bloc saber gran, start */
      
      if (aleatori > aleatori2){
         gran1 = aleatori;
      } else {
         gran1 = aleatori2;
      }
      
      if (aleatori3 > aleatori4){
         gran2 = aleatori3;
      } else {
         gran2 = aleatori4;
      }
      
      if (gran1 > gran2){
         supergran = gran1;
      } else {
         supergran = gran2;
      }
      System.out.println("");
      System.out.println("El mes gran es: " + supergran);
      
      /* Bloc saber gran, end */
      
      /* Bloc per saber el petit, start */
      
      if (aleatori < aleatori2){
         petit1 = aleatori;
      } else {
         petit1 = aleatori2;
      }
      
      if (aleatori3 < aleatori4){
         petit2 = aleatori3;
      } else {
         petit2 = aleatori4;
      }
      
      if (petit1 < petit2){
         superpetit = petit1;
      } else {
         superpetit = petit2;
      }
      System.out.println("");
      System.out.println("El mes petit es: " + superpetit);
      
      /* Bloc saber petit, end */
   }
}