import java.util.Scanner;
public class Seleccio3{
   public static void main(String[] args){
      Scanner nums = new Scanner(System.in);
      
      int num1;
      int num2;
      
      System.out.println("Introdueix un numero");
      num1 = nums.nextInt();
      
      System.out.println("Introdueix un altre numero");
      num2 = nums.nextInt();
      
      if ((num1 % num2 == 0) || (num2 % num1 == 0)){
         System.out.println("Son multiples");
      } else {
         System.out.println("No son multiples");
      }
   }
}