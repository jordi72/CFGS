import java.util.Scanner;
public class Consola10{
   public static void main(String[] args){
      Scanner seg = new Scanner(System.in);
      
      int segons;
      int minuts;
      int hores;
      int seguser;
      
      System.out.println("Introdueix el nombre de segons a convertir: ");
      seguser = seg.nextInt();
      
      hores=seguser/3600;
      minuts=hores*3600;
      minuts=seguser-minuts;
      minuts=minuts/60;
      segons=hores*3600+minuts*60;
      segons=seguser-segons;
      
      System.out.println(seguser + " segons equivalent a " + hores + " hores" + minuts + " minuts" + segons + " segons");
      
   }
}