import java.util.Scanner;
public class Consola2{
   public static void main(String[] args){
      Scanner num = new Scanner(System.in);
      
      double temperatura; 
      
      System.out.print("Introdueix una temperatura Celsius:");
      
      temperatura = num.nextDouble();
      
      final double fahrenheit = (9.0 / 5) * temperatura + 32;
      
      System.out.println("Els graus introduits equivaleixen a " + fahrenheit + " graus Fahrenheit.");  
   }
}