import java.util.Scanner;
public class Consola5{
   public static void main(String[] args){

   Scanner num = new Scanner(System.in);

   final double PI = 3.1416;

   double radi;

   double altura;

   System.out.println("Entra el radi i l'alçada del cilindre: ");
   radi = num.nextDouble();
   altura = num.nextDouble();

   double area = radi * radi * PI;

   double volumen = area * altura;

   System.out.println("L'area és: " + area);

   System.out.println("El volum és " + volumen);
   }
}
