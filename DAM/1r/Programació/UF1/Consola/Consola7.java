import java.util.Scanner;
public class Consola7{
  public static void main(String[] args){
    
    Scanner suma = new Scanner(System.in);
    
    int num1;
    int num2;
    
    System.out.println("Escriu un nombre");
    num1 = suma.nextInt();
    
    System.out.println("Escriu un altre nombre");
    num2 = suma.nextInt();
    
    System.out.printf("%5d%n", num1);
    System.out.printf("%+5d%n", num2);
    System.out.println("-------");
    System.out.printf("%5d%n", num1 + num2);
  }
}
