import java.util.Scanner;
public class Consola3{
   public static void main(String[] args){
   Scanner num = new Scanner(System.in);
   
   String nom;
   int edat;
   
   System.out.print("Hola. Com et dius?: ");
   nom = num.nextLine();
   
   System.out.print("Hola " + nom + ". Quants anys tens?");
   edat = num.nextInt();
   
   System.out.println("Així que et dius " + nom + " i tens " + edat + " anys.");
   }
}