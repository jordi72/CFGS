import java.util.Scanner;
public class Consola9{
   public static void main(String[] args){
      
      Scanner valors = new Scanner(System.in);
      
      String producte;
      
      int unitats;
      
      double preu;
      
      String codi;
      
      double ptas;
      
      
      System.out.println("Per favor introdueix la següent informació:");
      
      System.out.println("Nom de producte");      
      producte = valors.nextLine();
      
      System.out.println("Unitats en estoc:");
      unitats = valors.nextInt();
      
      System.out.println("Preu:");
      preu = valors.nextDouble();
      valors.nextLine();
      
      System.out.println("Codi de referencia:");
      codi = valors.nextLine();
      
      ptas = preu*166;
      
      System.out.println("+----------------------+");
      System.out.printf("|%s     | %10s%n", producte,codi + "|"); 
      System.out.printf("|Estoc:     | %10s%n", unitats + " U|");
      System.out.printf("|       %s€| %10s%n", preu,Math.round(ptas) + "Pessetes|");    
   }
}