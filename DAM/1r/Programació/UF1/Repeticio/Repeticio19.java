import java.util.Scanner;
public class Repeticio19{
   public static void main(String[] args){
      Scanner s = new Scanner(System.in);
      
      int columnes = 0;
      int filas = 0;
      int espais = 0;
      
      System.out.println("Numero de filas");
      filas = s.nextInt();
      
      System.out.println("Numero de columnes");
      columnes = s.nextInt();
      
      espais=columnes-1;
      
      for(int i=0; i<columnes; i++){
         for(int j=0; j<filas; j++){
         System.out.print("*");
         
         }
         if(i<espais){
            System.out.println();
         }
      }
   }
}