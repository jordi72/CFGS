import java.util.Scanner;
public class Repeticio6{
   public static void main(String[] args){
      Scanner s = new Scanner(System.in);
      
      int user;
      
      System.out.println("Donam un numero:");
      user = s.nextInt();
      
      while(user!=0){
         if(user % 2 == 0){
            System.out.println("Parell");
         } else {
            System.out.println("Senar");
         }
        System.out.println("Donam un numero:");
        user = s.nextInt(); 
      }
   }
}