import java.util.Scanner;
public class Repeticio5{
   public static void main(String[] args){
      Scanner s = new Scanner(System.in);
      
      int x;
      
      System.out.println("Donam un numero:");
      x = s.nextInt();
      
      while(x!=0){
         if(x<0){
            System.out.println("NEGATIU");
         } else {
            System.out.println("POSITIU");
         }
         System.out.println("Donam un numero:");
         x = s.nextInt();
      }
   }
}