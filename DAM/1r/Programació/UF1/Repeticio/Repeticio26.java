import java.util.Scanner;
public class Repeticio26{
   public static void main(String[] args){
      Scanner s = new Scanner(System.in);
      
      int num = 0;
      int quadrat = 0;
      
     // System.out.println("Introdueix un numero: ");
     
      while(num>=0){
      
       System.out.println("Introdueix un numero: ");

         if(s.hasNextInt()==true){
         num=s.nextInt();
         quadrat=num*num; 
         }else{
          s.nextLine();
         }
         
         if(num>0){
           System.out.println(num + "\u00B2 = " + quadrat); 
         }
        
      }
     
   }
}