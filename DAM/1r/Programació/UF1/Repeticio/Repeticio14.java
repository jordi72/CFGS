import java.util.Scanner;
public class Repeticio14{
   public static void main(String[] args){
      Scanner s = new Scanner(System.in);
      
      final double MILLKM = 1.609;
      double km = 1.609;
      int num = 10;
      
       System.out.println("Milles            Kilometres");
       System.out.println("............................");
         
      for(int i = 1 ; i<=num; i++){
         System.out.println(i + "            " + km);
         km=km+MILLKM; 
      }
   }
}