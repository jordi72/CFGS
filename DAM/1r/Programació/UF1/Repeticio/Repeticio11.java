import java.util.Scanner;
public class Repeticio11{
   public static void main(String[] args){
      Scanner s = new Scanner(System.in);
      
      int user=0;
      int resultat=1;
      
      System.out.println("Donam un numero: ");
      user = s.nextInt();
      
      while(user!=0){
         resultat=resultat*user;
         user--;
      }
      System.out.println(resultat);
   }
}
