import java.util.Scanner;
public class Repeticio4{
   public static void main(String[] args){
      Scanner s = new Scanner(System.in);
      
      int x;
      int y = 1;
      
      System.out.println("Donam un numero");
      x = s.nextInt();
      
      while(x>=0){
         System.out.println(x + "\u00B2" + " = " + x*x);
         System.out.println("Donam un numero:");
         x = s.nextInt();
      }
   }
}