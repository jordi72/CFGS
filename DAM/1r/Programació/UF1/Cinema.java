//Developer: Jordi Sarrato.
import java.util.Scanner;
public class Cinema{
   public static void main(String[] args){
      Scanner s = new Scanner(System.in);
   
      int entrades = 10;
      int user;
   
      while(entrades>0){
      
         System.out.println("Entrades restants: " + entrades);
         
         System.out.print("Quantes entrades vol comprar? [1-4]: ");
         
         if(s.hasNextInt()==true){
         user=s.nextInt();
         
            if(user>=1 && user<=4){
               if(user>entrades){
                   System.out.println("No queden tantes entrades disponibles. Nomes queden " + entrades + " entrades. \n");
               }else{
                  entrades=entrades-user;
                  System.out.println("Entrades reservades. Gracies per la seva confiança. \n");
                  s.nextLine();
               }
            }else{
               System.out.println("Entrada incorrecte. Introdueix un valor entre 1 i 4. \n" );
               s.nextLine();
            }
          
         }else{
            s.nextLine();
            System.out.println("Entrada incorrecte. Introdueix un valor entre 1 i 4. \n");  
         }
      } 
      System.out.println("************************");
      System.out.println("* Entrades exhaurides. *");
      System.out.println("************************");
      }
   }