public class Temps{
  public static void main(String[] args){
    final int fullDay = 86400;
    int hora = 9;
    int minut = 25;
    int segon = 30;
    hora = hora*3600;
    minut = minut*60;
    double segSinceMid = hora + minut + segon;
    double segEndDay = fullDay - segSinceMid;
    double Tanpercent = segSinceMid / fullDay * 100;
    System.out.println("Segons des de mitja nit: " + (int) segSinceMid);
    System.out.println("Segons fins a final del dia " + (int) segEndDay);
    System.out.println("Ha pasat el " + Tanpercent + "% del dia");
  }
}
