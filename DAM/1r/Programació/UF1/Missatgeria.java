//Developer: Jordi Sarrato Mercadé
import java.util.Scanner;
public class Missatgeria{
   public static void main(String[] args){
      Scanner s = new Scanner(System.in);
      
      String carta;
      String paquet;
      String paquetLock;
      String enviament;
      String fragilNormal;
      int fragilPreu = 20;
      double pes;
      int totalPagar = 0;
      int cartaN = 2;
      int cartaU = 5;
      int totalCarta = 0;
      
      //Carta?
      
      System.out.println("[C]arta, [P]aquet?");
      carta = s.nextLine();
      
      if ((carta.equals("C")) || (carta.equals("c"))){
       
       //Normal o Urgent?
         
         System.out.println("Enviament [N]ormal o [U]rgent?");
         enviament = s.nextLine();
         
         if ((enviament.equals("N")) || (enviament.equals("n")) || (enviament.equals("U")) || (enviament.equals("u"))){
         
            if ((enviament.equals("N")) || (enviament.equals("n"))){
               totalCarta = cartaN;          
            } else {
               totalCarta = cartaU;
            }
         
             System.out.println("------------------------------");
             System.out.println("Tipus d'enviament: " + carta);
             System.out.println("Enviament: " + enviament);
             System.out.println("Total a pagar: " + totalCarta);
             System.out.println("------------------------------");
          
         } else {
            System.out.println("Entrada incorrecte!!");  
         }
      } else {
             paquet = "p";
             paquetLock = "P";
      
            if ((paquet.equals("P")) || (paquet.equals("p"))){
             
               //pes
               
               System.out.println("Introdueix el pes del paquet en Kg:");
               pes = s.nextDouble();
               s.nextLine();
               
               if (pes<2.0){
                  totalPagar = 8;
               } else if ((pes>2.0) && (pes<5.0)) {
                  totalPagar = 20;
               }
               //Fragil o Normal?
               
               System.out.println("Embalatge [F]ragil o [N]ormal?");
               fragilNormal = s.nextLine();     
              
               if ((fragilNormal.equals("F")) || (fragilNormal.equals("f"))){
               
                   System.out.println("------------------------------");
                   System.out.println("Tipus de paquet: " + paquet);
                   System.out.println("Pes del paquet: " + pes);
                   System.out.println("FRAGIL");
                   System.out.println("Total a pagar: " + totalPagar);
                   System.out.println("------------------------------"); 
                    
               } else if ((fragilNormal.equals("F")) || (fragilNormal.equals("f")) || (fragilNormal.equals("N")) || (fragilNormal.equals("n"))){
                     
                   System.out.println("------------------------------");
                   System.out.println("Tipus de paquet: " + paquet);
                   System.out.println("Pes del paquet: " + pes);
                   System.out.println("Total a pagar: " + totalPagar);
                   System.out.println("------------------------------");          
               } else {
                  System.out.println("Entrada incorrecte!!"); 
               } 
             } else {
               System.out.println("xd");
               }
            }
   }
}