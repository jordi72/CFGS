public class Matriu6{
   public static void main(String[] args){
      
      int matriu1[] = {1,3,5,7,9,11,13,15,17,19,21,23};
      int matriu2[] = {2,4,6,8,10,12,14,16,18,20,22,24};
      int matriu3[] = new int[24];
      int contador = 0;
      int aux = 0;
      int aux2= 0;
           
      for(int i=0; i<matriu3.length; i++){
         if(contador>=6){
            contador=0;
         }
         if(contador<3){
            matriu3[i]=matriu1[aux2];
            aux2++;
         }
         else if(contador>2){
            matriu3[i]=matriu2[aux];
            aux++;
         }
         contador++;         
      }
      for(int i=0; i<matriu3.length; i++){  
         System.out.print(matriu3[i] + " ");
      }    
   }
}