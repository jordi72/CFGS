import java.util.Scanner;
import java.util.Random;
public class Matriu3{
   public static void main(String[] args){
      Scanner s = new Scanner(System.in);
      Random r = new Random();
      
         int[] matriu;
         int valorMatriu = 0;
         int numero1 = 0;
         int numeroEnd = 0;
         
         System.out.println("Especifica la llongitud de la matriu: [2-100]");
         valorMatriu = s.nextInt();
         
         matriu = new int [valorMatriu];
         
         System.out.print("Matriu generada: [");
         
         for(int i=0; i<valorMatriu; i++){
            matriu[i] = r.nextInt(100); 
            System.out.print(matriu[i] + " "); 
            numero1 = matriu[0];
            
         }
         System.out.println("]");
         System.out.print("Indexs senars: [");
         for(int j=0; j<valorMatriu; j++){
            if(j%2==1){
               System.out.print(matriu[j] + ",");
            }
         }
         System.out.println("]");
         System.out.print("Elements senars: [");
         for(int k=0; k<valorMatriu; k++){
            if(matriu[k]%2!=0){
               System.out.print(matriu[k] + ",");
            }
         }
         System.out.println("]");
         System.out.print("Elements parells: [");
         for(int l=0; l<valorMatriu; l++){
            if(matriu[l]%2==0){
               System.out.print(matriu[l] + ",");
            }
         }
         System.out.println("]");
         
         System.out.print("Elements al reves: ");
         for(int m=valorMatriu-1; m>=0; m--){
            System.out.print(matriu[m] + ", ");
         }
         System.out.println();
         numeroEnd=matriu[valorMatriu-1];

         System.out.print("Primer i ultim element: " + numero1 + " " + numeroEnd);
         
   }
}