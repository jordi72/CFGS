public class Matriu5{
   public static void main(String[] args){
      
      int matriu1[] = {1,3,5,7,9,11,13,15,17,19};
      int matriu2[] = {2,4,6,8,10,12,14,16,18,20};
      int matriu3[] = new int[20];
      int contador1 = 0;
      int contador2 = 0;
      
      for(int i=0; i<matriu3.length; i++){
         matriu3[i]=matriu1[contador1];
         System.out.print(matriu3[i] + ", ");
         i++;
         matriu3[i]=matriu2[contador2];
         contador1++;
         contador2++;
         System.out.print(matriu3[i] + ", ");
      }
      
   }
}