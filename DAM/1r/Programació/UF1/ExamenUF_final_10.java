import java.util.Scanner;
import java.util.Random;

public class ExamenUF_final_10 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random generadorAleatori = new Random();

        int opcioSeleccionada = 0;
        boolean sortir = false;
        boolean opcioSeleccionadaOk = false;

        do {
            // Comprovar entrada men�
            do {
                System.out.println("");
                System.out.println("1.- Calculadora");
                System.out.println("2.- Dibuixa una T");
                System.out.println("3.- Estadistiques");
                System.out.println("4.- Diagonal");
                System.out.println("5.- Sortir");
                System.out.print(">> Tria opci�: ");
                if (!in.hasNextInt()) {
                    System.out.println("Entrada incorrecte!!)\n\n");
                    in.next();
                } else {
                    opcioSeleccionada = in.nextInt();
                    in.nextLine();
                    if (opcioSeleccionada >= 1 && opcioSeleccionada <= 5) {
                        opcioSeleccionadaOk = true;
                    }
                }
            } while (!opcioSeleccionadaOk);

            // Seleccionar opci� men�
            switch (opcioSeleccionada) {
                case 1:
                    int valor1;
                    int valor2;

                    System.out.println("\n\n-- Opci� 1 - Calculadora --");
                    System.out.print("Valor 1: ");
                    valor1 = in.nextInt();
                    System.out.print("Valor 2: ");
                    valor2 = in.nextInt();
                    if (valor1 != 0 && valor2 != 0) {
                        if (valor1 * valor2 > 0) {
                            System.out.printf("La suma de %d i %d �s %d.%n", valor1, valor2, valor1 + valor2);
                        } else {
                            System.out.printf("La divisi� de %d entre %d �s %f.%n", valor1, valor2, ((double) valor1) / valor2);
                        }
                    } else {
                        System.out.println("Un dels valors �s zero!!");
                    }
                    break;
                case 2:
                    int midaT = 0;
                    System.out.println("\n\n-- Opci� 2 - Dibuixa una T --");

                    do {
                        System.out.print("Quina �s la mida de la T? [ha de ser un n�mero senar entre 3 i 11] ");
                        midaT = in.nextInt();
                    } while (midaT % 2 == 0 || midaT < 3 || midaT > 11);
                    System.out.print("\n");

                    for (int i = 0; i < midaT; i++) {
                        System.out.print("* ");
                    }
                    System.out.print("\n");
                    for (int i = 0; i < midaT - 1; i++) {
                        for (int j = 0; j < midaT - 1; j++) {
                            System.out.print(" ");
                        }
                        System.out.println("*");
                    }
                    break;
                case 3:
                    System.out.println("\n\n-- Opci� 3 - Estad�stiques --");
                    int nombreTirades = 0;
                    int resultatSumes[] = new int[11]; // 1 + 1 -> 6 + 6

                    do {
                        System.out.print("Quantes tirades de daus es realitzaran? ");
                        nombreTirades = in.nextInt();
                    } while (nombreTirades <= 0);

                    for (int i = 0; i < nombreTirades; i++) {
                        int dau1 = generadorAleatori.nextInt(6) + 1;
                        int dau2 = generadorAleatori.nextInt(6) + 1;
                        resultatSumes[dau1 + dau2 - 2]++;
                        // System.out.printf("[%d, %d] ", dau1, dau2);
                    }

                    System.out.println("Percentatges de tirades: ");
                    for (int i = 0; i < resultatSumes.length; i++) {
                        System.out.printf("Suma %d -> %d tirades que corresponen al %.2f%% %n",
                                i + 2, resultatSumes[i], 100 * ((float) resultatSumes[i]) / nombreTirades);

                    }
                    break;
                case 4:
                    System.out.println("\n\n-- Opci� 4 - Diagonal --");

                    int mida = 0;
                    do {
                        System.out.print("Introdueix la mida de la matriu: [2-10] ");
                        mida = in.nextInt();
                    } while (mida < 2 || mida > 10);

                    int[][] matriu = new int[mida][mida];
                    int sumaDiagonalPrincipal = 0;
                    int sumaDiagonalInvertida = 0;
                    System.out.print("\n");
                    for (int i = 0; i < mida; i++) {
                        for (int j = 0; j < mida; j++) {
                            matriu[i][j] = generadorAleatori.nextInt(8) + 1;
                            System.out.print(matriu[i][j] + " ");
                            if (i == j) {
                                sumaDiagonalPrincipal += matriu[i][j];
                            }
                            if (i + j == mida - 1) {
                                sumaDiagonalInvertida += matriu[i][j];
                            }
                        }
                        System.out.print("\n");
                    }
                    System.out.print("\n");
                    System.out.println("La suma dels valors de la diagonal principal �s: " + sumaDiagonalPrincipal);
                    System.out.println("La suma dels valors de la diagonal inversa �s: " + sumaDiagonalInvertida + "\n");
                    break;

                case 5:
                    System.out.println("Ad�u");
                    sortir = true;
                    break;
            }
        } while (!sortir);
    }
}