1.-

function objprop(objecte) {
  var resultat='';
  for (propietat in objecte) {
    resultat+= propietat+' valor '+objecte[propietat]+'\n';
  }
  return resultat;
}

objecte={
  Propietat1: 123,
  Propietat2: 'abcd',
  Propietat3: true,
  Propietat4: [1,2,3],
  Propietat5: null
}

objprop(objecte);

/*
Propietat1 valor 123
Propietat2 valor abcd
Propietat3 valor true
Propietat4 valor 1,2,3
Propietat5 valor null

*/


2.-

function subobj(obj,tab=0) {
  var resultat='';
  for (propietat in obj) {
    for (var space=0; space<tab; space++) {
      resultat+=' ';
    }
    resultat+= propietat+' valor '+obj[propietat]+'\n';
    if (typeof obj[propietat] == 'object') {
      resultat+=subobj(obj[propietat],tab+1);
    }
  }
  return resultat;
}

objecte={
  Propietat1: 123,
  Propietat2: 'abc',
  Propietat3: true,
  Propietat4: [1,2,3],
  Propietat5: null,
  Propietat6: {
    Propietat1: 2567,
    Propietat2: 'bdfdfg',
    Propietat3: {
      Propietat1: 365645,
      Propietat2: {
        Propietat1: 46756845
      },
      Propietat3: 'cfgfgukd'
    }
  }
}

subobj(objecte);
/*
Propietat1 valor 123
Propietat2 valor abc
Propietat3 valor true
Propietat4 valor 1,2,3
 0 valor 1
 1 valor 2
 2 valor 3
Propietat5 valor null
Propietat6 valor [object Object]
 Propietat1 valor 2567
 Propietat2 valor bdfdfg
 Propietat3 valor [object Object]
  Propietat1 valor 365645
  Propietat2 valor [object Object]
   Propietat1 valor 46756845
  Propietat3 valor cfgfgukd

*/