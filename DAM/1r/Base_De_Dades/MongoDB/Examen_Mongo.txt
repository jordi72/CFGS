1.-
> db.bios.find({"contribs":"OOP"}, {"name.last":1, "name.first":1}).sort({"name.last":1, "name.first":1})
{ "_id" : 5, "name" : { "first" : "Ole-Johan", "last" : "Dahl" } }
{ "_id" : 4, "name" : { "first" : "Kristen", "last" : "Nygaard" } }

2.-

> db.bios.find({$and:[{"contribs":{$exists:true}},{"name.first":"Dennis", "name.last":"Ritchie"}]}, {"contribs":1})
{ "_id" : ObjectId("51e062189c6ae665454e301d"), "contribs" : [ "UNIX", "C" ] }

3.-
> db.bios.find({"contribs.2":{$exists:true}}, {"contribs":1, "name":1}).sort({"contribs":-1})
{ "_id" : 3, "name" : { "first" : "Grace", "last" : "Hopper" }, "contribs" : [ "UNIVAC", "compiler", "FLOW-MATIC", "COBOL" ] }
{ "_id" : ObjectId("51df07b094c6acd67e492f41"), "name" : { "first" : "John", "last" : "McCarthy" }, "contribs" : [ "Lisp", "Artificial Intelligence", "ALGOL" ] }
{ "_id" : 1, "name" : { "first" : "John", "last" : "Backus" }, "contribs" : [ "Fortran", "ALGOL", "Backus-Naur Form", "FP" ] }

4.-
> db.bios.find({"awards.0":{$exists:false}}).count()
1

5.-
> db.bios.find({"awards.year":{$gt:"2010"}}, {"name":1, "awards":1})
{ "_id" : 8, "name" : { "first" : "Yukihiro", "aka" : "Matz", "last" : "Matsumoto" }, "awards" : [ { "award" : "Award for the Advancement of Free Software", "year" : "2011", "by" : "Free Software Foundation" } ] }
