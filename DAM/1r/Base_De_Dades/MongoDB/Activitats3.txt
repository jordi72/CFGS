﻿1.- Cerca en quants països s’utilitza l’euro.

> db.countries.find({currency:"EUR"}).count()
35
>

2.- Cerca en quines subregions està dividida Europa.

> db.countries.distinct("subregion",{region:"Europe"},{subregion:1});
[
	"Northern Europe",
	"Southern Europe",
	"Western Europe",
	"Eastern Europe",
	"Central Europe"
]
>

3.- Cerca quin país té el domini d’Internet (tld) .tk.

> db.countries.find({tld:".tk"})
{ "_id" : ObjectId("59282a16015fc6b0865e4951"), "name" : { "common" : "Tokelau", "official" : "Tokelau", "native" : { "eng" : { "official" : "Tokelau", "common" : "Tokelau" }, "smo" : { "official" : "Tokelau", "common" : "Tokelau" }, "tkl" : { "official" : "Tokelau", "common" : "Tokelau" } } }, "tld" : [ ".tk" ], "cca2" : "TK", "ccn3" : "772", "cca3" : "TKL", "cioc" : "", "currency" : [ "NZD" ], "callingCode" : [ "690" ], "capital" : "Fakaofo", "altSpellings" : [ "TK" ], "region" : "Oceania", "subregion" : "Polynesia", "languages" : { "eng" : "English", "smo" : "Samoan", "tkl" : "Tokelauan" }, "translations" : { "deu" : { "official" : "Tokelau", "common" : "Tokelau" }, "fra" : { "official" : "Îles Tokelau", "common" : "Tokelau" }, "hrv" : { "official" : "Tokelau", "common" : "Tokelau" }, "ita" : { "official" : "Tokelau", "common" : "Isole Tokelau" }, "jpn" : { "official" : "トケラウ諸島", "common" : "トケラウ" }, "nld" : { "official" : "Tokelau", "common" : "Tokelau" }, "por" : { "official" : "Tokelau", "common" : "Tokelau" }, "rus" : { "official" : "Токелау", "common" : "Токелау" }, "slk" : { "official" : "Tokelauské ostrovy", "common" : "Tokelau" }, "spa" : { "official" : "Tokelau", "common" : "Islas Tokelau" }, "fin" : { "official" : "Tokelau", "common" : "Tokelau" }, "zho" : { "official" : "托克劳", "common" : "托克劳" } }, "latlng" : [ -9, -172 ], "demonym" : "Tokelauan", "landlocked" : false, "borders" : [ ], "area" : 12 }
>

4.- Cerca quins països tenen més d’un domini d’Internet.

db.countries.find({"tld.2":{$exists:true}}, {name:1,tld:1,_id:0})
{ "name" : { "common" : "China", "official" : "People's Republic of China", "native" : { "zho" : { "official" : "中华人民共和国", "common" : "中国" } } }, "tld" : [ ".cn", ".中国", ".中國", ".公司", ".网络" ] }
{ "name" : { "common" : "Sri Lanka", "official" : "Democratic Socialist Republic of Sri Lanka", "native" : { "sin" : { "official" : "ශ්‍රී ලංකා ප්‍රජාතාන්ත්‍රික සමාජවාදී ජනරජය", "common" : "ශ්‍රී ලංකාව" }, "tam" : { "official" : "இலங்கை சனநாயக சோசலிசக் குடியரசு", "common" : "இலங்கை" } } }, "tld" : [ ".lk", ".இலங்கை", ".ලංකා" ] }
{ "name" : { "common" : "Russia", "official" : "Russian Federation", "native" : { "rus" : { "official" : "Российская Федерация", "common" : "Россия" } } }, "tld" : [ ".ru", ".su", ".рф" ] }
{ "name" : { "common" : "Singapore", "official" : "Republic of Singapore", "native" : { "zho" : { "official" : "新加坡共和国", "common" : "新加坡" }, "eng" : { "official" : "Republic of Singapore", "common" : "Singapore" }, "msa" : { "official" : "Republik Singapura", "common" : "Singapura" }, "tam" : { "official" : "சிங்கப்பூர் குடியரசு", "common" : "சிங்கப்பூர்" } } }, "tld" : [ ".sg", ".新加坡", ".சிங்கப்பூர்" ] }
{ "name" : { "common" : "Taiwan", "official" : "Republic of China (Taiwan)", "native" : { "zho" : { "official" : "中华民国", "common" : "臺灣" } } }, "tld" : [ ".tw", ".台湾", ".台灣" ] }
>

5.- Cerca quins països tenen una jota al seu nom (majúscula o minúscula).

db.countries.find({$or:[{"name.common":/J/},{"name.common":/j/}]})

{ "_id" : ObjectId("59282a16015fc6b0865e493a"), "name" : { "common" : "Svalbard and Jan Mayen", "official" : "Svalbard og Jan Mayen", "native" : { "nor" : { "official" : "Svalbard og Jan Mayen", "common" : "Svalbard og Jan Mayen" } } }, "tld" : [ ".sj" ], "cca2" : "SJ", "ccn3" : "744", "cca3" : "SJM", "cioc" : "", "currency" : [ "NOK" ], "callingCode" : [ "4779" ], "capital" : "Longyearbyen", "altSpellings" : [ "SJ", "Svalbard and Jan Mayen Islands" ], "region" : "Europe", "subregion" : "Northern Europe", "languages" : { "nor" : "Norwegian" }, "translations" : { "deu" : { "official" : "Inselgruppe Spitzbergen", "common" : "Spitzbergen" }, "fra" : { "official" : "Jan Mayen Svalbard", "common" : "Svalbard et Jan Mayen" }, "hrv" : { "official" : "Svalbard og Jan Mayen", "common" : "Svalbard i Jan Mayen" }, "ita" : { "official" : "Svalbard og Jan Mayen", "common" : "Svalbard e Jan Mayen" }, "jpn" : { "official" : "スバールバル諸島OGヤンマイエン", "common" : "スヴァールバル諸島およびヤンマイエン島" }, "nld" : { "official" : "Svalbard og Jan Mayen", "common" : "Svalbard en Jan Mayen" }, "por" : { "official" : "Svalbard og Jan Mayen", "common" : "Ilhas Svalbard e Jan Mayen" }, "rus" : { "official" : "Свальбарда ог Ян-Майен", "common" : "Шпицберген и Ян-Майен" }, "slk" : { "official" : "Svalbard a Jan Mayen", "common" : "Svalbard a Jan Mayen" }, "spa" : { "official" : "Svalbard og Jan Mayen", "common" : "Islas Svalbard y Jan Mayen" }, "fin" : { "official" : "Huippuvuoret", "common" : "Huippuvuoret" }, "zho" : { "official" : "斯瓦尔巴特", "common" : "斯瓦尔巴特" } }, "latlng" : [ 78, 20 ], "demonym" : "Norwegian", "landlocked" : false, "borders" : [ ], "area" : -1 }
{ "_id" : ObjectId("59282a16015fc6b0865e4950"), "name" : { "common" : "Tajikistan", "official" : "Republic of Tajikistan", "native" : { "rus" : { "official" : "Республика Таджикистан", "common" : "Таджикистан" }, "tgk" : { "official" : "Ҷумҳурии Тоҷикистон", "common" : "Тоҷикистон" } } }, "tld" : [ ".tj" ], "cca2" : "TJ", "ccn3" : "762", "cca3" : "TJK", "cioc" : "TJK", "currency" : [ "TJS" ], "callingCode" : [ "992" ], "capital" : "Dushanbe", "altSpellings" : [ "TJ", "Toçikiston", "Republic of Tajikistan", "Ҷумҳурии Тоҷикистон", "Çumhuriyi Toçikiston" ], "region" : "Asia", "subregion" : "Central Asia", "languages" : { "rus" : "Russian", "tgk" : "Tajik" }, "translations" : { "deu" : { "official" : "Republik Tadschikistan", "common" : "Tadschikistan" }, "fra" : { "official" : "République du Tadjikistan", "common" : "Tadjikistan" }, "hrv" : { "official" : "Republika Tadžikistan", "common" : "Tađikistan" }, "ita" : { "official" : "Repubblica del Tajikistan", "common" : "Tagikistan" }, "jpn" : { "official" : "タジキスタン共和国", "common" : "タジキスタン" }, "nld" : { "official" : "Tadzjikistan", "common" : "Tadzjikistan" }, "por" : { "official" : "República do Tajiquistão", "common" : "Tajiquistão" }, "rus" : { "official" : "Республика Таджикистан", "common" : "Таджикистан" }, "slk" : { "official" : "Taǆická republika", "common" : "Taǆikistan" }, "spa" : { "official" : "República de Tayikistán", "common" : "Tayikistán" }, "fin" : { "official" : "Tadžikistanin tasavalta", "common" : "Tadžikistan" }, "zho" : { "official" : "塔吉克斯坦共和国", "common" : "塔吉克斯坦" } }, "latlng" : [ 39, 71 ], "demonym" : "Tadzhik", "landlocked" : true, "borders" : [ "AFG", "CHN", "KGZ", "UZB" ], "area" : 143100 }


6.- Cerca el nom nadiu del país que en espanyol es diu "Japón".

> db.countries.find({"translations.spa.common":"Japón"},{"name.native":1})
{ "_id" : ObjectId("59282a16015fc6b0865e48e8"), "name" : { "native" : { "jpn" : { "official" : "日本", "common" : "日本" } } } }
>

7.- Cerca el nom dels països que tenen més de 5 països veïns.

> db.countries.find({"borders.4":{$exists:true}},{"name.common":1})
{ "_id" : ObjectId("59282a15015fc6b0865e4877"), "name" : { "common" : "Afghanistan" } }
{ "_id" : ObjectId("59282a15015fc6b0865e487e"), "name" : { "common" : "Argentina" } }
{ "_id" : ObjectId("59282a15015fc6b0865e4885"), "name" : { "common" : "Austria" } }
{ "_id" : ObjectId("59282a15015fc6b0865e4886"), "name" : { "common" : "Azerbaijan" } }
{ "_id" : ObjectId("59282a15015fc6b0865e488a"), "name" : { "common" : "Burkina Faso" } }
{ "_id" : ObjectId("59282a15015fc6b0865e488c"), "name" : { "common" : "Bulgaria" } }
{ "_id" : ObjectId("59282a15015fc6b0865e4891"), "name" : { "common" : "Belarus" } }
{ "_id" : ObjectId("59282a15015fc6b0865e4894"), "name" : { "common" : "Bolivia" } }
{ "_id" : ObjectId("59282a15015fc6b0865e4895"), "name" : { "common" : "Brazil" } }
{ "_id" : ObjectId("59282a15015fc6b0865e489b"), "name" : { "common" : "Central African Republic" } }
{ "_id" : ObjectId("59282a15015fc6b0865e489e"), "name" : { "common" : "Switzerland" } }
{ "_id" : ObjectId("59282a15015fc6b0865e48a0"), "name" : { "common" : "China" } }
{ "_id" : ObjectId("59282a15015fc6b0865e48a1"), "name" : { "common" : "Ivory Coast" } }
{ "_id" : ObjectId("59282a15015fc6b0865e48a2"), "name" : { "common" : "Cameroon" } }
{ "_id" : ObjectId("59282a15015fc6b0865e48a3"), "name" : { "common" : "DR Congo" } }
{ "_id" : ObjectId("59282a15015fc6b0865e48a4"), "name" : { "common" : "Republic of the Congo" } }
{ "_id" : ObjectId("59282a15015fc6b0865e48a6"), "name" : { "common" : "Colombia" } }
{ "_id" : ObjectId("59282a15015fc6b0865e48b0"), "name" : { "common" : "Germany" } }
{ "_id" : ObjectId("59282a15015fc6b0865e48b5"), "name" : { "common" : "Algeria" } }
{ "_id" : ObjectId("59282a16015fc6b0865e48ba"), "name" : { "common" : "Spain" } }
Type "it" for more
>

8.- Cerca en quants països es parla francès.

> db.countries.find({"languages.fra":{$exists:true}}).count()
46
>


9.- Cerca el nom comú i el nom oficial dels països africans en què es parla
francès.
> db.countries.find({$and:[{"languages.fra":{$exists:true}},{region:"Africa"}]},{"name.common":1,"name.official":1})

{ "_id" : ObjectId("59282a15015fc6b0865e4887"), "name" : { "common" : "Burundi", "official" : "Republic of Burundi" } }
{ "_id" : ObjectId("59282a15015fc6b0865e4889"), "name" : { "common" : "Benin", "official" : "Republic of Benin" } }
{ "_id" : ObjectId("59282a15015fc6b0865e488a"), "name" : { "common" : "Burkina Faso", "official" : "Burkina Faso" } }
{ "_id" : ObjectId("59282a15015fc6b0865e489b"), "name" : { "common" : "Central African Republic", "official" : "Central African Republic" } }
{ "_id" : ObjectId("59282a15015fc6b0865e48a1"), "name" : { "common" : "Ivory Coast", "official" : "Republic of Côte d'Ivoire" } }
{ "_id" : ObjectId("59282a15015fc6b0865e48a2"), "name" : { "common" : "Cameroon", "official" : "Republic of Cameroon" } }
{ "_id" : ObjectId("59282a15015fc6b0865e48a3"), "name" : { "common" : "DR Congo", "official" : "Democratic Republic of the Congo" } }
{ "_id" : ObjectId("59282a15015fc6b0865e48a4"), "name" : { "common" : "Republic of the Congo", "official" : "Republic of the Congo" } }
{ "_id" : ObjectId("59282a15015fc6b0865e48a7"), "name" : { "common" : "Comoros", "official" : "Union of the Comoros" } }
{ "_id" : ObjectId("59282a15015fc6b0865e48b1"), "name" : { "common" : "Djibouti", "official" : "Republic of Djibouti" } }
{ "_id" : ObjectId("59282a16015fc6b0865e48c3"), "name" : { "common" : "Gabon", "official" : "Gabonese Republic" } }
{ "_id" : ObjectId("59282a16015fc6b0865e48c9"), "name" : { "common" : "Guinea", "official" : "Republic of Guinea" } }
{ "_id" : ObjectId("59282a16015fc6b0865e48cd"), "name" : { "common" : "Equatorial Guinea", "official" : "Republic of Equatorial Guinea" } }
{ "_id" : ObjectId("59282a16015fc6b0865e4902"), "name" : { "common" : "Madagascar", "official" : "Republic of Madagascar" } }
{ "_id" : ObjectId("59282a16015fc6b0865e4907"), "name" : { "common" : "Mali", "official" : "Republic of Mali" } }
{ "_id" : ObjectId("59282a16015fc6b0865e4911"), "name" : { "common" : "Mauritius", "official" : "Republic of Mauritius" } }
{ "_id" : ObjectId("59282a16015fc6b0865e4914"), "name" : { "common" : "Mayotte", "official" : "Department of Mayotte" } }
{ "_id" : ObjectId("59282a16015fc6b0865e4917"), "name" : { "common" : "Niger", "official" : "Republic of Niger" } }
{ "_id" : ObjectId("59282a16015fc6b0865e4931"), "name" : { "common" : "Réunion", "official" : "Réunion Island" } }
{ "_id" : ObjectId("59282a16015fc6b0865e4934"), "name" : { "common" : "Rwanda", "official" : "Republic of Rwanda" } }
Type "it" for more
>

10.- Cerca el nom comú i el continent (region) dels països no africans en què es parla àrab.

db.countries.find({$and:[{"languages.ara":{$exists:true}},{region:{$ne:"Africa"}}]},{"name.common":1,"region":1})

{ "_id" : ObjectId("59282a15015fc6b0865e487d"), "name" : { "common" : "United Arab Emirates" }, "region" : "Asia" }
{ "_id" : ObjectId("59282a15015fc6b0865e488d"), "name" : { "common" : "Bahrain" }, "region" : "Asia" }
{ "_id" : ObjectId("59282a16015fc6b0865e48e1"), "name" : { "common" : "Iraq" }, "region" : "Asia" }
{ "_id" : ObjectId("59282a16015fc6b0865e48e3"), "name" : { "common" : "Israel" }, "region" : "Asia" }
{ "_id" : ObjectId("59282a16015fc6b0865e48e7"), "name" : { "common" : "Jordan" }, "region" : "Asia" }
{ "_id" : ObjectId("59282a16015fc6b0865e48f1"), "name" : { "common" : "Kuwait" }, "region" : "Asia" }
{ "_id" : ObjectId("59282a16015fc6b0865e48f3"), "name" : { "common" : "Lebanon" }, "region" : "Asia" }
{ "_id" : ObjectId("59282a16015fc6b0865e4921"), "name" : { "common" : "Oman" }, "region" : "Asia" }
{ "_id" : ObjectId("59282a16015fc6b0865e492e"), "name" : { "common" : "Palestine" }, "region" : "Asia" }
{ "_id" : ObjectId("59282a16015fc6b0865e4930"), "name" : { "common" : "Qatar" }, "region" : "Asia" }
{ "_id" : ObjectId("59282a16015fc6b0865e4935"), "name" : { "common" : "Saudi Arabia" }, "region" : "Asia" }
{ "_id" : ObjectId("59282a16015fc6b0865e494b"), "name" : { "common" : "Syria" }, "region" : "Asia" }
{ "_id" : ObjectId("59282a16015fc6b0865e496a"), "name" : { "common" : "Yemen" }, "region" : "Asia" }
>

11.- Cerca com es diu Espanya en rus.

> db.countries.find({"name.common":"Spain"},{"translations.rus.common":1, "translations.rus.official":1})
{ "_id" : ObjectId("59282a16015fc6b0865e48ba"), "translations" : { "rus" : { "official" : "Королевство Испания", "common" : "Испания" } } }
>

12.- Cerca en quants països existeix més d’una divisa oficial.


> db.countries.find({"currency.1":{$exists:true}},{"name.common":1, "currency":1})

{ "_id" : ObjectId("59282a15015fc6b0865e4894"), "name" : { "common" : "Bolivia" }, "currency" : [ "BOB", "BOV" ] }
{ "_id" : ObjectId("59282a15015fc6b0865e4898"), "name" : { "common" : "Bhutan" }, "currency" : [ "BTN", "INR" ] }
{ "_id" : ObjectId("59282a15015fc6b0865e489e"), "name" : { "common" : "Switzerland" }, "currency" : [ "CHE", "CHF", "CHW" ] }
{ "_id" : ObjectId("59282a15015fc6b0865e489f"), "name" : { "common" : "Chile" }, "currency" : [ "CLF", "CLP" ] }
{ "_id" : ObjectId("59282a15015fc6b0865e48aa"), "name" : { "common" : "Cuba" }, "currency" : [ "CUC", "CUP" ] }
{ "_id" : ObjectId("59282a16015fc6b0865e48b9"), "name" : { "common" : "Western Sahara" }, "currency" : [ "MAD", "DZD", "MRO" ] }
{ "_id" : ObjectId("59282a16015fc6b0865e48d9"), "name" : { "common" : "Haiti" }, "currency" : [ "HTG", "USD" ] }
{ "_id" : ObjectId("59282a16015fc6b0865e48f9"), "name" : { "common" : "Lesotho" }, "currency" : [ "LSL", "ZAR" ] }
{ "_id" : ObjectId("59282a16015fc6b0865e4915"), "name" : { "common" : "Namibia" }, "currency" : [ "NAD", "ZAR" ] }
{ "_id" : ObjectId("59282a16015fc6b0865e4923"), "name" : { "common" : "Panama" }, "currency" : [ "PAB", "USD" ] }
{ "_id" : ObjectId("59282a16015fc6b0865e493d"), "name" : { "common" : "El Salvador" }, "currency" : [ "SVC", "USD" ] }
{ "_id" : ObjectId("59282a16015fc6b0865e495e"), "name" : { "common" : "Uruguay" }, "currency" : [ "UYI", "UYU" ] }
{ "_id" : ObjectId("59282a16015fc6b0865e495f"), "name" : { "common" : "United States" }, "currency" : [ "USD", "USN", "USS" ] }
>


13.- Cerca el nom dels països que són limítrofs amb Sèrbia.

db.countries.find({$and:[{"borders.1":{$exists:true}}, {borders:"SRB"}]}, {"name.common":1})

{ "_id" : ObjectId("59282a15015fc6b0865e488c"), "name" : { "common" : "Bulgaria" } }
{ "_id" : ObjectId("59282a15015fc6b0865e488f"), "name" : { "common" : "Bosnia and Herzegovina" } }
{ "_id" : ObjectId("59282a16015fc6b0865e48d8"), "name" : { "common" : "Croatia" } }
{ "_id" : ObjectId("59282a16015fc6b0865e48da"), "name" : { "common" : "Hungary" } }
{ "_id" : ObjectId("59282a16015fc6b0865e48f0"), "name" : { "common" : "Kosovo" } }
{ "_id" : ObjectId("59282a16015fc6b0865e4906"), "name" : { "common" : "Macedonia" } }
{ "_id" : ObjectId("59282a16015fc6b0865e490a"), "name" : { "common" : "Montenegro" } }
{ "_id" : ObjectId("59282a16015fc6b0865e4932"), "name" : { "common" : "Romania" } }
>


14.- Cerca el nom dels països en què l’anglès és l’única llengua oficial.

db.countries.find({"languages.eng":"English",$where{Object.keys(db.countries.)}}, {"languages":1}) //TOFIX


15.- Cerca quants idiomes es parlen a Espanya.

> Object.keys(db.countries.find({"name.common":"Spain"}, {"languages":1}).toArray()[0].languages).length
5
