# -*- coding: utf-8 -*-
import sys

#Proves realitzades.
#1.- Enregistrar usuari, borrar el document i llegir el document, mostra correctament l'excepció.
#No he vist que peti per res mes, si reinicio el fitxer des del programa, no peta, tot i tenint el fitxer obert.


def registrar(usuari, cognom, data):
    try:
        file=open("personal.txt", 'a')
        file.write(usuari+":"+cognom+":"+data+"\n")
        file.close()
    except FileNotFoundError:
        print("No s'ha pogut escriure.")
    menu()

def listar():
    try:
        file=open("personal.txt", 'r')
    except FileNotFoundError:
        print("No s'ha trobat el fitxer o no es pot llegir.")
    for linea in file:
        print(linea)
    menu()
        
def ordenar():
    try:
        file=open("personal.txt", 'r')
    except FileNotFoundError:
        print("No s'ha trobat el fitxer o no es pot llegir.")
    filtrat=[]
    for linea in file:
        filtrat.append(linea.strip().split(":"))
    filtrat.sort(key=lambda x:x[2])
    print(filtrat)  
    menu()
    
def filtre():
    index=0
    try:
        file=open("personal.txt", 'r')
    except FileNotFoundError:
        print("No s'ha trobat el fitxer o no es pot llegir.")
    matriu=[]
    for linea in file:
        matriu.append(linea.strip().split(":"))
    filtre=input(print("Per a que vols filtrar? Nom o Cognom?"))
    if(filtre=="Nom"):
        filtre2=input("Per a quin Nom vols filtrar?")
        for i in range(len(matriu)):
            if(matriu[i][index]==filtre2):
                print(matriu[i])
    elif(filtre=="Cognom"):
        index=1
        filtre2=input("Per a quin Cognom vols filtrar?")
        for i in range(len(matriu)):
            if(matriu[i][index]==filtre2):
                print(matriu[i])
    else:
        print("Mala elecció, torna a començar")
        menu()
    menu()
     
def reiniciar():
    try:
        file=open("personal.txt", 'w').close()
    except FileNotFoundError:
        print("No s'ha trobat el fitxer o no es pot llegir.")
    menu()
    
def sortir():
    sys.exit(0)

def menu():
    choose=input(print("1.- Registrar nou usuari\n2.- Llistar els usuaris\n3.- Llistar amb filtre\n4.- Llistar ordenadament per la data\n5.- Esborrar registres\n6.- Sortir"))
    if(choose=="1"):
        usuari=input("Nom: ")
        cognom=input("Cognom: ")
        data=(input("Data de neixement (yyyy-mm-dd): "))
        registrar(usuari, cognom, data)
    elif(choose=="2"):
        listar()
    elif(choose=="3"):
        filtre()
    elif(choose=="4"):
        ordenar()
    elif(choose=="5"):
        reiniciar()
    elif(choose=="6"):
        sortir()
menu()