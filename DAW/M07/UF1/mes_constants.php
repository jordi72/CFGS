<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <?php
  /*  1) Passa una temperatura de 30ºC a Fahrenheit

  Take the temperature in Celsius and multiply 1.8.
  Add 32 degrees.
  The result is degrees Fahrenheit.*/

  define("F",1.8);
  define("ADD",32);
  echo "Exercici 1: ";
  echo (30*F)+ADD."<br/>\n";
/*
2) Fes un programa que escriga en lletres els números del 30 al 49. Utilitza les constants T que val "Trenta" i Q que val "Quaranta"
*/

    define("T","Trenta");
    define("Q","Quaranta");
    $m=array("un","dos","tres","quatre","cinc","sis","set","vuit","nou");
    echo "Exercici 2: <br/>\n";
    echo T."<br/>\n";
    foreach ($m as $valor) {
      echo T."-".$valor." <br/>\n";
    }
    echo Q."<br/>\n";
    foreach ($m as $valor) {
      echo Q."-".$valor." <br/>\n";
    }

    ?>
  </body>
</html>
