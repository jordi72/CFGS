<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <?php
  //  Crea un programa en PHP que calcula la longitud d'una circumferència de radi=3. Longitud=2*PI*r

  //PI ha de ser una constant
      define("PI",3.1415);
      define("RADI",3);
      $longitud=2*PI*RADI;
      echo $longitud;
     ?>
  </body>
</html>
