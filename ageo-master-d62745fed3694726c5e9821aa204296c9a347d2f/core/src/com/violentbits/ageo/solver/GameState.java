package com.violentbits.ageo.solver;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.violentbits.ageo.apparat.Cell;
import com.violentbits.ageo.apparat.Program;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class GameState {
	private static StringBuilder builder = new StringBuilder();
	
	private GameState() {}
	
	public static String computeGameState(WorldController worldController) {
		builder.setLength(0);
		builder.append(worldController.getCurrentBotIndex()).append(" ");
		builder.append(worldController.getCurrentBotSpeed()).append(" ");
		for (Bot bot : worldController.getBots()) {
			computeBotState(bot);
		}
		return builder.toString();
	}
	
	private static void computeBotState(Bot bot) {
		builder.append("B(");
		builder.append(Math.round(bot.getX())).append(" ");
		builder.append(Math.round(bot.getY())).append(" ");
		builder.append(computeRotation(bot.getRotation()));
		builder.append(bot.isActive());
		computeProgramState(bot.getProgram());
		builder.append(")");
	}
	
	private static String computeRotation(float rotation) {
		String result;
		rotation = (720+rotation)%360;
		if (MathUtils.isEqual(rotation, 90f, 1f))
			result="L";
		else if (MathUtils.isEqual(rotation, 180f, 1f))
			result="D";
		else if (MathUtils.isEqual(rotation, 270f, 1f))
			result="R";
		else
			result="U";
		return result;
	}
	
	private static void computeProgramState(Program program) {
		builder.append("PS(");
		builder.append(program.getCounter()).append(" ");
		builder.append(program.getNInstructions()).append(" ");
		Array<Cell> cells = program.getCells();
		for (int i=0; i<program.getNInstructions(); i++) {
			builder.append(cells.get(i).getInstruction().getName()).append(" ");
		}
		builder.append(")");
	}
}
