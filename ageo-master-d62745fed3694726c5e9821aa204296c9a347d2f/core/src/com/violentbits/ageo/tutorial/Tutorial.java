package com.violentbits.ageo.tutorial;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.removeActor;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.utils.Array;
import com.violentbits.ageo.apparat.AbstractCell;
import com.violentbits.ageo.apparat.Cell;
import com.violentbits.ageo.apparat.CellListener;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.HudRenderer;
import com.violentbits.ageo.game.WorldController;
import com.violentbits.ageo.game.WorldListener;
import com.violentbits.ageo.instructions.Instruction;
import com.violentbits.ageo.io.Assets;
import com.violentbits.ageo.tutorial.TutorialStep.TutorialEvent;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * An in-game tutorial. A tutorial is a sequence of steps. Each step
 * may involve one target actor, and all steps have an event that
 * causes the transition to the next step.
 */
public class Tutorial implements WorldListener, CellListener {
	private Array<TutorialStep> steps = new Array<TutorialStep>();
	private int currentStepIndex=-1;
	private TutorialStep currentStep;
	private HudRenderer renderer;
	private WorldController worldController;
	private ClickListener botClickListener = new ClickListener() {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			Bot bot = (Bot) event.getListenerActor();
			renderer.getInstructionsTable().setDisabled(true);
			renderer.getProgramTable().setDisabled(true);
			if (currentStep!= null && currentStep.getEndEvent()==TutorialEvent.ON_BOT_SELECTED
					&& bot==worldController.getBots().get(((IntTutorialStep)currentStep).getInt())) {
				nextStep();
			}
		}
	};
	private ClickListener stageClickListener = new ClickListener() {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			renderer.getHudStage().removeListener(this);
			nextStep();
		}
	};
	/**
	 * Constructor.
	 * 
	 * @param worldController  A reference to the world controller.
	 * @param renderer  A reference to the world renderer.
	 * @param tutorialInstructions  An array of strings that specifies the steps in the tutorial.
	 */
	public Tutorial(WorldController worldController, HudRenderer renderer, String[] tutorialInstructions) {
		this.renderer = renderer;
		this.worldController = worldController;
		worldController.addListener(this);
		
		Array<Bot> bots = worldController.getBots();
		for (Bot b : bots) {
			b.addListener(botClickListener);
			b.setTouchable(Touchable.disabled);
			for (Cell c : b.getProgram().getCells()) {
				c.addListener(this);
			}
		}
		
		renderer.setDisabled(true);
		renderer.getInstructionsTable().setDisabled(true);
		renderer.getProgramTable().setDisabled(true);
		steps = TutorialFactory.parseTutorial(tutorialInstructions, worldController, renderer);
		Stage stage = Tutorial.this.renderer.getHudStage();
		Label tutorialLabel = new Label("Tutorial", Assets.instance.skin, "title");
		tutorialLabel.setPosition(
				(stage.getWidth()-tutorialLabel.getWidth())*0.5f,
				(stage.getHeight()-tutorialLabel.getHeight())*0.75f);
		tutorialLabel.setColor(1, 1, 1, 0);
		tutorialLabel.addAction(sequence(fadeIn(1f), delay(1.5f), fadeOut(1), run(new Runnable() {
			@Override
			public void run() {
				nextStep();
			}
		}), removeActor()));
		tutorialLabel.setTouchable(Touchable.disabled);
		stage.addActor(tutorialLabel);
	}

	@Override
	public void worldChanged(EventType event, Object object) {
		if (currentStep!=null) {
			TutorialEvent endEvent = currentStep.getEndEvent();
			if (event==EventType.RUN && endEvent==TutorialEvent.ON_PLAY) {
				nextStep();
			} else if (event==EventType.STOP && endEvent==TutorialEvent.ON_STOP) {
				nextStep();
			} else if (event==EventType.ACTION && endEvent==TutorialEvent.ON_NSTEPS
					&& worldController.getNSteps()>=((IntTutorialStep)currentStep).getInt()) {
				nextStep();
			} else if (event==EventType.WON && endEvent==TutorialEvent.ON_WIN) {
				nextStep();
			}
		}
	}

	@Override
	public void changed(AbstractCell cell, Instruction oldValue, Instruction newValue) {
		if (currentStep!=null) {
			TutorialEvent endEvent = currentStep.getEndEvent();
			if (currentStep instanceof InstructionTutorialStep) {
				InstructionTutorialStep step = (InstructionTutorialStep) currentStep;
				if (newValue!=null && newValue.getName().equals(step.getInstructionName())) {
					nextStep();
				}
			} else if (endEvent==TutorialEvent.ON_INSTRUCTION_REMOVED &&
					newValue==null) {
				nextStep();
			}
		}
	}
	
	private void nextStep() {
		if (currentStep!=null) {
			currentStep.end();
		}
		currentStepIndex++;
		if (steps.size>currentStepIndex) {
			currentStep = steps.get(currentStepIndex);
			currentStep.start();
			if (currentStep.getEndEvent()==TutorialEvent.ON_CLICK) {
				renderer.getHudStage().addListener(stageClickListener);
			}
		} else {
			renderer.setDisabled(false);
			renderer.getInstructionsTable().setDisabled(false);
			renderer.getProgramTable().setDisabled(false);
			for (Bot b : worldController.getBots()) {
				b.setTouchable(Touchable.enabled);
				b.removeListener(botClickListener);
			}
		}
	}

	@Override
	public void event(CellEvent event, AbstractCell cell, Object eventInfo) {
		// Nothing to do
	}
}
