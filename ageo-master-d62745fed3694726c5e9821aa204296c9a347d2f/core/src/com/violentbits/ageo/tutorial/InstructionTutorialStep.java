package com.violentbits.ageo.tutorial;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.violentbits.ageo.apparat.CellActor;
import com.violentbits.ageo.game.HudRenderer;

public class InstructionTutorialStep extends TutorialStep {
	private HudRenderer renderer;
	private String instructionName;

	public InstructionTutorialStep(HudRenderer renderer, String instructionName) {
		super(null, TutorialEvent.ON_INSTRUCTION_ADDED);
		this.renderer = renderer;
		this.instructionName = instructionName;
	}

	@Override
	public void start() {
		for (Actor actor : renderer.getInstructionsTable().getCellActors()) {
			CellActor cellActor = (CellActor) actor;
			if (cellActor.getCell().getInstruction().getName().equals(instructionName)) {
				target=cellActor;
			}
		}
		super.start();
	}
	
	public String getInstructionName() {
		return instructionName;
	}
}
