package com.violentbits.ageo;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.violentbits.ageo.io.Assets;

/**
 * Main game class.
 */
public class AgeoGame extends Game {
	/**
	 * Starts the game.
	 */
	@Override
	public void create() {
		Gdx.app.setLogLevel(Constants.LOG_LEVEL);
		Assets.instance.init(new AssetManager());
		setScreen(new MenuScreen(this));
	}
}
