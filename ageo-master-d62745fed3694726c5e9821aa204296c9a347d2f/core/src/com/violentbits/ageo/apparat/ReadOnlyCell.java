package com.violentbits.ageo.apparat;

import com.violentbits.ageo.instructions.Instruction;

public class ReadOnlyCell extends AbstractCell {

	@Override
	public boolean put(Instruction instruction) {
		boolean put = false;
		if (this.instruction == null) {
			this.instruction = instruction;
			notifyListeners(null);
			put = true;
		}
		return put;
	}

	@Override
	public Instruction remove() {
		return instruction;
	}

}
