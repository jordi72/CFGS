package com.violentbits.ageo.apparat;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Payload;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Source;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Target;

public class CellTarget extends Target {

    public CellTarget(CellActor actor) {
        super(actor);
        getActor().setColor(Color.LIGHT_GRAY);
    }

    @Override
    public boolean drag(Source source, Payload payload, float x, float y, int pointer) {
        // in case we drag something over this target, we highlight it a bit
        getActor().setColor(1,1,1,0.8f);
        // returning true means that this is a valid target
        return true;
    }

    @Override
    public void drop(Source source, Payload payload, float x, float y, int pointer) {
        // we already handle all of this in dragStop in the Source
    }

    @Override
    public void reset(Source source, Payload payload) {
        getActor().setColor(Color.LIGHT_GRAY);
    }

}
