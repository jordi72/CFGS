package com.violentbits.ageo.apparat;

import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.instructions.Instruction;

public interface ProgramListener {
	public void executed(Bot bot, Instruction instruction, int counter);
}
