package com.violentbits.ageo.victory;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Array;
import com.violentbits.ageo.game.WorldController;

public class ManyGoToVictoryCondition extends AbstractVictoryCondition {
	private Array<GoToVictoryCondition> goToVictories = new Array<GoToVictoryCondition>();

	public ManyGoToVictoryCondition() {}

	public ManyGoToVictoryCondition(int botId, int x, int y) {
		addVictoryCondition(botId, x, y);
	}
	
	public void addVictoryCondition(int botId, int x, int y) {
		goToVictories.add(new GoToVictoryCondition(botId, x, y));
	}
	
	@Override
	public boolean checkVictory(WorldController worldController) {
		boolean victory = true;
		for (GoToVictoryCondition goToVictory : goToVictories) {
			victory &= goToVictory.checkVictory(worldController);
			if (!victory) break;
		}
		return victory;
	}

	@Override
	public void help(final WorldController worldController, final Stage worldStage, final Stage hudStage) {
		float time = 0;
		for (final GoToVictoryCondition goToVictory : goToVictories) {
			hudStage.addAction(Actions.delay(time, Actions.run(new Runnable() {
				@Override
				public void run() {
					goToVictory.help(worldController, worldStage, hudStage);
				}
			})));
			time+=arrowTime*2;
		}
	}

}
