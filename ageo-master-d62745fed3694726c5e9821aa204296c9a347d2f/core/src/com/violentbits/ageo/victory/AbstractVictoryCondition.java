package com.violentbits.ageo.victory;

import static com.badlogic.gdx.math.Interpolation.bounceOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.removeActor;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.repeat;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.violentbits.ageo.io.Assets;

public abstract class AbstractVictoryCondition implements VictoryCondition {
	protected static final float arrowTime = 4.8f;
	
	protected Image createArrow(String text, float x, float y, Stage worldStage, Stage hudStage) {
		Vector2 coord;
		
		Image arrow = new Image(Assets.instance.get("arrow"));
		Label label = new Label(text, Assets.instance.skin);
		int desp=20;
		arrow.setOrigin(arrow.getWidth()/2f, arrow.getHeight()/2f);
		label.setOrigin(label.getWidth()/2f, label.getHeight()/2f);
		if (y>worldStage.getHeight()/2f) { // up
			coord = worldStage.stageToScreenCoordinates(new Vector2(x+0.5f,y));
			coord = hudStage.screenToStageCoordinates(coord);
			desp=-desp;
			arrow.setPosition(coord.x-arrow.getWidth()/2f, coord.y-arrow.getHeight());
			label.setPosition(coord.x-label.getWidth()/2f, arrow.getY()-label.getHeight()+desp);
		} else { // down
			coord = worldStage.stageToScreenCoordinates(new Vector2(x+0.5f,y+1f));
			coord = hudStage.screenToStageCoordinates(coord);
			arrow.setPosition(coord.x-arrow.getWidth()/2f, coord.y);
			arrow.setRotation(180);
			label.setPosition(coord.x-label.getWidth()/2f, arrow.getY()+arrow.getHeight()+desp);
		}
		hudStage.addActor(arrow);
		hudStage.addActor(label);
		arrow.addAction(sequence(
			repeat(3, sequence(
				moveBy(0, desp, 0.8f),
				moveBy(0, -desp, 0.8f, bounceOut)
			)),
			removeActor(label),
			removeActor()
		));
		return arrow;
	}
}
