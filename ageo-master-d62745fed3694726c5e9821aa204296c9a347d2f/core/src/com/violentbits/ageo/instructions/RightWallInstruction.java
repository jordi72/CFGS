package com.violentbits.ageo.instructions;

public class RightWallInstruction extends MoveToWallInstruction implements RotableInstruction {
	public RightWallInstruction() {
		dx=1;
	}
}
