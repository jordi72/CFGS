package com.violentbits.ageo.instructions;

import com.violentbits.ageo.apparat.Program;
import com.violentbits.ageo.DirectionsHelper;
import com.violentbits.ageo.apparat.CellListener.CellEvent;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;
import com.violentbits.ageo.scene.ActionsEx;

public class RotateWorldInstruction extends Instruction {
private int rotation;
	
	@Override
	protected void setName(String name) {
		String[] nameParts = name.split("#");
		super.setName(nameParts[0]);
		rotation = Integer.parseInt(nameParts[1]);
	}

	@Override
	public String getName() {
		return super.getName()+"#"+rotation;
	}
	
	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		bot.addAction(ActionsEx.rotateCamera(rotation, true, worldController.actionTime));
		rotateInstructions(worldController);
		return true;
	}

	@Override
	public boolean runInstantly(WorldController worldController, Bot bot) {
		rotateInstructions(worldController);
		return true;
	}
	
	private void rotateInstruction(float camRotation, String newName, Program program, int pos) {
		final Instruction newIns=InstructionFactory.instance.getInstruction(newName);
		program.setInstruction(pos, newIns);
		program.sendEvent(pos, CellEvent.ROTATE_INSTANTLY, camRotation);
	}
	
	private void rotateInstructions(WorldController worldController) {
		String newName;
		Program program;
		float camRotation=-DirectionsHelper.getCurrentRotation(worldController)-rotation;
		
		for (Bot bot : worldController.getBots()) {
			program = bot.getProgram();
			for (int pos=0; pos<program.getNInstructions(); pos++) {
				Instruction ins = program.getInstruction(pos);
				String insName = ins.getName();
				if (ins instanceof MoveToWallInstruction) {
					newName = DirectionsHelper.rotate(insName.split("W", 2)[0], rotation)+"Wall";
					rotateInstruction(camRotation, newName, program, pos);
				} else if (ins instanceof MovementInstruction) {
					newName = DirectionsHelper.rotate(insName, rotation);
					rotateInstruction(camRotation, newName, program, pos);
				}
			}
		}
	}
}
