package com.violentbits.ageo.instructions;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;
import com.violentbits.ageo.scene.LaserActor;

public class LaserInstruction extends Instruction {
	private Actor activate(WorldController worldController, Bot bot) {
		LaserActor laser = new LaserActor(worldController);
		laser.activate(bot);
		return laser;
	}

	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		Actor laser = activate(worldController, bot);
		worldController.addActor(laser);
		return true;
	}

	@Override
	public boolean runInstantly(WorldController worldController, Bot bot) {
		activate(worldController, bot);
		return true;
	}
}
