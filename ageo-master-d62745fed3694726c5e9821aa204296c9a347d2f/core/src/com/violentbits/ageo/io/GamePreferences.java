package com.violentbits.ageo.io;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.MathUtils;

public class GamePreferences {
	public static final String TAG = GamePreferences.class.getName();
	public static final GamePreferences instance = new GamePreferences();

	public boolean sound;
	public boolean music;
	public float soundVol;
	public float musicVol;

	private Preferences prefs;
	
	private Profile profile;

	private GamePreferences() {
		prefs = Gdx.app.getPreferences("config.ini");
	}
	
	public void setCurrentProfile(int nProfile) {
		if (profile==null || profile.getNProfile()!=nProfile) {
			profile = new Profile(nProfile);
		}
	}
	
	public Profile getCurrentProfile() {
		return profile;
	}
	
	public Profile getProfile(int nProfile) {
		return new Profile(nProfile);
	}

	public void load() {
		sound = prefs.getBoolean("sound", true);
		music = prefs.getBoolean("music", true);
		soundVol = MathUtils.clamp(prefs.getFloat("soundVol", 0.5f), 0.0f, 1.0f);
		musicVol = MathUtils.clamp(prefs.getFloat("musicVol", 0.5f), 0.0f, 1.0f);
		int nProfile = prefs.getInteger("profile", -1);
		if (nProfile>=0) {
			setCurrentProfile(nProfile);
		}
	}

	public void save() {
		prefs.putBoolean("sound", sound);
		prefs.putBoolean("music", music);
		prefs.putFloat("soundVol", soundVol);
		prefs.putFloat("musicVol", musicVol);
		if (profile!=null) {
			prefs.putInteger("profile", profile.getNProfile());
		}
		prefs.flush();
	}	
}
