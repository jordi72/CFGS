package com.violentbits.ageo.io;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTile.BlendMode;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.io.LevelStage;

/**
 * Class used to manage game assets.
 */
public class Assets implements Disposable, AssetErrorListener {
	private static final String TAG = Assets.class.getName();
	/**
	 * A single instance of the Assets class.
	 */
	public static final Assets instance = new Assets();
		
	public Skin skin;
	public ParticleEffect effect;
	
	private ObjectMap<String, AtlasRegion> textures = new ObjectMap<String, AtlasRegion>();
	private ObjectMap<String, NinePatch> ninePatches = new ObjectMap<String, NinePatch>();
	
	/**
	 * The AssetManager used to load assets.
	 */
	private AssetManager assetManager;
	
	private LevelInfo levelInfo;
	
	private Assets () {}
	/**
	 * Loads all assets using the provided AssetManager.
	 * 
	 * @param assetManager  The AssetManager to use.
	 */
	public void init(AssetManager assetManager) {
		this.assetManager = assetManager;
		// set asset manager error handler
		assetManager.setErrorListener(this);
		// load texture atlas
		assetManager.load("atlas.atlas", TextureAtlas.class);
		// start loading assets and wait until finished
		InternalFileHandleResolver resolver = new InternalFileHandleResolver();
		assetManager.setLoader(TiledMap.class, new TmxMapLoader(resolver));
		assetManager.setLoader(LevelStageArray.class, new LevelInfoLoader(resolver));
		assetManager.setLoader(BotArray.class, new BotLoader(resolver));
		assetManager.load("levels/levels.json", LevelStageArray.class);
		assetManager.load("uiskin.json", Skin.class);
		//skin = new Skin(Gdx.files.internal("uiskin.json"));
		assetManager.load("sounds/move.ogg", Sound.class);
		assetManager.load("sounds/delete.wav", Sound.class);
		assetManager.load("sounds/swap.wav", Sound.class);
		assetManager.load("sounds/no.ogg", Sound.class);
		assetManager.load("sounds/highUp.mp3", Sound.class);
		assetManager.load("sounds/lowRandom.mp3", Sound.class);
		assetManager.load("sounds/lowThreeTone.mp3", Sound.class);
		assetManager.load("sounds/select.wav", Sound.class);
		assetManager.load("sounds/win.ogg", Sound.class);
		assetManager.load("sounds/deactivate.wav", Sound.class);
		
		for (int i=1; i<=8; i++)
			assetManager.load("sounds/sphere"+i+".ogg", Sound.class);
		
		assetManager.load("music/imrobot_v01.mp3", Music.class);
		assetManager.finishLoading();
		
		
		
		if (levelInfo!=null)
			loadLevel(levelInfo);
		skin = assetManager.get("uiskin.json", Skin.class);
		TextureAtlas atlas = assetManager.get("atlas.atlas", TextureAtlas.class);
		Array<AtlasRegion> regions = atlas.getRegions();
		for (AtlasRegion region : regions) {
			if (region.splits == null)
				textures.put(region.name, region);
			else
				ninePatches.put(region.name, atlas.createPatch(region.name));
		}

		Gdx.app.debug(TAG, "# of assets loaded: " + assetManager.getAssetNames().size);
		/*for (String a : assetManager.getAssetNames())
			Gdx.app.debug(TAG, "asset: " + a);*/
		
		effect = new ParticleEffect();
		effect.load(Gdx.files.internal("transfer.effect"), atlas);
	}
	
	public void loadLevel(LevelInfo levelInfo) {
		if (this.levelInfo != null) {
			if (assetManager.isLoaded(this.levelInfo.map))
				assetManager.unload(this.levelInfo.map);
			if (assetManager.isLoaded(this.levelInfo.bots))
				assetManager.unload(this.levelInfo.bots);
		}
		this.levelInfo = levelInfo;
		assetManager.load(levelInfo.map, TiledMap.class);
		assetManager.load(levelInfo.bots, BotArray.class);
		assetManager.finishLoading();
		GamePreferences.instance.getCurrentProfile().loadLevel(levelInfo.name);
		// mark tiles as opaque
		TiledMapTileSet tileSet = getMap().getTileSets().getTileSet(0);
		for (TiledMapTile tile : tileSet) {
			tile.setBlendMode(BlendMode.NONE);
		}
	}
	
	public AtlasRegion get(String textureName) {
		//Gdx.app.debug(TAG, "Requested "+textureName);
		return textures.get(textureName);
	}
	
	public NinePatch getNinePatch(String name) {
		//Gdx.app.debug(TAG, "Requested NinePatch: "+name);
		return ninePatches.get(name);
	}
	
	public Sound getSound(String soundName) {
		//Gdx.app.debug(TAG, "Requested "+soundName);
		return assetManager.get("sounds/"+soundName, Sound.class);
	}
	
	public Music getMusic(String musicName) {
		return assetManager.get("music/"+musicName, Music.class);
	}
	
	public TiledMap getMap() {
		TiledMap map = null;
		if (levelInfo != null)
			map = assetManager.get(levelInfo.map); 
		return map;
	}
	
	public Array<Bot> getBots() {
		Array<Bot> bots = null;
		if (levelInfo != null)
			bots = ((BotArray)assetManager.get(levelInfo.bots)).bots;
		return bots;
	}
	
	public LevelInfo getLevelInfo() {
		return levelInfo;
	}
	
	public Array<LevelStage> getStages() {
		return ((LevelStageArray)assetManager.get("levels/levels.json")).levelStages;
	}
	
	/**
	 * Disposes the AssetManager.
	 */
	@Override
	public void dispose () {
		assetManager.dispose();
		effect.dispose();
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void error(AssetDescriptor asset, Throwable throwable) {
		Gdx.app.error(TAG, "Couldn't load asset '" + asset.fileName + "'", (Exception)throwable);
	}
	
	public ImageButtonStyle createButtonStyle(String name) {
		ImageButtonStyle style = new ImageButtonStyle();
		TextureRegionDrawable imageUp = new TextureRegionDrawable(Assets.instance.get(name));
		style.up = imageUp;
		style.down = imageUp.tint(Constants.LIGHT_GRAY);
        style.disabled = imageUp.tint(Constants.GRAY);
		return style;
	}
}
