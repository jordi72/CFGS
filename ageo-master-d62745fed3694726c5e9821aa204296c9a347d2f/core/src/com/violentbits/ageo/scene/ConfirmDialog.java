package com.violentbits.ageo.scene;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.violentbits.ageo.MenuScreen;
import com.violentbits.ageo.io.Assets;

public class ConfirmDialog extends Table {
	private Skin skin;
	private String question;
	private Label questionLbl;
	private ConfirmListener listener;
	private MenuScreen menuScreen;

	public ConfirmDialog(MenuScreen menuScreen) {
		this.menuScreen = menuScreen;
		this.skin = menuScreen.getSkin();
		init();
	}
	
	public void setQuestion(String question) {
		this.question = question;
		questionLbl.setText(question);
		this.invalidate();
	}
	
	public void setConfirmListener(ConfirmListener listener) {
		this.listener = listener;
	}
	
	private void returnToCaller() {
		menuScreen.back();
		//addAction(moveTo(getX(), -getHeight(),  ACTION_TIME_FAST));
	}
	
	private void init() {
		questionLbl = new Label(question, skin);
		defaults().space(30f);
		add(questionLbl).center().colspan(2).row();
		Button cancelBtn = new Button(new TextureRegionDrawable(Assets.instance.get("End")));
		cancelBtn.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				returnToCaller();
			}
		});
		add(cancelBtn);
		Button okBtn = new Button(new TextureRegionDrawable(Assets.instance.get("Activate")));
		okBtn.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (listener!=null)
					listener.confirmed();
				returnToCaller();
			}
		});
		add(okBtn);
		pack();
		
	}
}
