package com.violentbits.ageo.scene;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class ActionsEx {
	
	static public MoveCameraAction moveCamera() {
		return moveCamera(0, 0, 0, null);
	}

	static public MoveCameraAction moveCamera(float duration) {
		return moveCamera(0, 0, duration, null);
	}

	static public MoveCameraAction moveCamera(float duration, Interpolation interpolation) {
		return moveCamera(0, 0, duration, interpolation);
	}
	
	static public MoveCameraAction moveCamera(float dx, float dy, float duration) {
		return moveCamera(dx, dy, duration, null);
	}
	
	static public MoveCameraAction moveCamera(float dx, float dy, float duration, Interpolation interpolation) {
		MoveCameraAction action = Actions.action(MoveCameraAction.class);
		action.setDuration(duration);
		action.setInterpolation(interpolation);
		action.setDx(dx);
		action.setDy(dy);
		return action;
	}

	static public MoveCameraAction moveCameraAligned(int alignment) {
		return moveCameraAligned(alignment, 0, 0, 0, null);
	}

	static public MoveCameraAction moveCameraAligned(int alignment, float duration) {
		return moveCameraAligned(alignment, 0, 0, duration, null);
	}

	static public MoveCameraAction moveCameraAligned(int alignment, float duration, Interpolation interpolation) {
		return moveCameraAligned(alignment, 0, 0, duration, interpolation);
	}
	
	static public MoveCameraAction moveCameraAligned(int alignment, float dx, float dy, float duration) {
		return moveCameraAligned(alignment, dx, dy, duration, null);
	}
	
	static public MoveCameraAction moveCameraAligned(int alignment, float dx, float dy, float duration, Interpolation interpolation) {
		MoveCameraAction action = Actions.action(MoveCameraAction.class);
		action.setAlignment(alignment);
		action.setDuration(duration);
		action.setInterpolation(interpolation);
		action.setDx(dx);
		action.setDy(dy);
		return action;
	}
	
	static public RotateCameraAction rotateCamera(float rotation, boolean absolute, float duration) {
		return rotateCamera(rotation, absolute, duration, null);
	}
	
	static public RotateCameraAction rotateCamera(float rotation, boolean absolute, float duration, Interpolation interpolation) {
		RotateCameraAction action = Actions.action(RotateCameraAction.class);
		action.setEndRotation(rotation);
		action.setAbsolute(absolute);
		action.setDuration(duration);
		action.setInterpolation(interpolation);
		return action;
	}
}
