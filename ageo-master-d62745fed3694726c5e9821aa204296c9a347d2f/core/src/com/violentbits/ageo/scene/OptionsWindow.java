package com.violentbits.ageo.scene;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.violentbits.ageo.MenuScreen;
import com.violentbits.ageo.SoundController;
import com.violentbits.ageo.io.Assets;
import com.violentbits.ageo.io.GamePreferences;

public class OptionsWindow extends Table {
	private TextButton btnWinOptSave;
	private TextButton btnWinOptCancel;
	private CheckBox chkSound;
	private Slider sldSound;
	private CheckBox chkMusic;
	private Slider sldMusic;
	
	private MenuScreen menuScreen;

	public OptionsWindow(MenuScreen menuScreen) {
		this.menuScreen = menuScreen;
		setSkin(menuScreen.getSkin());
		init();
	}

	private void init() {
		add(buildOptWinAudioSettings()).row();
		add(buildOptWinButtons()).pad(10, 0, 10, 0);
		pack();
		loadSettings();
	}

	private Table buildOptWinAudioSettings() {
		Skin skin = getSkin();
		Table tbl = new Table();
		// + Title: "Audio"
		tbl.pad(10, 10, 0, 10);
		tbl.add(new Label("Audio", skin, "default-font", Color.ORANGE)).colspan(3);
		tbl.row();
		tbl.columnDefaults(0).padRight(10);
		tbl.columnDefaults(1).padRight(10);
		// + Checkbox, "Sound" label, sound volume slider
		chkSound = new CheckBox("", skin);
		tbl.add(chkSound);
		tbl.add(new Label("Sound", skin));
		sldSound = new Slider(0.0f, 1.0f, 0.1f, false, skin);
		tbl.add(sldSound);
		tbl.row();
		// + Checkbox, "Music" label, music volume slider
		chkMusic = new CheckBox("", skin);
		tbl.add(chkMusic);
		tbl.add(new Label("Music", skin));
		sldMusic = new Slider(0.0f, 1.0f, 0.1f, false, skin);
		tbl.add(sldMusic);
		tbl.row();
		return tbl;
	}

	private Table buildOptWinButtons() {
		Skin skin = getSkin();
		Table tbl = new Table();
		// + Separator
		Label lbl = null;
		lbl = new Label("", skin);
		lbl.setColor(0.75f, 0.75f, 0.75f, 1);
		lbl.setStyle(new LabelStyle(lbl.getStyle()));
		lbl.getStyle().background = skin.newDrawable("white");
		tbl.add(lbl).colspan(2).height(1).width(220).pad(0, 0, 0, 1);
		tbl.row();
		lbl = new Label("", skin);
		lbl.setColor(0.5f, 0.5f, 0.5f, 1);
		lbl.setStyle(new LabelStyle(lbl.getStyle()));
		lbl.getStyle().background = skin.newDrawable("white");
		tbl.add(lbl).colspan(2).height(1).width(220).pad(0, 1, 5, 0);
		tbl.row();
		// + Save Button with event handler
		btnWinOptSave = new TextButton("Save", skin);
		tbl.add(btnWinOptSave).padRight(30);
		btnWinOptSave.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				onSaveClicked();
			}
		});
		// + Cancel Button with event handler
		btnWinOptCancel = new TextButton("Cancel", skin);
		tbl.add(btnWinOptCancel);
		btnWinOptCancel.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				onCancelClicked();
			}
		});
		return tbl;
	}

	private void loadSettings() {
		GamePreferences prefs = GamePreferences.instance;
		prefs.load();
		chkSound.setChecked(prefs.sound);
		sldSound.setValue(prefs.soundVol);
		chkMusic.setChecked(prefs.music);
		sldMusic.setValue(prefs.musicVol);
	}

	private void saveSettings() {
		GamePreferences prefs = GamePreferences.instance;
		prefs.sound = chkSound.isChecked();
		prefs.soundVol = sldSound.getValue();
		prefs.music = chkMusic.isChecked();
		prefs.musicVol = sldMusic.getValue();
		prefs.save();
	}

	private void onSaveClicked() {
		saveSettings();
		SoundController.instance.onSettingsUpdated();
		returnToMenu();
	}

	private void onCancelClicked() {
		loadSettings();
		SoundController.instance.onSettingsUpdated();
		returnToMenu();
	}
	
	private void returnToMenu() {
		SoundController.instance.play(Assets.instance.getSound("select.wav"));
		menuScreen.back();
		/*MainMenu mainMenu = menuScreen.getMainMenu();
		mainMenu.addAction(
				delay(ACTION_TIME_FAST,
						moveTo(getX()+getWidth()/2f-mainMenu.getWidth()/2f, getY(), ACTION_TIME_FAST)));
		addAction(moveTo(-getWidth(), getY(),  ACTION_TIME_FAST));*/
	}
}
