package com.violentbits.ageo.scene;

import static com.violentbits.ageo.Constants.ACTION_TIME_FAST;
import static com.violentbits.ageo.scene.ActionsEx.moveCameraAligned;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.violentbits.ageo.AgeoGame;
import com.violentbits.ageo.InstructionsScreen;
import com.violentbits.ageo.LevelScreen;
import com.violentbits.ageo.MenuScreen;
import com.violentbits.ageo.SoundController;
import com.violentbits.ageo.io.Assets;
import com.violentbits.ageo.io.GamePreferences;
import com.violentbits.ageo.io.Profile;

public class MainMenu extends Table {
	private MenuScreen menuScreen;
	private AgeoGame game;
	private TextButton continueBtn;
	private Label welcomeLbl;
	private Skin skin;

	public MainMenu(MenuScreen menuScreen, AgeoGame game) {
		this.menuScreen = menuScreen;
		this.game = game;
		init();
	}
	
	private TextButton createContinueBtn() {
		TextButton continueBtn = new TextButton("Continue", skin);
		continueBtn.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				SoundController.instance.play(Assets.instance.getSound("select.wav"));
				game.setScreen(new LevelScreen(game));
			}
		});
		return continueBtn;
	}
	
	private TextButton createProfilesBtn() {
		TextButton profilesBtn = new TextButton("Profiles", skin);
		profilesBtn.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				SoundController.instance.play(Assets.instance.getSound("select.wav"));
				menuScreen.pushActor(MainMenu.this);
				menuScreen.getGameChooser().addAction(moveCameraAligned(Align.center, ACTION_TIME_FAST));
			}
		});
		return profilesBtn;
	}
	
	private TextButton createOptionsBtn() {
		TextButton optionsBtn = new TextButton("Options", skin);
		optionsBtn.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				SoundController.instance.play(Assets.instance.getSound("select.wav"));
				menuScreen.pushActor(MainMenu.this);
				OptionsWindow optionsWindow = menuScreen.getOptionsWindow();
				optionsWindow.addAction(moveCameraAligned(Align.center, ACTION_TIME_FAST));
			}
		});
		return optionsBtn;
	}
	
	private TextButton createInstructionsBtn() {
		TextButton instructionsBtn = new TextButton("Instructions", skin);
		instructionsBtn.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				SoundController.instance.play(Assets.instance.getSound("select.wav"));
				game.setScreen(new InstructionsScreen(game));
			}
		});
		return instructionsBtn;
	}

	private void init() {
		skin = menuScreen.getSkin();
		defaults().space(20f);
		welcomeLbl = new Label("Welcome", skin);
		add(welcomeLbl).expandX().center().row();
		continueBtn = createContinueBtn();
		add(continueBtn).width(150f).height(30f).row();
		add(createProfilesBtn()).width(150f).height(30f).row();
		add(createOptionsBtn()).width(150f).height(30f).row();
		add(createInstructionsBtn()).width(150f).height(30f);
		pack();
		refresh();
	}
	
	public void refresh() {
		Profile profile = GamePreferences.instance.getCurrentProfile();
		if (profile==null || profile.getName().equals("")) {
			welcomeLbl.setText("Welcome");
		} else {
			welcomeLbl.setText("Welcome "+profile.getName());
		}
		continueBtn.setDisabled(profile==null);
	}
}
