int [][][] maps={{
                {0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0},
                }};
int map = -1;
//loading module
PImage land;

void setup(){
  map = 0;
  background(153);
  size(320, 320);
  land = loadImage("1.png");
}

void draw(){
  if(map==0){
    for(int i=0;i<maps[map].length;i++){
      for(int j=0;j<maps[map][i].length;j++){
        if(maps[map][j][i]==0){
          image(land, i*32, j*32);
        }
      }
    }
  }
}