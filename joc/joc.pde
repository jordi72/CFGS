 /*0=terra 1=muro 2=porta 3=obj1 4=obj2 5=muroporta 6? 7imp 8=miniBoss 9=player*/
                    //Mapa1 aball
int [][][] mapas={{{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                   {1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,0,0,3,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,1,1,2,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1},
                   {1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,5,1},
                   {1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1},
                   {1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,1,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,1},
                   {1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1},
                   {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1} },
                   //Mapa2 aball
                 { {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                   {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1},
                   {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,5,1},
                   {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1},
                   {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                   {1,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1},
                   {1,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,1},
                   {1,1,1,1,1,1,1,1,1,1,0,1,0,0,0,0,0,0,0,1},
                   {1,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,1},
                   {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1} }}; 
int lvl = -1;
//Variables Jugador
int xJugador = 3;
int yJugador = 3;
int hpJugador = 5;
boolean jugadorViu=true;

//Variables Imp
int xImp = 10;
int yImp = 17;
int hpImp = 5;
boolean impViu=true;
boolean lluitaImp=false;
boolean tempsLluitaImp=false;
boolean tempsDefensaImp=false;
//Variables Lock
int xLock = 17;
int yLock = 2;
int hpLock = 15;
boolean lockViu = true;
boolean lluitaLock = false;
boolean tempsLluitaLock=false;
boolean tempsDefensaLock=false;
//Variables Objs
int xObj1 = 4;
int yObj1 = 4;
int xObj2 = 11;
int yObj2 = 18;
boolean obj1Agafat = false;
boolean obj2Agafat = false;
//Obj End
boolean dibuixar = false;
boolean moviment = true;
PImage terra;
PImage muro;
PImage porta;
PImage muroPorta;
PImage obj1;
PImage obj2;
PImage lock;
PImage imp;
PImage boss;
PImage player;

boolean introText = true;


void setup(){
  background(153);
  size(700,672);
  intro();
  terra = loadImage("1.png");
  muro = loadImage("0.png");
  porta = loadImage("cd.png");
  obj1 = loadImage("3.png");
  obj2 = loadImage("4.png");
  player = loadImage("player.png");
  muroPorta = loadImage("cd.png");
  lock = loadImage("miniBoss.png");
  imp = loadImage("imp.png");
}


void draw(){
  
  if(lvl==0){
  fill(0);
  textSize(14);
  text("HP", 650, 100);
  text("Jugador", 650, 120);
  text(hpJugador, 650, 140);
  text("HP", 650, 180);
  text("Imp", 650, 200);
  text(hpImp, 650, 220);
}
if(lvl==1){
  fill(0);
  textSize(14);
  text("HP", 650, 100);
  text("Jugador", 650, 120);
  text(hpJugador, 650, 140);
  text("HP", 650, 180);
  text("Warlock", 650, 200);
  text(hpLock, 650, 220);
}

  if(dibuixar&&introText){
    dibuixarMapa0();
    dibuixar=false;
    dibuixarInventari();
  }
  if(hpImp==0){
    impViu=false;
    image(terra, xImp*32, yImp*32);
    image(player, xJugador*32, yJugador*32);
    
  }
  if(hpLock==0){
    lockViu=false;
    image(terra, xLock*32, yLock*32);
    image(player, xJugador*32, yJugador*32);
  }
  if(tempsLluitaImp==true){
    delay(500);
    image(player, xJugador*32, yJugador*32);
    image(imp, xImp*32, yImp*32);
    hpImp--;
    tempsLluitaImp=false;
    lluitaImp=false;
  }
  if(tempsDefensaImp==true){
    delay(500);
    image(player, xJugador*32, yJugador*32);
    image(imp, xImp*32, yImp*32);
    hpJugador--;
    tempsDefensaImp=false;
    lluitaImp=false;
  }
  if(tempsLluitaLock==true){
    delay(500);
    image(player, xJugador*32, yJugador*32);
    image(lock, xLock*32, yLock*32);
    hpLock--;
    tempsLluitaLock=false;
    lluitaLock=false;
  }
  if(tempsDefensaLock==true){
    delay(500);
    image(player, xJugador*32, yJugador*32);
    image(lock, xLock*32, yLock*32);
    hpJugador--;
    tempsDefensaLock=false;
    lluitaLock=false;
  }
  
  if(hpJugador==0){
    background(0);
    textSize(30);
    fill(241, 44, 55);
    text("GAME OVER!!!", 300, 300);
  }
  if(hpLock==0){
    background(0);
    textSize(30);
    fill(241, 44, 55);
    text("YOU WIN!!!", 300, 300);
  }
}


void keyPressed(){
moviment=true;
  
  if(key=='w' || keyCode==UP){
        pucMoureJugador(yJugador-1, xJugador);
        moureJugador('y', -1);
  }
 
  
  if(key=='a' || keyCode==LEFT){
       pucMoureJugador(yJugador, xJugador-1);
       moureJugador('x', -1);
    
  } 
  
  if(key=='s' || keyCode==DOWN){
      pucMoureJugador(yJugador+1, xJugador);
      moureJugador('y', +1);
  }
  
  if(key=='d' || keyCode==RIGHT){   
      pucMoureJugador(yJugador, xJugador+1);
      moureJugador('x', +1);

  }

  
  if(key=='e'){
    accio();         
  }
  
  if(key=='z'){
    lluita();
  }
  
  if(key=='n'){
    tirarObj(1);
  }
   if(key=='m'){
    tirarObj(2);
  }
  if(key=='c'){
    introText=true;
    lvl=0;
    dibuixarMapa0();
    dibuixarInventari();
  }
  if(key=='1'){
    if(lluitaImp==true){
      atacar();
    }
    if(lluitaLock==true){
      atacar();
    }
  }
  if(key=='2'){
    if(lluitaImp==true){
      defensar();
    }
    if(lluitaLock==true){
      defensar();
    }
  }
  if(key=='3'){
    if(lluitaImp==true){
      fugir();
    }
    if(lluitaLock==true){
      fugir();
    }
  }
}

void dibuixarMapa0(){
  background(153);
 
  for(int i=0;i<mapas[lvl].length;i++){
    for(int j=0;j<mapas[lvl][i].length;j++){
      if(mapas[lvl][j][i]==0){
        image(terra, i*32,j*32);
        }
        if(mapas[lvl][j][i]==1){
        image(muro, i*32,j*32);
        }
        if(mapas[lvl][j][i]==2){
        image(porta, i*32,j*32);
        }
        if(mapas[lvl][j][i]==3){
        image(obj1, i*32,j*32);
        }
        if(mapas[lvl][j][i]==4){
        image(obj2, i*32,j*32);
        }
        if(mapas[lvl][j][i]==5){
        image(muroPorta, i*32,j*32);
        }
        if(mapas[lvl][j][i]==8){
        image(lock, i*32, j*32);
        }
         if(obj1Agafat==false){
          image(obj1, xObj1*32, yObj1*32);
        }
         if(obj2Agafat==false){
          image(obj2, xObj2*32, yObj2*32);
        }
        image(imp, xImp*32, yImp*32);
        image(player, xJugador*32, yJugador*32);
    } 
  }
}

void accio(){
    if(xJugador==xObj1&&yJugador==yObj1){
      obj1Agafat=true;
      mapas[lvl][yObj1][xObj1]=0;
      xObj1=-1;
      yObj1=-1;
    }else if(xJugador==xObj2&&yJugador==yObj2){
      obj2Agafat=true;
      mapas[lvl][xObj2][yObj2]=1;
      xObj2=-1;
      yObj2=-1;
    }else if((xJugador==18)&&(yJugador==12)&&(obj2Agafat==true)){
          lvl++;
          xJugador=1;
          yJugador=18;
          dibuixarMapa0();
          dibuixarInventari();
        }
}

void tirarObj(int obj){
  if(obj==1&&obj1Agafat==true){
     xObj1=xJugador;
     yObj1=yJugador;
     obj1Agafat=false; 
  }
  if(obj==2&&obj2Agafat==true){
     xObj2=xJugador;
     yObj2=yJugador;
     obj2Agafat=false;
  }
}

void pucMoureJugador(int y, int x){
         if(mapas[lvl][y][x]==1){
            moviment=false;
          }else if((mapas[lvl][y][x]==2)&&(obj1Agafat==false)){
            moviment=false;
          }else if((mapas[lvl][y][x]==5)&&(obj2Agafat==false)){
            moviment=false;
          }else if((y==17&&x==10&&impViu==true)){
            moviment=false;
          }else if((y==2&&x==17&&lockViu==true)){
            moviment=false;
          }
}

void moureJugador(char x, int direccio){
      if(x=='y'&&direccio==-1){
        if(moviment){
        yJugador--;
        dibuixar=true;
      } 
      }
      if(x=='x'&&direccio==-1){
        if(moviment){
        xJugador--;
        dibuixar=true;
      }
      }
      if(x=='y'&&direccio==+1){
        if(moviment){
        yJugador++;
        dibuixar=true;
      }
      }
      if(x=='x'&&direccio==+1){
        if(moviment){
        xJugador++;
        dibuixar=true;
      }
      } 
    }

void dibuixarInventari(){
  fill(153);
  rect(0, 640, 640, 32);
  
  if(obj1Agafat==true){
    image(obj1, 0, 640, 32, 32);
  }
  if(obj2Agafat==true&&obj1Agafat==false){
    image(obj2, 0, 640, 32, 32);
  }
  else if(obj2Agafat==true){
    image(obj2, 33, 640, 32, 32);
  }
}

void lluita(){
  if(lvl==0&&xJugador==9&&yJugador==17&&impViu==true){
    opcionsLluita(1);                                                            
  } 
  if(lvl==1&&xJugador==16&&yJugador==2&&lockViu==true){
    opcionsLluita(2);
  }
}
void opcionsLluita(int a){
  if(a==1){
  lluitaImp=true;
  rect(250, 300, 200, 200); 
  fill(241, 55, 44);
  text("OPCIONS DE LLUITA", 290, 320);
  text("1.- Atacar", 290, 350);
  text("2.- Defensar", 290, 380);
  text("3.- Fugir", 290, 410);
  }
  if(a==2){
  lluitaLock=true;
  rect(250, 300, 200, 200); 
  fill(241, 55, 44);
  text("OPCIONS DE LLUITA", 290, 320);
  text("1.- Atacar", 290, 350);
  text("2.- Defensar", 290, 380);
  text("3.- Fugir", 290, 410);
  }
}
void atacar(){
  if(lvl==0){
    dibuixarMapa0();
    dibuixarInventari();
    image(terra, xJugador*32, yJugador*32);
    image(terra, xImp*32, yImp*32);
    image(player, xImp*32, yImp*32);
    tempsLluitaImp=true;
  }
  if(lvl==1){
    dibuixarMapa0();
    dibuixarInventari();
    image(terra, xJugador*32, yJugador*32);
    image(terra, xLock*32, yLock*32);
    image(player, xLock*32, yLock*32);
    tempsLluitaLock=true;
  }
}
void defensar(){
  if(lvl==0){
    dibuixarMapa0();
    dibuixarInventari();
    image(terra, xJugador*32, yJugador*32);
    image(terra, xImp*32, yImp*32);
    image(imp, xJugador*32, yJugador*32);
    tempsDefensaImp=true;
  }
  if(lvl==1){
    dibuixarMapa0();
    dibuixarInventari();
    image(terra, xJugador*32, yJugador*32);
    image(terra, xLock*32, yLock*32);
    image(lock, xJugador*32, yJugador*32);
    tempsDefensaLock=true;
  }
}
void fugir(){
  if(lvl==0){
    dibuixar=true;
    xJugador=4;
    yJugador=4;
    lluitaImp=false;
  }
  if(lvl==1){
    dibuixar=true;
    xJugador=4;
    yJugador=4;
    lluitaLock=false;
  }
}



void intro(){
  background(0);
  fill(241, 55, 44);
  textSize(14);
  text("Ets un caballer del regne de Arathi, el teu rei Arathor, t'ha enviar a aquesta masmorra a matar", 10, 100);  
  text("al Senyor del Terror Khan.", 10, 130);
  text("Tota la masmorra esta plagada de els seus generals mes poderosos, amb els qui hauras de lluitar,", 10, 150); 
  text("per arribar a en Khan i, matar-lo.", 10, 170);
  text("Per agafar Objectes i pasar de lvl presiona la tecls E", 10, 200);
  text("Per lluitar fica't davant el jefe que vulguis matar i presiona la Z", 10, 220);
  text("Per llençar el primer objecte presiona N, per llençar el segon presiona M", 10, 240);
  text("Presiona la C per començar el joc.", 10, 260);
}